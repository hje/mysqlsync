unit ErrorLogUnit;

interface

uses
  System.Classes;

type
  TErrorLog = class(TinterFacedObject)
    private
      FErrorLog: TStringList;
    public
      constructor create; overload;
      destructor Destroy; overload;
      procedure FAddError(SenderName, ErrorMsg: string);
      property GetErrorLog: TStringList read FErrorLog;
  end;

implementation

uses
  System.SysUtils;

{ TErrorLog }

procedure TErrorLog.FAddError(SenderName, ErrorMsg: string);
begin
    FErrorLog.Add(DateTimeToStr(now)+': '+SenderName+': '+ErrorMsg);
end;

constructor TErrorLog.create;
begin
    inherited create;
    FErrorLog:=TstringList.create;
end;

destructor TErrorLog.Destroy;
begin
    FErrorlog.Free;
    inherited Destroy;
end;

end.

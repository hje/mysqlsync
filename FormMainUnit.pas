unit FormMainUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FormJ2OrdersUnit,
  SyncWebShopToLocalDBUnit, FormCleanUpUnit;

type
  TFormMain = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    btnCleanUp: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure btnCleanUpClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

uses FormMySQLDataUnit, FormLocalDBUnit;

procedure TFormMain.Button1Click(Sender: TObject);
begin
    if not assigned(FormMySQLData) then
        FormMySQLData:=TformMySQLData.create(self);

    FormMySQLData.Show;
end;

procedure TFormMain.Button2Click(Sender: TObject);
begin
    if not assigned(FormLocalDB) then
       FormLocalDB:=TFormLocalDB.Create(self);

    FormLocalDB.Show;
end;

procedure TFormMain.Button3Click(Sender: TObject);
begin
    if not assigned(FormJ2Orders) then
       FormJ2Orders:=TFormJ2Orders.Create(self);

    FormJ2Orders.Show;
end;

procedure TFormMain.Button4Click(Sender: TObject);
var
    SyncWebToLocalDB: TSyncWebToLocalDB;
begin
    SyncWebToLocalDB:=TSyncWebToLocalDB.Create;
    try
        SyncWebToLocalDB.SyncWebShopToLocalDB;
    finally
        SyncWebToLocalDB.free;
    end;
end;

procedure TFormMain.btnCleanUpClick(Sender: TObject);
var
  FormCleanUp: TFormCleanUp;
begin
    FormCleanUp:=TFormCleanUp.Create(self);
    FormCleanUp.Show;
end;

end.

object FormLocalDB: TFormLocalDB
  Left = 0
  Top = 0
  Caption = 'LocalDB'
  ClientHeight = 559
  ClientWidth = 974
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 974
    Height = 559
    ActivePage = tbsItems
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'ItemGroups'
      object Splitter1: TSplitter
        Left = 0
        Top = 105
        Width = 966
        Height = 8
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 834
      end
      object Splitter2: TSplitter
        Left = 0
        Top = 201
        Width = 966
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 197
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 966
        Height = 41
        Align = alTop
        Caption = 'Panel1'
        ShowCaption = False
        TabOrder = 0
        object Button1: TButton
          Left = 7
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Open'
          TabOrder = 0
          OnClick = Button1Click
        end
        object Button3: TButton
          Left = 335
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Export to Menu'
          TabOrder = 1
          OnClick = Button3Click
        end
        object Button7: TButton
          Left = 417
          Top = 6
          Width = 75
          Height = 25
          Caption = #218'tflyt allar'
          TabOrder = 2
          OnClick = Button7Click
        end
        object BtnCreateRootMenu: TButton
          Left = 549
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Root Menu'
          TabOrder = 3
          OnClick = BtnCreateRootMenuClick
        end
        object Button15: TButton
          Left = 840
          Top = 6
          Width = 115
          Height = 25
          Caption = 'Sort Non Shop Menus'
          TabOrder = 4
          OnClick = Button15Click
        end
        object Button13: TButton
          Left = 680
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Dummy Asset'
          TabOrder = 5
          OnClick = Button13Click
        end
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 41
        Width = 966
        Height = 64
        Align = alTop
        DataSource = DSoItemGrpHeader
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'HEADERID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'HEADERNAME'
            Visible = True
          end>
      end
      object DBGrid3: TDBGrid
        Left = 0
        Top = 113
        Width = 966
        Height = 88
        Align = alTop
        DataSource = DSoGroupNameID
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'GRPNAMEID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRPNAME'
            Width = 165
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VISIBLEONPRODUCTPAGE'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRPHEADERID'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATAAREAID'
            Width = 64
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 204
        Width = 966
        Height = 327
        Align = alClient
        Caption = 'Panel2'
        ShowCaption = False
        TabOrder = 3
        object DBGrid1: TDBGrid
          Left = 1
          Top = 1
          Width = 595
          Height = 325
          Align = alLeft
          DataSource = DSoItemGroups
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnTitleClick = DBGrid1TitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'GROUPID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCRIPTION'
              Width = 239
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PARENTGRP'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENUORDER'
              Width = 64
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENUSPANTO'
              Width = 64
              Visible = True
            end>
        end
        object TreeView1: TTreeView
          Left = 596
          Top = 1
          Width = 369
          Height = 325
          Align = alClient
          Indent = 19
          TabOrder = 1
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Settings'
      ImageIndex = 1
      object Label1: TLabel
        Left = 24
        Top = 16
        Width = 147
        Height = 13
        Caption = 'MENU_STORE_COMPONENTID'
        FocusControl = DBEdit1
      end
      object Label2: TLabel
        Left = 24
        Top = 44
        Width = 58
        Height = 13
        Caption = 'MENU_TYPE'
        FocusControl = DBEdit2
      end
      object Label3: TLabel
        Left = 24
        Top = 72
        Width = 75
        Height = 13
        Caption = 'MENU_PARAMS'
      end
      object Label4: TLabel
        Left = 24
        Top = 402
        Width = 53
        Height = 13
        Caption = 'LANGUAGE'
        FocusControl = DBEdit4
      end
      object Label5: TLabel
        Left = 672
        Top = 3
        Width = 65
        Height = 13
        Caption = 'DATAAREAID'
        FocusControl = DBEdit5
      end
      object Label6: TLabel
        Left = 24
        Top = 127
        Width = 98
        Height = 13
        Caption = 'MENU_ALIASPREFIX'
        FocusControl = DBEdit6
      end
      object Label7: TLabel
        Left = 24
        Top = 256
        Width = 90
        Height = 13
        Caption = 'CAT_ALIASPREFIX'
        FocusControl = DBEdit7
      end
      object Label8: TLabel
        Left = 24
        Top = 151
        Width = 91
        Height = 13
        Caption = 'MENU_LINKPREFIX'
        FocusControl = DBEdit3
      end
      object Label9: TLabel
        Left = 24
        Top = 286
        Width = 94
        Height = 13
        Caption = 'ASSET_FIRSTLEVEL'
        FocusControl = DBEdit8
      end
      object Label10: TLabel
        Left = 24
        Top = 312
        Width = 126
        Height = 13
        Caption = 'ASSET_CAT_NAMEPREFIX'
        FocusControl = DBEdit9
      end
      object Label11: TLabel
        Left = 24
        Top = 335
        Width = 126
        Height = 13
        Caption = 'ASSET_ART_NAMEPREFIX'
        FocusControl = DBEdit10
      end
      object Label12: TLabel
        Left = 24
        Top = 231
        Width = 82
        Height = 13
        Caption = 'CAT_ROOTTITLE'
        FocusControl = DBEdit11
      end
      object Label13: TLabel
        Left = 24
        Top = 174
        Width = 74
        Height = 13
        Caption = 'MENU_ROOTID'
        FocusControl = DBEdit12
      end
      object Label14: TLabel
        Left = 24
        Top = 361
        Width = 77
        Height = 13
        Caption = 'ASSET_ROOTID'
        FocusControl = DBEdit13
      end
      object Label15: TLabel
        Left = 24
        Top = 207
        Width = 66
        Height = 13
        Caption = 'CAT_ROOTID'
        FocusControl = DBEdit14
      end
      object Label16: TLabel
        Left = 24
        Top = 435
        Width = 117
        Height = 13
        Caption = 'CONTENT_ALIASPREFIX'
        FocusControl = DBEdit15
      end
      object Label17: TLabel
        Left = 24
        Top = 475
        Width = 64
        Height = 13
        Caption = 'IMAGE_PATH'
        FocusControl = DBEdit15
      end
      object DBEdit1: TDBEdit
        Left = 184
        Top = 13
        Width = 134
        Height = 21
        DataField = 'MENU_STORE_COMPONENTID'
        DataSource = DSoWebSettings
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 184
        Top = 41
        Width = 316
        Height = 21
        DataField = 'MENU_TYPE'
        DataSource = DSoWebSettings
        TabOrder = 1
      end
      object DBEdit4: TDBEdit
        Left = 184
        Top = 399
        Width = 95
        Height = 21
        DataField = 'LANGUAGE'
        DataSource = DSoWebSettings
        TabOrder = 2
      end
      object DBEdit5: TDBEdit
        Left = 672
        Top = 22
        Width = 134
        Height = 21
        DataField = 'DATAAREAID'
        DataSource = DSoWebSettings
        TabOrder = 3
      end
      object DBEdit6: TDBEdit
        Left = 184
        Top = 124
        Width = 134
        Height = 21
        DataField = 'MENU_ALIASPREFIX'
        DataSource = DSoWebSettings
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 184
        Top = 252
        Width = 134
        Height = 21
        DataField = 'CAT_ALIASPREFIX'
        DataSource = DSoWebSettings
        TabOrder = 5
      end
      object DBMemo1: TDBMemo
        Left = 184
        Top = 68
        Width = 609
        Height = 53
        DataField = 'MENU_PARAMS'
        DataSource = DSoWebSettings
        TabOrder = 6
      end
      object Button2: TButton
        Left = 558
        Top = 11
        Width = 75
        Height = 25
        Caption = 'Open'
        TabOrder = 7
        OnClick = Button2Click
      end
      object DBNavigator1: TDBNavigator
        Left = 348
        Top = 10
        Width = 204
        Height = 25
        DataSource = DSoWebSettings
        VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
        TabOrder = 8
      end
      object DBEdit3: TDBEdit
        Left = 184
        Top = 148
        Width = 609
        Height = 21
        DataField = 'MENU_LINKPREFIX'
        DataSource = DSoWebSettings
        TabOrder = 9
      end
      object DBEdit8: TDBEdit
        Left = 184
        Top = 283
        Width = 65
        Height = 21
        DataField = 'ASSET_FIRSTLEVEL'
        DataSource = DSoWebSettings
        TabOrder = 10
      end
      object DBEdit9: TDBEdit
        Left = 184
        Top = 309
        Width = 161
        Height = 21
        DataField = 'ASSET_CAT_NAMEPREFIX'
        DataSource = DSoWebSettings
        TabOrder = 11
      end
      object DBEdit10: TDBEdit
        Left = 184
        Top = 333
        Width = 161
        Height = 21
        DataField = 'ASSET_ART_NAMEPREFIX'
        DataSource = DSoWebSettings
        TabOrder = 12
      end
      object DBEdit11: TDBEdit
        Left = 184
        Top = 228
        Width = 3319
        Height = 21
        DataField = 'MENU_ROOTTITLE'
        DataSource = DSoWebSettings
        TabOrder = 13
      end
      object DBEdit12: TDBEdit
        Left = 184
        Top = 172
        Width = 134
        Height = 21
        DataField = 'MENU_ROOTID'
        DataSource = DSoWebSettings
        TabOrder = 14
      end
      object DBEdit13: TDBEdit
        Left = 184
        Top = 358
        Width = 65
        Height = 21
        DataField = 'ASSET_ROOTID'
        DataSource = DSoWebSettings
        TabOrder = 15
      end
      object DBEdit14: TDBEdit
        Left = 184
        Top = 204
        Width = 134
        Height = 21
        DataField = 'CAT_ROOTID'
        DataSource = DSoWebSettings
        TabOrder = 16
      end
      object DBEdit15: TDBEdit
        Left = 184
        Top = 432
        Width = 134
        Height = 21
        DataField = 'CONTENT_ALIASPREFIX'
        DataSource = DSoWebSettings
        TabOrder = 17
      end
      object DBEdit16: TDBEdit
        Left = 184
        Top = 472
        Width = 433
        Height = 21
        DataField = 'IMAGEPATH'
        DataSource = DSoWebSettings
        TabOrder = 18
      end
      object Button21: TButton
        Left = 864
        Top = 20
        Width = 75
        Height = 25
        Caption = 'Open'
        TabOrder = 19
        OnClick = Button21Click
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Errorlog'
      ImageIndex = 2
      object MemoErrorLog: TMemo
        Left = 0
        Top = 0
        Width = 681
        Height = 531
        Align = alLeft
        TabOrder = 0
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'MenuOrder'
      ImageIndex = 3
      object DBGrid4: TDBGrid
        Left = 0
        Top = 0
        Width = 385
        Height = 531
        Align = alLeft
        DataSource = DSoWebOrder
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'GROUPID'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRPNAMEID'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MENUORDER'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MENUSPANTO'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PARENTGRP'
            Width = 64
            Visible = True
          end>
      end
      object Button8: TButton
        Left = 888
        Top = 40
        Width = 75
        Height = 25
        Caption = 'Create Order'
        TabOrder = 1
        OnClick = Button8Click
      end
      object Button9: TButton
        Left = 888
        Top = 3
        Width = 75
        Height = 25
        Caption = 'Open'
        TabOrder = 2
        OnClick = Button9Click
      end
      object TreeView2: TTreeView
        Left = 385
        Top = 0
        Width = 359
        Height = 531
        Align = alLeft
        Indent = 19
        TabOrder = 3
      end
      object Button12: TButton
        Left = 888
        Top = 71
        Width = 75
        Height = 25
        Caption = 'Show Tree'
        TabOrder = 4
        OnClick = Button12Click
      end
    end
    object tbsItems: TTabSheet
      Caption = 'Items'
      ImageIndex = 4
      object Splitter3: TSplitter
        Left = 0
        Top = 425
        Width = 966
        Height = 3
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 371
        ExplicitWidth = 61
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 966
        Height = 73
        Align = alTop
        Caption = 'Panel3'
        ShowCaption = False
        TabOrder = 0
        object Label18: TLabel
          Left = 848
          Top = 8
          Width = 37
          Height = 13
          Caption = 'Label18'
        end
        object Label19: TLabel
          Left = 848
          Top = 27
          Width = 37
          Height = 13
          Caption = 'Label18'
        end
        object Button10: TButton
          Left = 6
          Top = 5
          Width = 75
          Height = 25
          Caption = 'Open'
          TabOrder = 0
          OnClick = Button10Click
        end
        object Button11: TButton
          Left = 305
          Top = 3
          Width = 102
          Height = 25
          Caption = 'Export Item'
          TabOrder = 1
          OnClick = Button11Click
        end
        object Button4: TButton
          Left = 585
          Top = 3
          Width = 97
          Height = 25
          Caption = 'Export Image Path'
          TabOrder = 2
          OnClick = Button4Click
        end
        object Button5: TButton
          Left = 305
          Top = 34
          Width = 102
          Height = 25
          Caption = 'Export All Items'
          TabOrder = 3
          OnClick = Button5Click
        end
        object Button6: TButton
          Left = 585
          Top = 34
          Width = 121
          Height = 25
          Caption = 'Export All Image paths'
          TabOrder = 4
          OnClick = Button6Click
        end
        object rgItemStatus: TRadioGroup
          Left = 100
          Top = 0
          Width = 185
          Height = 35
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Open'
            'Closed')
          TabOrder = 5
        end
        object Button14: TButton
          Left = 448
          Top = 3
          Width = 105
          Height = 25
          Caption = 'Update Status'
          TabOrder = 6
          OnClick = Button14Click
        end
        object Button16: TButton
          Left = 448
          Top = 34
          Width = 105
          Height = 25
          Caption = 'Update All Status'
          TabOrder = 7
          OnClick = Button16Click
        end
      end
      object DBGrid5: TDBGrid
        Left = 0
        Top = 73
        Width = 966
        Height = 352
        Align = alTop
        DataSource = DSoItems
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'EGIVARUNR'
            Width = 84
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VARUNAVN'
            Width = 306
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GROUPID'
            Title.Caption = 'Webgroup ID'
            Width = 73
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RELATEDGRPNAMEID'
            Title.Caption = 'WebGrp Level'
            Width = 77
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRIPTION'
            Title.Caption = 'Group Name'
            Width = 106
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PARENTGRP'
            Title.Caption = 'Web ParentGrp'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ITEMGRPID'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRPNAME'
            Title.Caption = 'Group Level'
            Width = 70
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 428
        Width = 966
        Height = 103
        Align = alClient
        TabOrder = 2
        object DBRichEdit1: TDBRichEdit
          Left = 1
          Top = 1
          Width = 248
          Height = 101
          Align = alLeft
          DataField = 'EXTRADESCRIPTION'
          DataSource = DSoExtraDescription
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          TabOrder = 0
          Zoom = 100
        end
      end
    end
    object tbsAssetsOrder: TTabSheet
      Caption = 'Assets Order'
      ImageIndex = 5
      object LabelStart: TLabel
        Left = 872
        Top = 152
        Width = 49
        Height = 13
        Caption = 'LabelStart'
      end
      object LabelEnd: TLabel
        Left = 872
        Top = 171
        Width = 37
        Height = 13
        Caption = 'Label18'
      end
      object DBGridAssetsOrder: TDBGrid
        Left = 0
        Top = 0
        Width = 449
        Height = 531
        Align = alLeft
        DataSource = DSoAssetsOrder
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'ID'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ASSETTYPE'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LFT'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RGT'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PARENTCATID'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ASSETORDER'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHILDCOUNT'
            Width = 30
            Visible = True
          end>
      end
      object Button17: TButton
        Left = 872
        Top = 3
        Width = 75
        Height = 25
        Caption = 'Open'
        TabOrder = 1
        OnClick = Button17Click
      end
      object Button18: TButton
        Left = 872
        Top = 34
        Width = 75
        Height = 25
        Caption = 'Create'
        TabOrder = 2
        OnClick = Button18Click
      end
      object Button19: TButton
        Left = 872
        Top = 65
        Width = 75
        Height = 25
        Caption = 'Set Lft/Rgt'
        TabOrder = 3
        OnClick = Button19Click
      end
      object Button20: TButton
        Left = 840
        Top = 104
        Width = 107
        Height = 25
        Caption = 'Update Asset table'
        TabOrder = 4
        OnClick = Button20Click
      end
      object Memo1: TMemo
        Left = 449
        Top = 0
        Width = 288
        Height = 531
        Align = alLeft
        Lines.Strings = (
          'Memo1')
        TabOrder = 5
      end
    end
  end
  object DSoItemGroups: TDataSource
    DataSet = dmLocal.FDQItemGroups
    OnDataChange = DSoItemGroupsDataChange
    Left = 252
    Top = 216
  end
  object DSoItemGrpHeader: TDataSource
    DataSet = dmLocal.FDQItemGrpHeader
    Left = 252
    Top = 168
  end
  object DSoWebSettings: TDataSource
    DataSet = dmLocal.FDQWebSettings
    Left = 520
    Top = 152
  end
  object DSoGroupNameID: TDataSource
    DataSet = dmLocal.FDQItemGrpName
    Left = 44
    Top = 264
  end
  object DSoWebOrder: TDataSource
    DataSet = dmLocal.FDQMenuOrder
    Left = 36
    Top = 328
  end
  object DSoItems: TDataSource
    DataSet = dmLocal.FDQItems
    Left = 36
    Top = 384
  end
  object DSoAssetsOrder: TDataSource
    DataSet = dmLocal.FDQAssetOrder
    Left = 276
    Top = 384
  end
  object DSoExtraDescription: TDataSource
    DataSet = dmLocal.FDMemExtraDesc
    Left = 444
    Top = 312
  end
end

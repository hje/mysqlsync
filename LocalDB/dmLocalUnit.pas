unit dmLocalUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.IB,
  FireDAC.Phys.IBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Generics.Collections, System.Variants,
  FireDAC.Stan.StorageBin, RecordsUnit, Vcl.ComCtrls, FormTreeViewUnit,
  ProgressFormUnit;

Const
  TABLE_CUSTOMERS = 7;   // Kundar
  TABLE_CONTACTS = 29;

type
  TDbParams = record
    DBName: string;
    ServerIP: string;
    procedure LoadDBParams;
  end;

  TWebSettings = record
     Menu_Store_ComponentID: integer;
     Menu_Type: string;
     Menu_Params: string;
     Menu_AliasPrefix: string;
     Menu_RootTitle: string;
     Menu_RootID: integer;
     Menu_LinkPrefix: string;
     Cat_AliasPrefix: string;
     Cat_RootID: integer;
     Asset_RootLevel: integer;
     Asset_CategoryNamePrefix: string;
     Asset_ArticleNamePrefix: string;
     Asset_RootID: integer;
     Content_AliasPrefix: string;
     ImagePath: string;
     Language: string;
     procedure LoadWebSettings;
   end;

  TdmLocal = class(TDataModule)
    FDConLocalDb: TFDConnection;
    FDQItemGroups: TFDQuery;
    FDQItemGroupsGROUPID: TStringField;
    FDQItemGroupsDESCRIPTION: TStringField;
    FDQItemGroupsPARENTGRP: TStringField;
    FDQItemGrpHeader: TFDQuery;
    FDQItemGrpHeaderHEADERID: TIntegerField;
    FDQItemGrpHeaderHEADERNAME: TStringField;
    FDQWebSettings: TFDQuery;
    FDQWebSettingsMENU_STORE_COMPONENTID: TIntegerField;
    FDQWebSettingsMENU_TYPE: TStringField;
    FDQWebSettingsMENU_PARAMS: TStringField;
    FDQWebSettingsLANGUAGE: TStringField;
    FDQWebSettingsDATAAREAID: TSmallintField;
    FDQWebSettingsMENU_ALIASPREFIX: TStringField;
    FDQWebSettingsCAT_ALIASPREFIX: TStringField;
    FDQItemGrpName: TFDQuery;
    FDQItemGrpNameGRPNAMEID: TIntegerField;
    FDQItemGrpNameGRPNAME: TStringField;
    FDQItemGrpNameDATAAREAID: TSmallintField;
    FDQItemGrpNameVISIBLEONPRODUCTPAGE: TSmallintField;
    FDQItemGrpNameGRPHEADERID: TIntegerField;
    FDQGetGroupID: TFDQuery;
    FDQItemGroupsDATAAREAID: TSmallintField;
    FDQItemGroupsGRPHEADERID: TIntegerField;
    FDQGetGroupIDGROUPID: TStringField;
    FDQGetGroupIDDESCRIPTION: TStringField;
    FDQGetGroupIDPARENTGRP: TStringField;
    FDQGetGroupIDGRPNAMEID: TIntegerField;
    FDQGetGroupIDGRPHEADERID: TIntegerField;
    FDQGetGroupIDHEADERNAME: TStringField;
    FDQWebSettingsMENU_LINKPREFIX: TStringField;
    FDQWebSettingsASSET_FIRSTLEVEL: TIntegerField;
    FDQWebSettingsASSET_CAT_NAMEPREFIX: TStringField;
    FDQWebSettingsASSET_ART_NAMEPREFIX: TStringField;
    FDQWebSettingsMENU_ROOTTITLE: TStringField;
    FDQWebSettingsMENU_ROOTID: TIntegerField;
    FDQWebSettingsASSET_ROOTID: TIntegerField;
    FDQWebSettingsCAT_ROOTID: TIntegerField;
    FDQMenuOrder: TFDQuery;
    FDQAllItemGroups: TFDQuery;
    FDQAllItemGroupsGROUPID: TStringField;
    FDQAllItemGroupsGRPNAMEID: TIntegerField;
    FDComDeleteMenuOrder: TFDCommand;
    FDQItemGroupsMENUORDER: TIntegerField;
    FDQItems: TFDQuery;
    FDQItemsGRPNAME: TStringField;
    FDQItemsGROUPID: TStringField;
    FDQItemsDESCRIPTION: TStringField;
    FDQItemsPARENTGRP: TStringField;
    FDQItemsRELATEDITEMGRPID: TStringField;
    FDQItemsITEMGRPID: TStringField;
    FDQItemsRELATEDGRPNAMEID: TIntegerField;
    FDQItemsVARUNAVN: TStringField;
    FDQWebSettingsCONTENT_ALIASPREFIX: TStringField;
    FDQItemsEGIVARUNR: TStringField;
    FDQItemsSATSUR: TCurrencyField;
    FDQEan: TFDQuery;
    FDQEanEAN: TStringField;
    FDQItemsSOLUPRISUR1: TBCDField;
    FDQItemsVARUNRID: TIntegerField;
    FDQWebSettingsIMAGEPATH: TStringField;
    FDQMenuOrderGetMaxRgt: TFDQuery;
    FDQItemGroupsMENUSPANTO: TIntegerField;
    FDMemMenuOrder: TFDMemTable;
    FDQMenuOrderGROUPID: TStringField;
    FDQMenuOrderGRPNAMEID: TIntegerField;
    FDQMenuOrderMENUORDER: TIntegerField;
    FDQMenuOrderMENUSPANTO: TIntegerField;
    FDQMenuOrderPARENTGRP: TStringField;
    FDQMenuOrderDESCRIPTION: TStringField;
    FDQMenuOrderGRPHEADERID: TIntegerField;
    FDMemMenuOrderGROUPID: TStringField;
    FDMemMenuOrderGRPNAMEID: TIntegerField;
    FDMemMenuOrderMENUORDER: TIntegerField;
    FDMemMenuOrderMENUSPANTO: TIntegerField;
    FDMemMenuOrderPARENTGRP: TStringField;
    FDMemMenuOrderDESCRIPTION: TStringField;
    FDMemMenuOrderGRPHEADERID: TIntegerField;
    FDQMenuOrderGetMaxRgtMAX: TIntegerField;
    FDQItemsSTATUS: TIntegerField;
    FDQAssetOrder: TFDQuery;
    FDComDeleteAssetOrder: TFDCommand;
    FDQAllItemGroupsPARENTGRP: TStringField;
    FDQAllAsstetItems: TFDQuery;
    FDQAllAsstetItemsEGIVARUNR: TStringField;
    FDQAllAsstetItemsRELATEDITEMGRPID: TStringField;
    FDQAllAsstetItemsSTATUS: TIntegerField;
    FDMemAssetClone: TFDMemTable;
    FDQCheckChildGrp: TFDQuery;
    FDQGetChildItems: TFDQuery;
    FDQGetChildItemsCHILDCOUNT: TIntegerField;
    FDQCheckChildGrpCHILDCOUNT: TIntegerField;
    FDQGetGenID: TFDQuery;
    FDQItemsDATAAREAID: TSmallintField;
    FDMemExtraDesc: TFDMemTable;
    FDTAdaptExtraDesc: TFDTableAdapter;
    FDComExtraDesc: TFDCommand;
    FDMemExtraDescEXTRADESCRIPTION: TMemoField;
    procedure DataModuleCreate(Sender: TObject);
    procedure FDQItemGroupsBeforeOpen(DataSet: TDataSet);
    procedure FDQItemGrpHeaderBeforeOpen(DataSet: TDataSet);
    procedure FDQItemGrpNameAfterScroll(DataSet: TDataSet);
    procedure FDQMenuOrderBeforeOpen(DataSet: TDataSet);
    procedure FDQItemGrpNameBeforeOpen(DataSet: TDataSet);
    procedure FDQItemsAfterScroll(DataSet: TDataSet);
    procedure FDMemExtraDescBeforeOpen(DataSet: TDataSet);
  private
    dbParams: TDbParams;
    function CountChildGroups(GroupID: string): integer;
    function CountChildItems(DS: TFDmemTable; GroupID: string): integer;
  public
    //WebSettings: TWebSettings;
    procedure CreateWebMenuOrder;
    procedure CreateAssetOrder;
    procedure SetAssetLftRgt;
    procedure UpdateAssetsLftRgt(Selected: boolean);
    function GetGenID(GeneratorName: string): integer;

  end;

var
  dmLocal: TdmLocal;

const
  MENU_ORDER_CONST = 1;
  ASSET_LFT_CONST = 1000;
  CLIENT_ID = 1;
  DATAAREAID = 1;

implementation

uses
  Vcl.Forms, Vcl.Dialogs, dmMainUnit;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


procedure TdmLocal.DataModuleCreate(Sender: TObject);
begin
    dbParams.LoadDBParams;
    TFDPhysIBConnectionDefParams(FDConLocalDb.Params).Server:=dbParams.ServerIP;
    TFDPhysIBConnectionDefParams(FDConLocalDb.Params).Database:=dbParams.DBName;

    //WebSettings.LoadWebSettings;
end;

function TdmLocal.CountChildGroups(GroupID: string): integer;
begin
    if FDQCheckChildGrp.Active then
       FDQCheckChildGrp.Close;

    FDQCheckChildGrp.ParamByName('DATAAREAID').Value:=DATAAREAID;
    FDQCheckChildGrp.ParamByName('parentgroup').Value:=GroupID;
    FDQCheckChildGrp.Open;

    result:=FDQCheckChildGrpCHILDCOUNT.Value;
end;

function TdmLocal.CountChildItems(DS: TFDmemTable; GroupID: string): integer;
var
    DSClone: TFDMemTable;
begin
    Result:=0;
    DSClone:= TFDMemTable.Create(nil);
    try
        DSClone.CloneCursor(ds);
        DSClone.Filter:='PARENTCATID = '+QuotedStr(GroupID)+' AND ASSETTYPE = '+QuotedStr('com_content.article.');
        DSClone.Filtered:=True;

        //showmessage(DSClone.FieldByName('ID').AsString+'; '+DSClone.FieldByName('PARENTCATID').AsString+'; '+intToStr(DSClone.RecordCount));
        result:=DSClone.RecordCount;
    finally
        DSClone.Free;
    end;
end;

procedure TdmLocal.CreateAssetOrder;
var
    Order: integer;
    LftOffset: integer;
    GrpChildCount: integer;
    FormProgress: TFormProgress;
    WebSettings: TWebsettings;
begin
    WebSettings.LoadWebSettings;    Order:=0;
    LftOffset:=2;

    FDQAssetOrder.DisableControls;
    FormProgress:=TFormProgress.Create(nil);
    try
        FormProgress.Show;
        FDQAssetOrder.Open;

        if FDQAllItemGroups.Active then
          FDQAllItemGroups.Close;

        FDQAllItemGroups.ParamByName('DATAAREAID').Value:=DATAAREAID;
        FDQAllItemGroups.Open;

        FDComDeleteAssetOrder.Execute;
        FDQAssetOrder.Refresh;
        FormProgress.ProgressBar1.Max:=FDQAllItemGroups.RecordCount;

        while not FDQAllItemGroups.eof do
        begin
            GrpChildCount:= CountChildGroups(FDQAllItemGroupsGROUPID.Value);

            FDQAssetOrder.Append;
            FDQAssetOrder.FieldByName('ID').Value:=trim(FDQAllItemGroupsGROUPID.Value);
            FDQAssetOrder.FieldByName('ASSETTYPE').Value:=WebSettings.Asset_CategoryNamePrefix;
            FDQAssetOrder.FieldByName('PARENTCATID').Value:=FDQAllItemGroupsPARENTGRP.Value;
            FDQAssetOrder.FieldByName('CHILDCOUNT').Value:=GrpChildCount;
            FDQAssetOrder.FieldByName('ASSETORDER').Value:=Order;
            FDQAssetOrder.Post;
            inc(Order);

            if FDQAllAsstetItems.Active then
               FDQAllAsstetItems.Close;

            FDQAllAsstetItems.ParamByName('DATAAREAID').Value:=DATAAREAID;
            FDQAllAsstetItems.ParamByName('GROUPID').Value:=FDQAllItemGroupsGROUPID.Value;
            FDQAllAsstetItems.Open;

            if (FDQAllAsstetItems.RecordCount > 0) then
            begin
                FDQAllAsstetItems.First;

                while not FDQAllAsstetItems.Eof do
                begin
                    FDQAssetOrder.Append;
                    FDQAssetOrder.FieldByName('ID').Value:=trim(FDQAllAsstetItemsEGIVARUNR.Value);
                    FDQAssetOrder.FieldByName('ASSETTYPE').Value:=WebSettings.Asset_ArticleNamePrefix;
                    FDQAssetOrder.FieldByName('PARENTCATID').Value:=trim(FDQAllItemGroupsGROUPID.Value);
                    //FDQAssetOrder.FieldByName('LFT').Value:=Order+LftOffset;
                    //inc(Order);
                    //FDQAssetOrder.FieldByName('Rgt').Value:=Order+LftOffset;
                    FDQAssetOrder.FieldByName('ASSETORDER').Value:=Order;
                    FDQAssetOrder.Post;
                    inc(Order);

                    FDQAllAsstetItems.Next;
                end;
            end;

            FormProgress.ProgressBar1.StepIt;
            FDQAllItemGroups.Next;
        end;
    finally
        FDQAssetOrder.EnableControls;
        FormProgress.Free;
    end;
end;

procedure TdmLocal.CreateWebMenuOrder;
begin
    with dmLocal do
    begin
        if FDQItemGrpName.Active then
            FDQItemGrpName.Close;
        if FDQItemGrpHeader.Active then
            FDQItemGrpHeader.Close;

        FDQItemGrpHeader.Open;
        FDQItemGrpName.Open;

        if FDQMenuOrder.Active then
          FDQMenuOrder.Close;

        FDQMenuOrder.Open;

        if FDQAllItemGroups.Active then
          FDQAllItemGroups.Close;

        FDQAllItemGroups.ParamByName('DATAAREAID').Value:=DATAAREAID;
        FDQAllItemGroups.Open;

        FDComDeleteMenuOrder.Execute;
        FDQMenuOrder.Refresh;

        while not FDQAllItemGroups.eof do
        begin
            //if FDQMenuOrder.Locate('GROUPID;GRPNAMEID',VarArrayOf([FDQAllItemGroupsGROUPID.Value,FDQAllItemGroupsGRPNAMEID.Value]),[]) then
            //   FDQMenuOrder.Edit
            //else
            FDQMenuOrder.Append;
            FDQMenuOrderGROUPID.Value:=trim(FDQAllItemGroupsGROUPID.Value);
            FDQMenuOrderGRPNAMEID.Value:=FDQAllItemGroupsGRPNAMEID.Value;
            FDQMenuOrder.Post;

            //inc(Order);
            FDQAllItemGroups.Next;
        end;
    end;
end;



{ TDbParams }

procedure TDbParams.LoadDBParams;
var
  IniFileName: string;
  IniFile: TStringList;
begin
    IniFileName:=IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName))+'MySQLSync.ini';

    iniFile:=TStringList.Create;
    try
        IniFile.LoadFromFile(IniFileName);
        DBName:=IniFile.Values['LocalDB'];
        ServerIP:=IniFile.Values['LocalServer'];
    finally
        IniFile.Free;
    end;
end;



procedure TdmLocal.FDMemExtraDescBeforeOpen(DataSet: TDataSet);
begin
    FDComExtraDesc.ParamByName('RELID').Value:=FDQItemsVARUNRID.Value;
    FDComExtraDesc.ParamByName('DATAAREAID').Value:=FDQItemsDATAAREAID.Value;
    //FDQExtraDescription.ParamByName('REFTABLEID').Value:=10;
end;

procedure TdmLocal.FDQItemGroupsBeforeOpen(DataSet: TDataSet);
begin
    FDQItemGroups.ParamByName('DATAAREAID').Value:=DATAAREAID;
    FDQItemGroups.ParamByName('GRPNAMEID').Value:=FDQItemGrpNameGRPNAMEID.Value;
end;

procedure TdmLocal.FDQItemGrpHeaderBeforeOpen(DataSet: TDataSet);
begin
    FDQItemGrpHeader.ParamByName('DATAAREAID').Value:=DATAAREAID;
end;

procedure TdmLocal.FDQItemGrpNameAfterScroll(DataSet: TDataSet);
begin
    FDQItemGroups.Close;
    FDQItemGroups.Open;
end;

procedure TdmLocal.FDQItemGrpNameBeforeOpen(DataSet: TDataSet);
begin
   FDQItemGrpName.ParamByName('GRPHEADERID').Value:=FDQItemGrpHeaderHEADERID.Value;
end;

procedure TdmLocal.FDQItemsAfterScroll(DataSet: TDataSet);
begin
    FDMemExtraDesc.Close;
    FDMemExtraDesc.Open;
end;

procedure TdmLocal.FDQMenuOrderBeforeOpen(DataSet: TDataSet);
begin
    FDQMenuOrder.ParamByName('DATAAREAID').Value:=DATAAREAID;
end;




function TdmLocal.GetGenID(GeneratorName: string): integer;
begin
    with FDQGetGenID do
    begin
        SQL.Clear;
        SQL.ADD('SELECT GEN_ID ('+GeneratorName+', 1) from rdb$database');
        Open;

        // Fields 0 er fyrsta felt � Queryini.
        Result := Fields [0].AsInteger;

        //if GenIBTransaction.Active then
        //   GenIBTransaction.commit;
    end;
end;

procedure TdmLocal.SetAssetLftRgt;
var
    LftRgt: integer;
    LftOffset: integer;
    GroupStack: TStack<TGroupRecord>;
    GroupRec: TGroupRecord;
    ParentGrpRec: TGroupRecord;
    //PopStack: boolean;
    LastAssetType: string;
    CurrentAssetType: string;
    RootNode: TTreeNode;
    SelectedNode: TTreeNode;
    FormProgress: TFormProgress;
    WebSettings: TWebsettings;
    strList: TStringList;
begin
    WebSettings.LoadWebSettings;
    if not assigned(strList) then
       strList:=TStringList.Create
    else
       strList.Clear;

    FormTreeView:=TFormTreeView.Create(self);
    GroupStack:=TStack<TGroupRecord>.create;
    FormProgress:=TFormProgress.Create(nil);
    FDQAssetOrder.DisableControls;
    try
        FormProgress.Show;
        LftRgt:=0;
        LftOffset:=1000;
        //PopStack:=False;

        if FDQAssetOrder.Active then
          FDQAssetOrder.First;

        FDMemAssetClone.CloneCursor(FDQAssetOrder);
        LastAssetType:=FDMEMAssetClone.FieldByName('ASSETTYPE').Value;

        GroupRec.GroupID:='00';
        GroupRec.ParentID:='';
        GroupRec.ChildNodes:=100;
        GroupStack.Push(GroupRec);
        FormProgress.ProgressBar1.Max:=FDMemAssetClone.RecordCount;

        while not FDMemAssetClone.eof do
        begin
            FormProgress.ProgressBar1.StepIt;
            CurrentAssetType:=FDMEMAssetClone.FieldByName('ASSETTYPE').Value;


                if CurrentAssetType = WebSettings.Asset_CategoryNamePrefix then
                begin
                    if (LastAssetType = WebSettings.Asset_ArticleNamePrefix) and (CurrentAssetType = WebSettings.Asset_CategoryNamePrefix) then
                    begin
                        LastAssetType:=CurrentAssetType;
                        GroupRec:=GroupStack.Pop;

                        while (GroupRec.ChildNodes = 0) do  // Count Groups
                        begin
                            if FDQAssetOrder.Locate('ID;ASSETTYPE',VarArrayOf([GroupRec.GroupID,WebSettings.Asset_CategoryNamePrefix]),[]) then
                            begin
                                // UpdateParent Group count
                                if (trim(GroupRec.ParentID) > '') then
                                begin
                                    ParentGrpRec:=GroupStack.Pop;
                                    ParentGrpRec.ChildNodes:=ParentGrpRec.ChildNodes-1;
                                    GroupStack.Push(ParentGrpRec);
                                end;

                                FDQAssetOrder.Edit;
                                FDQAssetOrder.FieldByName('RGT').AsInteger:=LftRgt+LftOffset;
                                FDQAssetOrder.Post;
                                GroupRec:=GroupStack.Pop;
                                inc(LftRgt);
                            end;
                        end;

                        GroupStack.Push(GroupRec);
                        continue;
                    end
                    else //if CurrentAssetType = WebSettings.Asset_CategoryNamePrefix then
                    begin
                        GroupRec.GroupID:=FDMemAssetClone.FieldByName('ID').Value;
                        GroupRec.ParentID:=FDMemAssetClone.FieldByName('PARENTCATID').Value;
                        GroupRec.ChildNodes:=FDMemAssetClone.FieldByName('CHILDCOUNT').Value;

                        if FDQAssetOrder.Locate('ID;ASSETTYPE',VarArrayOf([FDMEMAssetClone.FieldByName('ID').Value,CurrentAssetType]),[]) then
                        begin
                            FDQAssetOrder.Edit;
                            FDQAssetOrder.FieldByName('LFT').AsInteger:=LftRgt+LftOffset;
                            FDQAssetOrder.Post;
                            inc(LftRgt);
                        end;

                        while (GroupRec.ChildNodes = 0) do  // Count Groups
                        begin
                            if CountChildItems(FDMemAssetClone,GroupRec.GroupID) = 0 then  // Count articles
                            begin
                                //if FDQAssetOrder.Locate('ID;ASSETTYPE',VarArrayOf([GroupRec.GroupID,FDMEMAssetClone.FieldByName('ASSETTYPE').Value]),[]) then
                                if FDQAssetOrder.Locate('ID;ASSETTYPE',VarArrayOf([GroupRec.GroupID,WebSettings.Asset_CategoryNamePrefix]),[]) then
                                begin
                                    // UpdateParent Group count
                                    ParentGrpRec:=GroupStack.Pop;
                                    ParentGrpRec.ChildNodes:=ParentGrpRec.ChildNodes-1;
                                    GroupStack.Push(ParentGrpRec);

                                    FDQAssetOrder.Edit;
                                    FDQAssetOrder.FieldByName('RGT').AsInteger:=LftRgt+LftOffset;
                                    FDQAssetOrder.Post;

                                    strList.Add(GroupRec.GroupID+': Rgt: '+intToStr(LftRgt+LftOffset));
                                    GroupRec:=GroupStack.Pop;
                                    inc(LftRgt);
                                end;
                            end
                            else
                            begin
                                //GroupStack.Push(GroupRec);
                                Break;
                            end;
                        end;

                        GroupStack.Push(GroupRec);
                    end;
                end
                else   // is Item
                begin
                    if FDQAssetOrder.Locate('ID;ASSETTYPE',VarArrayOf([FDMEMAssetClone.FieldByName('ID').Value,CurrentAssetType]),[]) then
                    begin
                        FDQAssetOrder.Edit;
                        FDQAssetOrder.FieldByName('LFT').AsInteger:=LftRgt+LftOffset;
                        inc(LftRgt);
                        FDQAssetOrder.FieldByName('RGT').AsInteger:=LftRgt+LftOffset;
                        FDQAssetOrder.Post;
                        inc(LftRgt);
                    end;
                end;

                LastAssetType:=FDMEMAssetClone.FieldByName('ASSETTYPE').Value;
                FDMemAssetClone.Next;
        end;

        // Avgrei� teir ovastu b�lkarnar
        while (GroupStack.Count > 0) do
        begin
            GroupRec:=GroupStack.Pop;

            if (GroupRec.GroupID <> '00') then
            begin
                if FDQAssetOrder.Locate('ID;ASSETTYPE',VarArrayOf([GroupRec.GroupID,WebSettings.Asset_CategoryNamePrefix]),[]) then
                begin
                    strList.Add(GroupRec.GroupID+': Rgt: '+intToStr(LftRgt+LftOffset));
                    FDQAssetOrder.Edit;
                    FDQAssetOrder.FieldByName('RGT').AsInteger:=LftRgt+LftOffset;
                    FDQAssetOrder.Post;
                    inc(LftRgt);
                end;
            end;
        end;
    finally
        GroupStack.Free;
        FDQAssetOrder.EnableControls;
        FormProgress.Free;
    end;
end;

procedure TdmLocal.UpdateAssetsLftRgt(Selected: boolean);
var
    AssetLookUpID: string;
    FormProgress: TFormProgress;
    WebSettings: TWebsettings;
begin
    WebSettings.LoadWebSettings;
    FDQAssetOrder.Open;
    FDQAssetOrder.First;
    dmMain.FDMemAssets.Open;
    dmMain.FDMemCategories.Open;

    FormProgress:=TFormProgress.Create(nil);
    try
        FormProgress.Show;
        dmMain.FDMemAssets.DisableControls;
        dmMain.FDMemCategories.DisableControls;
        FDQAssetOrder.DisableControls;
        FormProgress.ProgressBar1.Max:=FDQAssetOrder.RecordCount;

        {if Selected = true then
        begin
            FDQAssetOrder
        end;}


        while not FDQAssetOrder.eof do
        begin
            with dmMain do
            begin
                if FDQAssetOrder.FieldByName('ASSETTYPE').Value = WebSettings.Asset_ArticleNamePrefix then
                begin
                    AssetLookUpID:='ItemID-'+FDQAssetOrder.FieldByName('ID').AsString;

                    if dmMain.FDMemAssets.Locate('Title',AssetLookupID,[]) then
                    begin
                        dmMain.FDMemAssets.Edit;
                        dmMain.FDMemAssetslft.Value:=FDQAssetOrder.FieldByName('LFT').Value;
                        dmMain.FDMemAssetsRgt.Value:=FDQAssetOrder.FieldByName('RGT').Value;
                        dmMain.FDMemAssets.Post;
                    end;
                end
                else if FDQAssetOrder.FieldByName('ASSETTYPE').Value = WebSettings.Asset_CategoryNamePrefix then
                begin
                    if FDMemCategories.Locate('ALIAS','cat-'+FDQAssetOrder.FieldByName('ID').AsString,[]) then
                    begin
                        AssetLookUpID:=WebSettings.Asset_CategoryNamePrefix+FDMemCategoriesid.AsString;
                    end;

                    if dmMain.FDMemAssets.Locate('name',AssetLookupID,[]) then
                    begin
                        dmMain.FDMemAssets.Edit;
                        dmMain.FDMemAssetslft.Value:=FDQAssetOrder.FieldByName('LFT').Value;
                        dmMain.FDMemAssetsRgt.Value:=FDQAssetOrder.FieldByName('RGT').Value;
                        dmMain.FDMemAssets.Post;
                    end;
                end;
            end;

            FormProgress.ProgressBar1.StepIt;
            FDQAssetOrder.Next;
        end;
    finally
        dmMain.FDMemAssets.EnableControls;
        dmMain.FDMemCategories.EnableControls;
        FDQAssetOrder.EnableControls;
        FormProgress.Free;
    end;
end;

{ TWebSettings }

procedure TWebSettings.LoadWebSettings;
begin
   if dmLocal.FDQWebSettings.Active then
      dmLocal.FDQWebSettings.Close;

   dmLocal.FDQWebSettings.Open;

   Menu_Store_ComponentID:=dmLocal.FDQWebSettingsMENU_STORE_COMPONENTID.Value;
   Menu_Type:=dmLocal.FDQWebSettingsMENU_TYPE.Value;
   Menu_Params:=dmLocal.FDQWebSettingsMENU_PARAMS.Value;
   Menu_AliasPrefix:=dmLocal.FDQWebSettingsMENU_ALIASPREFIX.Value;
   Menu_RootTitle:=dmLocal.FDQWebSettingsMENU_ROOTTITLE.Value;
   Menu_RootID:=dmLocal.FDQWebSettingsMENU_ROOTID.Value;
   Menu_LinkPrefix:=dmLocal.FDQWebSettingsMENU_LINKPREFIX.Value;
   Cat_AliasPrefix:=dmLocal.FDQWebSettingsCAT_ALIASPREFIX.Value;
   Cat_RootID:=dmLocal.FDQWebSettingsCAT_ROOTID.Value;
   Asset_RootLevel:=dmLocal.FDQWebSettingsASSET_FIRSTLEVEL.Value;
   Asset_CategoryNamePrefix:=dmLocal.FDQWebSettingsASSET_CAT_NAMEPREFIX.Value;
   Asset_ArticleNamePrefix:=dmLocal.FDQWebSettingsASSET_ART_NAMEPREFIX.Value;
   Asset_RootID:=dmLocal.FDQWebSettingsASSET_ROOTID.Value;
   Content_AliasPrefix:=dmLocal.FDQWebSettingsCONTENT_ALIASPREFIX.Value;
   ImagePath:=dmLocal.FDQWebSettingsIMAGEPATH.Value;
   Language:=dmLocal.FDQWebSettingsLANGUAGE.Value;
end;

end.

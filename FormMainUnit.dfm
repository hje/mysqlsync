object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'MySQLSync'
  ClientHeight = 466
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 8
    Top = 8
    Width = 145
    Height = 42
    Caption = 'MySQL Data'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 8
    Top = 56
    Width = 145
    Height = 41
    Caption = 'Local DB'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 456
    Top = 63
    Width = 145
    Height = 41
    Caption = 'Orders'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 456
    Top = 16
    Width = 145
    Height = 41
    Caption = 'Sync orders'
    TabOrder = 3
    OnClick = Button4Click
  end
  object btnCleanUp: TButton
    Left = 456
    Top = 182
    Width = 145
    Height = 41
    Caption = 'Clean Up'
    TabOrder = 4
    OnClick = btnCleanUpClick
  end
end

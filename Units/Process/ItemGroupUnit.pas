unit ItemGroupUnit;

interface

uses System.SysUtils, dmLocalUnit, Generics.Collections, Spring;

type

  TRootItemGrpRec = record
    RootGrpID: integer;
    RoogGrpName: string;
  end;

  TItemGrpRec = record
    GroupID: string;
    GroupName: string;
    ParentGroup: string;
    GroupHeaderID: integer;
    GroupPath: string;
    GroupLevel: integer;
    MenuOrder: integer;
    MenuSpan: integer;
    alias: string;
  end;

  TItemGroup = class(TInterfacedObject)
    private
      FWebSettings: TWebSettings;
      FItemGrpRec : TItemGrpRec;
      FRootItemGrpRec: TRootItemGrpRec;
      FDataAreaID: SmallInt;
      FItemGrpStack: TStack<TItemGrpRec>;
      procedure GetItemGroupLevel;
      procedure GetItemGroupFromDb;
      procedure GetRootItemGroup;
      function LoadItemGrpStack: TStack<TItemGrpRec>;
      procedure CreateItemGrpPath;
    public
      procedure UpdateItemGroup(GroupHeaderID: integer);
      constructor Create(DataAreaID: integer; GroupHeaderID: integer); overload;
      property RootItemGrp: TRootItemGrpRec read FRootItemGrpRec;
      property GetItemGroup: TItemGrpRec read FItemGrpRec;
      property GetItemGroupStack: TStack<TItemGrpRec> read LoadItemGrpStack;
      property GetItemGroupRecord: TItemGrpRec read FitemGrpRec;
  end;


implementation

{ TItemGroup }

constructor TItemGroup.Create(DataAreaID: integer; GroupHeaderID: integer);
begin
    inherited Create;
    FWebSettings.LoadWebSettings;
    FDataAreaID:= DataAreaID;
    FItemGrpRec.GroupHeaderID:= GroupHeaderID;
    FItemGrpStack:=TStack<TItemGrpREc>.create;
    GetItemGroupFromDb;
    GetItemGroupLevel;
end;

procedure TItemGroup.GetItemGroupFromDb;
begin
    FItemGrpRec.GroupID:=dmLocal.FDQItemGroupsGROUPID.Value;
    FItemGrpRec.GroupName:=dmLocal.FDQItemGroupsDESCRIPTION.Value;
    FItemGrpRec.ParentGroup:=dmLocal.FDQItemGroupsPARENTGRP.Value;
    FItemGrpRec.MenuOrder:=dmLocal.FDQItemGroupsMENUORDER.Value;
    FItemGrpRec.MenuSpan:=dmLocal.FDQItemGroupsMENUSPANTO.Value;
end;


procedure TItemGroup.GetItemGroupLevel;
var
    tmpItemGrpRec: TItemGrpRec;
begin
    FItemGrpRec.GroupLevel:=1;
    tmpItemGrpRec:=FItemGrpRec;
    FItemGrpStack.Push(FItemGrpRec);

    while (trim(tmpItemGrpRec.ParentGroup) > '') do
    begin
        dmLocal.FDQGetGroupID.Close;
        dmLocal.FDQGetGroupID.ParamByName('GROUPID').Value:=tmpItemGrpRec.ParentGroup;
        dmLocal.FDQGetGroupID.ParamByName('DATAAREAID').Value:=FDataAreaID;
        dmLocal.FDQGetGroupID.ParamByName('GRPHEADERID').Value:=tmpItemGrpRec.GroupHeaderID;
        dmLocal.FDQGetGroupID.Open;

        FItemGrpRec.GroupLevel:=FItemGrpRec.GroupLevel+1;

        tmpItemGrpRec.ParentGroup:=dmLocal.FDQGetGroupIDPARENTGRP.Value;
        tmpItemGrpRec.GroupHeaderID:=dmLocal.FDQGetGroupIDGRPHEADERID.Value;
        tmpItemGrpRec.GroupName:=dmLocal.FDQGetGroupIDDESCRIPTION.Value;
        tmpItemGrpRec.GroupID:=dmLocal.FDQGetGroupIDGROUPID.Value;
        FItemGrpStack.Push(tmpItemGrpRec);

        if (FItemGrpRec.GroupLevel > 10) then
            Break;
    end;

    CreateItemGrpPath;
end;

procedure TItemGroup.GetRootItemGroup;
begin
    with dmLocal do
    begin
        FRootItemGrpRec.RootGrpID:=FDQItemGrpHeaderHEADERID.Value;
        FRootItemGrpRec.RoogGrpName:=FDQItemGrpHeaderHEADERNAME.Value;
    end;
end;

procedure TItemGroup.CreateItemGrpPath;
var
  tempStack: TStack<TItemGrpRec>;
  ItemGrp: TItemGrpRec;
  Path: string;
begin
    tempStack:=TStack<TItemGrpRec>.Create;
    try
        for itemGrp in FItemGrpStack do
        begin
            tempStack.Push(itemGrp);
        end;

        while (tempStack.Count > 0) do
        begin
            ItemGrp:=tempStack.Pop;
            //Path:=Path+ItemGrp.GroupName+'/';
            Path:=Path+FWebSettings.Cat_AliasPrefix+ItemGrp.GroupID+'/';
        end;

        FItemGrpRec.GroupPath:=Path;
    finally
        tempStack.Free;
    end;
end;

function TItemGroup.LoadItemGrpStack: TStack<TItemGrpRec>;
var
  ItemGrpRec: TItemGrpRec;
begin
    result:=TStack<TItemGrpRec>.create;

    for ItemGrpRec in FItemGrpStack do
    begin
        result.Push(ItemGrpRec);
    end;
end;

procedure TItemGroup.UpdateItemGroup(GroupHeaderID: integer);
begin
    FItemGrpRec.GroupHeaderID:= GroupHeaderID;
    //FItemGrpStack:=TStack<TItemGrpREc>.create;
    GetItemGroupFromDb;
    GetItemGroupLevel;
end;

end.

unit ExportItemGroupsUnit;

interface

uses dmLocalUnit, System.SysUtils, dmMainUnit, ItemGroupUnit, AssetUnit,
      System.Generics.Collections, ErrorLogUnit, CategoriesUnit, JoomlaMenuUnit;

type


   {TItemGroup = record
    private
      ParentGroup: string;
      procedure GetItemGroupLevel;
    public
      GroupID: string;
      GroupName: string;
      GroupPath: string;
      GroupLevel: integer;
      procedure GetItemGroupFromDb;
  end;}

  TWebMenu = record
    strict private
      FItemGroup: TItemGroup;
      FWebSettings: TWebSettings;
      FCategory: TCategories;
      FErrorLog: TErrorLog;
      MenuStoreComponentID: integer;
      MenuLinkPrefix: string;
      MenuAliasPrefix: string;
      FMenuType: string;
      MenuParams: string;
      FMenuPath: string;
      FMenuLevel: integer;
      FLft: integer;  // Menuorder
      FRgt: integer;
      Language: string;
      CatAliasPrefix: string;
      //procedure GetGroupLevel;
      procedure WebMenuInit;
      procedure CreateMenuPath;
      function GetParentMenuID: integer;
      function GetRootMenuID: integer;
      procedure SaveToDb(MenuOrder, CatID, ParentMenuID: integer; GroupID, Description: string);
    public
      procedure CreateRootMenu;
      procedure CreateWebMenu(ItemGrp: TItemGroup; ErrorLog: TErrorLog);
      procedure SortNonShopWebMenus;
      function CheckIfRootMenuExists: Boolean;
  end;



implementation

uses
  Vcl.Dialogs, Data.DB, FireDAC.Comp.Client;



function TWebMenu.CheckIfRootMenuExists: Boolean;
begin
    WebMenuInit;

    with dmMain do
    begin
        if not FDMemTabMenuType.Active then
            FDMemTabMenuType.Open;

        FDMemTabMenuType.Locate('menutype',FMenuType,[]);

        if not FDMemMenu.Active then
            FDMemMenu.Open;

        FDMemMenu.Filtered:=False;
     end;

    result:= dmMain.FDMemMenu.Locate('id', FWebSettings.Menu_RootID, []);
end;

procedure TWebMenu.CreateMenuPath;
var
  ItemGrpStack: TStack<TItemGrpRec>;
  ItemGroup: TItemGrpRec;
begin
    ItemGrpStack:=FItemGroup.GetItemGroupStack;
    FMenuLevel:=1;
    FMenuPath:=FWebSettings.Menu_AliasPrefix+'root-'+dmLocal.FDQItemGrpHeaderHEADERID.AsString;

    while (ItemGrpStack.Count > 0) do
    begin
        ItemGroup:=ItemGrpStack.Pop;

        if (FMenuPath > '') then
           FMenuPath:=FMenuPath+'/'+MenuAliasPrefix+ItemGroup.GroupID
        else
           FMenuPath:=MenuAliasPrefix+ItemGroup.GroupID;

        FMenuLevel:=FMenuLevel+1;
    end;
end;


procedure TWebMenu.CreateRootMenu;
var
    JM: IJoomlaMenu;
begin
   JM:=TJoomlaMenu.create(MENU_ORDER_CONST+1);
   WebMenuInit;
   FMenuPath:=MenuAliasPrefix+'root-'+dmLocal.FDQItemGrpHeaderHEADERID.AsString;
   FMenuLevel:=1;

   with dmMain do
   begin
       if not FDMemTabMenuType.Active then
           FDMemTabMenuType.Open;

       FDMemTabMenuType.Locate('menutype',FMenuType,[]);

       if not FDMemMenu.Active then
           FDMemMenu.Open;
   end;

   Frgt:= JM.GetMaxMenuOrderID+1;
   SaveToDb(MENU_ORDER_CONST, FWebSettings.Cat_RootID, 1, 'root-'+dmLocal.FDQItemGrpHeaderHEADERID.AsString, dmLocal.FDQItemGrpHeaderHEADERNAME.Value);

   dmMain.FDMemMenu.Close;
   dmMain.FDMemMenu.Open;

   if dmMain.FDMemMenu.Locate('alias',MenuAliasPrefix+'root-'+dmLocal.FDQItemGrpHeaderHEADERID.AsString) then
   begin
       with dmLocal do
       begin
           FDQWebSettings.Edit;
           FDQWebSettingsMENU_ROOTID.Value:=dmMain.FDMemMenuid.Value;
           FDQWebSettings.Post;
       end;
   end;
end;

procedure TWebMenu.CreateWebMenu(ItemGrp: TItemGroup; ErrorLog: TErrorLog);
var
  CatID: integer;
  MenuParentID: integer;
  tempRootMenuID: integer;
  ItemGroupRec: TItemGrpRec;
begin
    WebMenuInit;
    FItemGroup:=ItemGrp;
    ItemGroupRec:=FItemGroup.GetItemGroup;
    CatID:=FCategory.ExportCatToWeb(FItemGroup,FWebSettings, ErrorLog);

    with dmMain do
    begin
        if not FDMemTabMenuType.Active then
            FDMemTabMenuType.Open;

        FDMemTabMenuType.Locate('menutype',FMenuType,[]);

        if not FDMemMenu.Active then
            FDMemMenu.Open;

        // Find root menu id, if it does'nt exist, then create one
        tempRootMenuID:=GetRootMenuID;

        if tempRootMenuID = -1 then
        begin
             CreateRootMenu;
        end;
        //exit;

        MenuParentID:=GetParentMenuID;

        if (MenuParentID < 1) then
           MenuParentID := FWebSettings.Menu_RootID;

        Frgt:=ItemGroupRec.MenuSpan+MENU_ORDER_CONST;

        CreateMenuPath;
        SaveToDb(ItemGroupRec.MenuOrder+MENU_ORDER_CONST,CatID,MenuParentID, ItemGroupRec.GroupID, ItemGroupRec.GroupName);
    end;
end;

function TWebMenu.GetParentMenuID: integer;
var
  ItemGrpRec: TItemGrpRec;
begin
    with dmMain do
    begin
        if not FDMemMenu.Active then
           FDMemMenu.Active:=True;

        ItemGrpRec:=FItemGroup.GetItemGroup;

        if FDMemMenu.Locate('alias',FWebSettings.Menu_AliasPrefix+ItemGrpRec.ParentGroup,[]) then
           result:=FDMemMenuid.Value
        else
           result:=-1;
    end;
end;

function TWebMenu.GetRootMenuID: integer;
begin
    with dmMain do
    begin
        if FDMemMenu.Locate('alias',FWebSettings.Menu_AliasPrefix+'root-'+dmLocal.FDQItemGrpHeaderHEADERID.AsString, []) then
        //if FDMemMenu.Locate('id', FWebSettings.Menu_RootID,[]) then
        begin
            result:=FDMemMenuid.Value;
            //showmessage(FDMemMenuid.AsString);
        end
        else
           result:=-1;
    end;
end;

procedure TWebMenu.SaveToDb(MenuOrder, CatID, ParentMenuID: integer; GroupID, Description: string);
begin
    with dmMain do
    begin
        if FDQMenuUpdate.Active then
           FDQMenuUpdate.Close;

        FDQMenuUpdate.ParamByName('alias').Value:=MenuAliasPrefix+GroupID;
        FDQMenuUpdate.Open;

        if (FDQMenuUpdate.RecordCount > 0) then
        begin
           FDQMenuUpdate.Edit;
        end
        else
        begin
            FDQMenuUpdate.Insert;
        end;

        FDQMenuUpdatemenutype.Value:=FMenuType;
        FDQMenuUpdateparams.Value:=MenuParams;
        FDQMenuUpdatelink.Value:=MenuLinkPrefix+IntToStr(CatID);
        FDQMenuUpdatecomponent_id.Value:=MenuStoreComponentID;
        FDQMenuUpdatelanguage.Value:=Language;
        FDQMenuUpdatetitle.Value:=Description;
        FDQMenuUpdatealias.Value:=MenuAliasPrefix+GroupID;

        // DONE -ohje -cmanglar:  Gera funkti�n sum genererar path �t fr� alias � �llum level
        FDQMenuUpdatepath.Value:=FMenuPath;
        FDQMenuUpdateparent_id.Value:=ParentMenuID;
        FDQMenuUpdatelevel.Value:=FMenuLevel;
        // ---//
        FDQMenuUpdatetype.Value:='component';
        FDQMenuUpdatepublished.Value:=1;
        FDQMenuUpdateaccess.Value:=1;
        FDQMenuUpdateimg.Value:='';
        FDQMenuUpdatetemplate_style_id.Value:=0;
        FDQMenuUpdatehome.Value:=0;
        FDQMenuUpdateclient_id.Value:=0;
        FDQMenuUpdatelft.Value:=MenuOrder; // Webmenu order
        FDQMenuUpdatergt.Value:=Frgt;
        FDQMenuUpdate.Post;
    end;
end;

procedure TWebMenu.SortNonShopWebMenus;
var
    TempAlias: string;
begin
    WebMenuInit;

    with dmMain do
    begin
        if not FDMemTabMenuType.Active then
            FDMemTabMenuType.Open;

        if FDMemTabMenuType.Filtered then
           FDMemTabMenuType.Filtered:=False;

        FDMemTabMenuType.Locate('menutype',FMenuType,[]);

        if not FDMemMenu.Active then
            FDMemMenu.Open;

        if FDMemMenu.Filtered then
           FDMemMenu.Filtered:=False;

        FDMemMenu.First;

        while not FDMemMenu.Eof do
        begin
            try
                tempAlias:=Copy(FDMemMenualias.Value,1, Length(FWebsettings.Menu_AliasPrefix));

                if (tempAlias <> FWebsettings.Menu_AliasPrefix) then
                begin
                    if (FDMemMenulft.Value < 1000) then
                    begin
                        FDMemMenu.Edit;

                        FDMemMenulft.Value:=FDMemMenulft.Value+1000; // Webmenu order
                        FDMemMenurgt.Value:=FDMemMenurgt.Value+1000;
                        FDMemMenu.Post;
                    end;
                end;
            except
                FDMemMenu.Cancel;
            end;

            FDMemMenu.Next;
        end;
    end;
end;

procedure TWebMenu.WebMenuInit;
begin
    FWebSettings.LoadWebSettings;
    MenuStoreComponentID:=FWebSettings.Menu_Store_ComponentID;
    FMenuType:=FWebSettings.Menu_Type;
    MenuParams:=FWebSettings.Menu_Params;
    Language:=FWebSettings.Language;
    MenuAliasPrefix:=FWebSettings.Menu_AliasPrefix;
    CatAliasPrefix:=FWebSettings.Cat_AliasPrefix;
    MenuLinkPrefix:=FWebSettings.Menu_LinkPrefix;
end;





end.

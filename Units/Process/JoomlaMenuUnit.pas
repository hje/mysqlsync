unit JoomlaMenuUnit;

interface

uses dmLocalUnit, System.Classes, RecordsUnit, Firedac.comp.Client;

type
  IJoomlaMenu = interface
  ['{3DFDEE2D-4356-476E-B1D6-98E26098A18F}']
    function SetMenuLftValue(GroupID: string):integer;
    function SetMenuRgtValue(GroupID: string):integer;
    function HasChild(MemTable: TFDMemTable; GrpRec: IGroupLevel; GroupID: string; GrpNameID: integer; var NodeText: string): boolean;
    function GetMaxMenuOrderID: integer;
  end;

  TJoomlaMenu = class(TInterfacedObject, IJoomlaMenu)
    private
      FMenuOrder: integer;
    public
      function SetMenuLftValue(GroupID: string):integer;
      function SetMenuRgtValue(GroupID: string):integer;
      function HasChild(MemTable: TFDMemTable; GrpRec: IGroupLevel; GroupID: string; GrpNameID: integer; var NodeText: string): boolean;
      function GetMaxMenuOrderID: integer;
      constructor create(MenuInitValue: integer); overload;
  end;


implementation

uses
  System.SysUtils;

{ TJoomlaMenu }

constructor TJoomlaMenu.create(MenuInitValue: integer);
begin
   FMenuOrder:=MenuInitValue;
end;

function TJoomlaMenu.SetMenuLftValue(GroupID: string):integer;
begin
   with dmLocal do
   begin
      if FDQMenuOrder.Locate('GROUPID',GroupID,[]) then
      begin
          FDQMenuOrder.Edit;
          FDQMenuOrderMENUORDER.Value:=FMenuOrder;
          FDQMenuOrder.Post;
      end;
   end;
   inc(FMenuOrder);
end;

function TJoomlaMenu.SetMenuRgtValue(GroupID: string):integer;
begin
   with dmLocal do
   begin
      if FDQMenuOrder.Locate('GROUPID',GroupID,[]) then
      begin
          FDQMenuOrder.Edit;
          FDQMenuOrderMENUSPANTO.Value:=FMenuOrder;
          FDQMenuOrder.Post;
      end;
   end;
   inc(FMenuOrder);
end;

function TJoomlaMenu.GetMaxMenuOrderID: integer;
begin
    with dmLocal do
    begin
        try
            FDQMenuOrderGetMaxRgt.Close;
            FDQMenuOrderGetMaxRgt.Open;
            result:=FDQMenuOrderGetMaxRgtMAX.Value;
        finally
           FDQMenuOrderGetMaxRgt.Close;
        end;
    end;
end;

function TJoomlaMenu.HasChild(MemTable: TFDMemTable; GrpRec: IGroupLevel; GroupID: string; GrpNameID: integer; var NodeText: string): boolean;
begin
    MemTable.Filter:='GROUPID = '+QuotedStr(GroupID+'*');
    MemTable.Filtered:=True;
    result:= memtable.RecordCount > 1;

    if result = true then
    begin
        NodeText:= GROUPID+': ('+intToStr(FMenuOrder)+') '+dmLocal.FDMemMenuOrderDESCRIPTION.Value;

        if dmlocal.FDQMenuOrder.Locate('GROUPID',GroupID,[]) then
        begin
            dmlocal.FDQMenuOrder.Edit;
            dmlocal.FDQMenuOrderMENUORDER.Value:=FMenuOrder;
            dmlocal.FDQMenuOrder.Post;
        end;

        GrpRec.AddGroup(GroupID,GrpNameID,MemTable.RecordCount);
        Inc(FMenuOrder);
    end
    else
    begin
        NodeText:= GROUPID+': ('+intToStr(FMenuOrder)+','+intToStr(FMenuOrder+1)+') '+dmLocal.FDMemMenuOrderDESCRIPTION.Value;
        SetMenuLftValue(GroupID);
        SetMenuRgtValue(GroupID);
    end;
end;


end.

{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N-,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$WARN SYMBOL_DEPRECATED ON}
{$WARN SYMBOL_LIBRARY ON}
{$WARN SYMBOL_PLATFORM ON}
{$WARN SYMBOL_EXPERIMENTAL ON}
{$WARN UNIT_LIBRARY ON}
{$WARN UNIT_PLATFORM ON}
{$WARN UNIT_DEPRECATED ON}
{$WARN UNIT_EXPERIMENTAL ON}
{$WARN HRESULT_COMPAT ON}
{$WARN HIDING_MEMBER ON}
{$WARN HIDDEN_VIRTUAL ON}
{$WARN GARBAGE ON}
{$WARN BOUNDS_ERROR ON}
{$WARN ZERO_NIL_COMPAT ON}
{$WARN STRING_CONST_TRUNCED ON}
{$WARN FOR_LOOP_VAR_VARPAR ON}
{$WARN TYPED_CONST_VARPAR ON}
{$WARN ASG_TO_TYPED_CONST ON}
{$WARN CASE_LABEL_RANGE ON}
{$WARN FOR_VARIABLE ON}
{$WARN CONSTRUCTING_ABSTRACT ON}
{$WARN COMPARISON_FALSE ON}
{$WARN COMPARISON_TRUE ON}
{$WARN COMPARING_SIGNED_UNSIGNED ON}
{$WARN COMBINING_SIGNED_UNSIGNED ON}
{$WARN UNSUPPORTED_CONSTRUCT ON}
{$WARN FILE_OPEN ON}
{$WARN FILE_OPEN_UNITSRC ON}
{$WARN BAD_GLOBAL_SYMBOL ON}
{$WARN DUPLICATE_CTOR_DTOR ON}
{$WARN INVALID_DIRECTIVE ON}
{$WARN PACKAGE_NO_LINK ON}
{$WARN PACKAGED_THREADVAR ON}
{$WARN IMPLICIT_IMPORT ON}
{$WARN HPPEMIT_IGNORED ON}
{$WARN NO_RETVAL ON}
{$WARN USE_BEFORE_DEF ON}
{$WARN FOR_LOOP_VAR_UNDEF ON}
{$WARN UNIT_NAME_MISMATCH ON}
{$WARN NO_CFG_FILE_FOUND ON}
{$WARN IMPLICIT_VARIANTS ON}
{$WARN UNICODE_TO_LOCALE ON}
{$WARN LOCALE_TO_UNICODE ON}
{$WARN IMAGEBASE_MULTIPLE ON}
{$WARN SUSPICIOUS_TYPECAST ON}
{$WARN PRIVATE_PROPACCESSOR ON}
{$WARN UNSAFE_TYPE OFF}
{$WARN UNSAFE_CODE OFF}
{$WARN UNSAFE_CAST OFF}
{$WARN OPTION_TRUNCATED ON}
{$WARN WIDECHAR_REDUCED ON}
{$WARN DUPLICATES_IGNORED ON}
{$WARN UNIT_INIT_SEQ ON}
{$WARN LOCAL_PINVOKE ON}
{$WARN MESSAGE_DIRECTIVE ON}
{$WARN TYPEINFO_IMPLICITLY_ADDED ON}
{$WARN RLINK_WARNING ON}
{$WARN IMPLICIT_STRING_CAST ON}
{$WARN IMPLICIT_STRING_CAST_LOSS ON}
{$WARN EXPLICIT_STRING_CAST OFF}
{$WARN EXPLICIT_STRING_CAST_LOSS OFF}
{$WARN CVT_WCHAR_TO_ACHAR ON}
{$WARN CVT_NARROWING_STRING_LOST ON}
{$WARN CVT_ACHAR_TO_WCHAR ON}
{$WARN CVT_WIDENING_STRING_LOST ON}
{$WARN NON_PORTABLE_TYPECAST ON}
{$WARN XML_WHITESPACE_NOT_ALLOWED ON}
{$WARN XML_UNKNOWN_ENTITY ON}
{$WARN XML_INVALID_NAME_START ON}
{$WARN XML_INVALID_NAME ON}
{$WARN XML_EXPECTED_CHARACTER ON}
{$WARN XML_CREF_NO_RESOLVE ON}
{$WARN XML_NO_PARM ON}
{$WARN XML_NO_MATCHING_PARM ON}
{$WARN IMMUTABLE_STRINGS OFF}
unit ProcessOrdersUnit;

interface

uses dmJ2OrdersUnit, dmLocalUnit, System.SysUtils, TeraFunkUnit;

type
  IProcessOrders = interface
  ['{994753D1-949B-406E-B8C0-885BFB5BD144}']
      procedure ProcessOrders;
  end;


  TProcessOrders = class(TInterfacedObject, IProcessOrders)
    private
      FdmJ2Orders: TdmJ2Orders;
      procedure OpenWebOrders(LastOrderID: string);
      procedure OpenWebOrderLines(LastOrderLineID: integer);
      function GetLastProcessedWebOrderID: string;
      function GetLocalSalesOrderID(ExtSalesOrderID: string):integer;
      function CopyOrderHeaders(LastOrderID: string):boolean;
      function CopyOrderLines(LastOrderLineID: integer):boolean;
    public
      procedure ProcessOrders;
      constructor Create(sender: TObject; dmJ2Orders: TdmJ2Orders); overload;
  end;

implementation

uses
  Vcl.Dialogs;

{ TProcessOrders }

function TProcessOrders.CopyOrderHeaders(LastOrderID: string):boolean;
var
    LDeliveryName: string;
begin
    result:=true;
    try
        OpenWebOrders(LastOrderID);

        with FdmJ2Orders do
        begin
            FDQSalesOrders.Open;
            FDQWebOrders.Open;

            while not FDMemOrders.Eof do
            begin
                if FDMemOrderInfo.Active then
                   FDMemOrderInfo.Close;

                FDComOrderInfo.ParamByName('ORDERID').Value:=FDMemOrdersorder_id.Value;
                FDMemOrderInfo.Open;

                FDQSalesOrders.Insert;
                FDQSalesOrdersID.Value:=dmLocal.GetGenID('SALESORDERID_GEN');
                FDQSalesOrdersCUSTOMERID.Value:=GetCustIDFromExtCustID(FDMemOrdersuser_id.Value).ID;
                FDQSalesOrdersCREATEDBY.Value:=FDQSalesOrdersCUSTOMERID.AsString;
                FDQSalesOrdersDATECREATED.AsDateTime:=FDMemOrderscreated_on.AsDateTime;
                FDQSalesOrdersDATAAREAID.Value:=DATAAREAID;
                FDQSalesOrdersSTATUS.Value:= 1;  // 1 = Registered
                FDQSalesOrdersORDEREDBY.Value:=GetCustIDFromExtCustID(FDMemOrdersuser_id.Value).Name;
                FDQSalesOrdersCUSTREF.Value:='NET: '+FDMemOrdersorder_id.Value;
                FDQSalesOrdersDESCRIPTION.Value:=FDMemOrderscustomer_note.Value;
                FDQSalesOrdersDESIRDELIVDATE.AsDateTime:=Now;  // skal r�ttast

                //FDQSalesOrdersDELIVERICONTACTID.Value:=GetContactIdFromExtCustID(FDMemOrderInfoshipping_company.AsInteger);

                if FDMemOrderInfoshipping_company.Value > ' ' then
                   LDeliveryName:=FDMemOrderInfoshipping_company.Value+', '+FDMemOrderInfoshipping_first_name.Value+' '+FDMemOrderInfoshipping_last_name.Value
                else
                   LDeliveryName:=FDMemOrderInfoshipping_first_name.Value+' '+FDMemOrderInfoshipping_last_name.Value;  // kemur �r order info tabell

                FDQSalesOrdersDELIVERYNAME.Value:=LDeliveryName;
                FDQSalesOrdersDELIVERYADDRESS.Value:=FDMemOrderInfoshipping_address_1.Value+', '+FDMemOrderInfoshipping_address_2.Value;
                FDQSalesOrdersDELIVERYCITY.Value:=FDMemOrderInfoshipping_city.Value;
                FDQSalesOrdersPOSTCODE.Value:=FDMemOrderInfoshipping_zip.Value;
                FDQSalesOrders.Post;

                FDQWebOrders.Insert;
                FDQWebOrdersSALESORDERID.Value:=FDQSalesOrdersID.Value;
                FDQWebOrdersORDERID.Value:=FDMemOrdersorder_id.Value;
                FDQWebOrdersUSERID.Value:=FDMemOrdersuser_id.Value;
                FDQWebOrdersUSEREMAIL.Value:=FDMemOrdersuser_email.Value;
                FDQWebOrdersPAYMENTTYPE.Value:=FDMemOrdersorderpayment_type.Value;
                FDQWebOrdersSUBTOTAL.Value:=FDMemOrdersorder_subtotal.Value;
                FDQWebOrdersVAT.Value:=FDMemOrdersorder_tax.Value;
                FDQWebOrdersSHIPPING.Value:=FDMemOrdersorder_shipping.Value;
                FDQWebOrdersSHIPPINGVAT.Value:=FDMemOrdersorder_shipping_tax.Value;
                FDQWebOrdersDATAAREAID.Value:=DATAAREAID;
                FDQWebOrders.Post;

                FDMemOrders.Next;
            end;
        end;
    except
        result:=false;
    end;
end;


function TProcessOrders.CopyOrderLines(LastOrderLineID: integer):boolean;
var
    LLocalSalesOrderID: integer;
    ItemInfo: TLocalItem;
begin
    result:=true;
    try
        OpenWebOrderLines(LastOrderLineID);

        With FdmJ2Orders do
        begin
            FDQSalesOrderLines.Open;

            while not FDMemOrderLines.Eof do
            begin
                LLocalSalesOrderID:=GetLocalSalesOrderID(FDMemOrderLinesOrder_ID.Value);
                ItemInfo:=GetItemInfoFromSku(FDMemOrderLinesorderitem_sku.Value);

                if ItemInfo.ItemID = -1 then
                begin
                    try
                        if not FDQSalesOrderRemarks.active then
                           FDQSalesOrderRemarks.Open;

                        FDQSalesOrderRemarks.Insert;
                        FDQSalesOrderRemarksLINEID.Value:=dmLocal.GetGenID('GEN_SALESORDERREMARKID');
                        FDQSalesOrderRemarksSALESORDERID.Value:=LLocalSalesOrderID;
                        FDQSalesOrderRemarksREMARK.Value:=FDMemOrderLinesorderitem_sku.Value+'/'+FDMemOrderLinesorderitem_name.Value+': V�ra finnist ikki';;
                        FDQSalesOrderRemarksDATAAREAID.Value:=DATAAREAID;
                        FDQSalesOrderRemarks.Post;
                    except
                        LogFile(self,#10#13+
                            'SalesOrder: '+intToStr(LLocalSalesOrderID)+'; '+#10#13+
                            'WebOrderID: '+FDMemOrderLinesorder_id.asString+'; '+#10#13+
                            'SKU:        '+FDMemOrderLinesorderitem_sku.Value+'/'+FDMemOrderLinesorderitem_name.Value );

                        FDQSalesOrderRemarks.Cancel;
                    end;
                end
                else
                begin
                    FDQSalesOrderLines.Insert;
                    FDQSalesOrderLinesLineID.Value:= dmLocal.GetGenID('SALESORDLINEID_GEN');
                    FDQSalesOrderLinesSALESORDERID.Value:=LLocalSalesOrderID;
                    FDQSalesOrderLinesINTERNALITEMID.Value:=FDMemOrderLinesorderitem_sku.Value;
                    //FDQSalesOrderLinesBARCODE.Value:=
                    FDQSalesOrderLinesQUANTITY.Value:=FDMemOrderLinesorderitem_quantity.AsExtended;
                    FDQSalesOrderLinesSALESPRICE.Value:=FDMemOrderLinesorderitem_finalprice_without_tax.Value;
                    FDQSalesOrderLinesLINEDISCPERCENT.Value:=0;
                    FDQSalesOrderLinesLINEDISC.Value:=0;
                    FDQSalesOrderLinesSTATUS.Value:=1; // 1 = Registered
                    FDQSalesOrderLinesCOSTPRICE.Value:=ItemInfo.CostPrice;
                    FDQSalesOrderLinesITEMID.Value:=ItemInfo.ItemID;
                    FDQSalesOrderLinesUNIT.Value:=ItemInfo.ItemUnit;
                    FDQSalesOrderLinesVATTARIFF.Value:=ItemInfo.VatTariff;
                    FDQSalesOrderLinesITEMNAME.Value:=FDMemOrderLinesorderitem_name.Value;
                    FDQSalesOrderLinesDATECREATED.AsDateTime:=FDMemOrderLinescreated_on.Value;
                    FDQSalesOrderLinesCREATEDBY.Value:=FDMemOrderLinescreated_by.AsString;
                    FDQSalesOrderLinesEXTORDERLINEID.Value:=FDMemOrderLinesj2store_orderitem_id.Value;
                    FDQSalesOrderLinesDATAAREAID.Value:=DATAAREAID;
                    FDQSalesOrderLines.Post;
                end;

                FDMemOrderLines.Next;
            end;
        end;
    except
        on E: Exception do
        begin
            showmessage(e.Message+': '+FdmJ2Orders.FDMemOrderLinesorderitem_sku.Value);
            result:=false;
        end;
    end;
end;

constructor TProcessOrders.Create(sender: TObject; dmJ2Orders: TdmJ2Orders);
begin
    inherited create;
    FdmJ2Orders:= dmJ2Orders;
end;


function TProcessOrders.GetLastProcessedWebOrderID: string;
begin
    with FdmJ2Orders do
    begin
      result:= GetLastWebSalesOrderID;
    end;
end;



function TProcessOrders.GetLocalSalesOrderID(ExtSalesOrderID: string): integer;
begin
    with FdmJ2Orders do
    begin
        FDQGetLocalSalesOrderID.Close;
        FDQGetLocalSalesOrderID.ParamByName('EXTSALESORDERID').AsString:=ExtSalesOrderID;
        FDQGetLocalSalesOrderID.Open;

        Result:=FDQGetLocalSalesOrderIDID.value;
        FDQGetLocalSalesOrderID.Close;
    end;
end;

procedure TProcessOrders.OpenWebOrderLines(LastOrderLineID: integer);
begin
    with FdmJ2Orders do
    begin
        if FDMemOrderLines.Active then
           FDMemOrderLines.Close;

        FDMemOrderLines.ParamByName('LASTORDERLINEID').Value:=LastOrderLineID;
        FDMemOrderLines.Open;
    end;
end;

procedure TProcessOrders.OpenWebOrders(LastOrderID: string);
begin
    with FdmJ2Orders do
    begin
        if FDMemOrders.Active then
           FDMemOrders.Close;

        FDComOrders.ParamByName('LASTORDERID').Value:=LastOrderID;
        FDMemOrders.Open;
    end;
end;

procedure TProcessOrders.ProcessOrders;
var
    LastProcessedOrder: string;
    LastProcessedLine: integer;
    CopySuccess: boolean;
begin
    // Import weborder headers
    FdmJ2Orders.FDTransOrders.StartTransaction;
    LastProcessedOrder:=GetLastProcessedWebOrderID;
    CopySuccess:=CopyOrderHeaders(LastProcessedOrder);

    if FdmJ2Orders.FDTransOrders.Active then
    begin
       if CopySuccess = False then
       begin
          FdmJ2Orders.FDTransOrders.Rollback;
          Exit
       end
       else
          FdmJ2Orders.FDTransOrders.Commit;
    end;

    // Import weborderLines
    FdmJ2Orders.FDTransOrders.StartTransaction;
    LastProcessedLine:=FdmJ2Orders.GetLastWebSalesOrderLineID;
    CopySuccess:=CopyOrderLines(LastProcessedLine);

    if FdmJ2Orders.FDTransOrders.Active then
    begin
       if CopySuccess = False then
          FdmJ2Orders.FDTransOrders.Rollback
       else
          FdmJ2Orders.FDTransOrders.Commit;
    end;
end;

end.

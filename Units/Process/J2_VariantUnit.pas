unit J2_VariantUnit;

interface

uses dmMainUnit;

type
  IJ2Variant = interface['{B0161D3F-FF2A-4ED8-AE3E-8D1834DC4446}']
     procedure ExportToWeb(ProductID: integer; SKU, UPC: string; Price: Currency);
  end;

  TJ2Variant = class(TInterfacedObject, IJ2Variant)
    private
      FVariantID: integer;
      FProductID: integer;
      FIsMaster: integer;
      FSKU: string;
      FUPC: string;
      FPrice: Currency;
      FPricingCalculator: string;
      FShipping: integer;
      procedure SaveToDB;
    public
      constructor create;
      procedure ExportToWeb(ProductID: integer; SKU, UPC: string; Price: Currency);
  end;

implementation

{ TJ2Variant }

constructor TJ2Variant.create;
begin
    inherited;
    FIsMaster:=1;
    FPricingCalculator:='standard';
    FShipping:=1;
end;

procedure TJ2Variant.ExportToWeb(ProductID: integer; SKU, UPC: string;
  Price: Currency);
begin
    FProductID:=ProductID;
    FSKU:=SKU;
    FUPC:=UPC;
    FPrice:=Price;
    SaveToDB;
end;

procedure TJ2Variant.SaveToDB;
begin
    with dmMain do
    begin
        if FDMemVariants.Active then
           FDMemVariants.Close;

        FDMemVariants.Open;

        if FDMemVariants.Locate('product_id', FProductID,[]) then
           FDMemVariants.Edit
        else
           FDMemVariants.Insert;

        FDMemVariantsproduct_id.Value:=FProductID;
        FDMemVariantsis_master.Value:=FIsMaster;
        FDMemVariantssku.Value:=FSKU;
        FDMemVariantsupc.Value:=FUPC;
        FDMemVariantsprice.Value:=FPrice;
        FDMemVariantspricing_calculator.Value:=FPricingCalculator;
        FDMemVariantsshipping.Value:=FShipping;
        FDMemVariants.Post;

        if FDConnection1.Connected then
           FDConnection1.Connected:=False;
    end;
end;

end.

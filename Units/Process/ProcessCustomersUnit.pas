unit ProcessCustomersUnit;

interface

uses dmJ2OrdersUnit, dmLocalUnit;

type
  ICopyCustomers = interface
  ['{694962C5-FFCA-4C8D-8D5C-331A9A052453}']
      procedure ProcessCustomers;
  end;

  TCopyCustomers = class(TInterFacedObject, ICopyCustomers)
    private
      FdmJ2Orders: TdmJ2Orders;
      function GetLastUpdate(TableID: integer): TDateTime;
      procedure ImportDeliveryAddresses;
      procedure ImportCustomers;
    public
      procedure ProcessCustomers;
      constructor Create(sender: TObject; dmJ2Orders: TdmJ2Orders); overload;
  end;


implementation

uses
  System.Variants, Vcl.Dialogs, System.SysUtils;

{ TCopyCustomers }

constructor TCopyCustomers.Create(sender: TObject; dmJ2Orders: TdmJ2Orders);
begin
    FdmJ2Orders:=dmJ2Orders;
end;

function TCopyCustomers.GetLastUpdate(TableID: integer):TDateTime;
begin
    with FdmJ2Orders do
    begin
        FDQGetLastSync.Open;

        if FDQGetLastSync.Locate('TABLEID;DIRECTION',VarArrayOf([TableID,'IMPORT']),[]) then
        begin
            if not FDQGetLastSyncLASTUPDATE.IsNull then
                result:=FDQGetLastSyncLASTUPDATE.AsDateTime
            else
                result:=0;
        end
        else
        begin
            showmessage('No job has been setup for customer sync.');
            result:=0;
        end;
    end;
end;

procedure TCopyCustomers.ImportCustomers;
var
    LastUpdate: TDateTime;
begin
    with FdmJ2Orders do
    begin
        LastUpdate:=GetLastUpdate(7);  // 7 = Customertable (KUNDAR)

        if FDMemUsers.Active then
            FDMemUsers.Close;

        FDComUsers.ParamByName('LASTUPDATE').Value:=LastUpdate;
        FDMemUsers.Open;
        FDMemUsers.First;

        while not FDMemUsers.Eof do
        begin
            if (FDMemUsersid.Value > 0) then
            begin
                FDQCustomers.Close;
                FDQCustomers.ParamByName('EXTCUSTNO').Value:=FDMemUsersid.Value;
                FDQCustomers.Open;

                if (FDQCustomers.RecordCount > 0) then
                   FDQCustomers.Edit
                else
                begin
                   FDQCustomers.Insert;
                   FDQCustomersNR.Value:=dmLocal.GetGenID('KUNDANR_GEN');
                end;

                FDQCustomersKONTONR.Value:=FDMemUsersid.AsString;
                FDQCustomersEXTKUNDANR.Value:=FDMemUsersid.AsString;
                FDQCustomersNAVN.Value:=FDMemUsersname.Value;
                //FDQCustomersBUDSTADUR.Value:=FDMemUsersaddress_1.Value;
                //FDQCustomersACCADDR01.Value:=FDMemUserscompany.Value;
                //FDQCustomersACCADDR02.Value:=FDMemUsersaddress_2.Value;
                //FDQCustomersBYGDBYUR.Value:=FDMemUserscity.Value;
                //FDQCustomersPOSTNR.Value:=FDMemUserszip.Value;
                //FDQCustomersTLF.Value:=FDMemUsersphone_1.Value;
                //FDQCustomersFARTLF.Value:=FDMemUsersphone_2.Value;
                //FDQCustomersEPOSTUR.Value:=FDMemUsersemail.Value;
                FDQCustomersDATAAREAID.Value:=DATAAREAID;
                FDQCustomers.Post;
            end;

            FDMemUsers.Next;
        end;

        FDMemUsers.Close;
    end;
end;

procedure TCopyCustomers.ImportDeliveryAddresses;
var
    LastUpdate: TDateTime;
    Customer: TCustomer;
begin
    with FdmJ2Orders do
    begin
         LastUpdate:=GetLastUpdate(29);  // 29 = CustDeliveryAddres

         if FDMemDelivAddres.Active then
            FDMemDelivAddres.Close;

         FDMemDelivAddres.ParamByName('LASTUPDATE').Value:=LastUpdate;
         FDMemDelivAddres.Open;
         FDMemDelivAddres.First;

         while not FDMemDelivAddres.eof do
         begin
            Customer:=GetCustIDFromExtCustID(FDMemDelivAddresuser_id.Value);

            if (Customer.ID > 0) then
            begin
                FDQDeliveryAddress.Close;
                FDQDeliveryAddress.ParamByName('EXTADDRESSID').Value:=FDMemDelivAddresj2store_address_id.Value;
                FDQDeliveryAddress.Open;

                if (FDQDeliveryAddress.RecordCount > 0) then
                begin
                    FDQDeliveryAddress.Edit;
                end
                else
                begin
                    FDQDeliveryAddress.Insert;
                    FDQDeliveryAddressID.Value:=dmLocal.GetGenID('CONTACTSID_GEN');
                    FDQDeliveryAddressEXTADDRESSID.Value:=FDMemDelivAddresj2store_address_id.Value;
                    FDQDeliveryAddressDATAAREAID.Value:=DATAAREAID;
                end;

                FDQDeliveryAddressREFID.Value:=Customer.ID.ToString;
                FDQDeliveryAddressREFTABLE.Value:= TABLE_CUSTOMERS;
                FDQDeliveryAddressFIRSTNAME.Value:=FDMemDelivAddresfirst_name.Value;
                FDQDeliveryAddressLASTNAME.Value:=FDMemDelivAddreslast_name.Value;
                FDQDeliveryAddressADDRESS1.Value:=FDMemDelivAddresaddress_1.Value;
                FDQDeliveryAddressADDRESS2.Value:=FDMemDelivAddresaddress_2.Value;
                FDQDeliveryAddressCITY.Value:=FDMemDelivAddrescity.Value;
                FDQDeliveryAddressPOSTCODE.Value:=FDMemDelivAddreszip.Value;
                FDQDeliveryAddressEMAIL.Value:=FDMemDelivAddresemail.Value;
                FDQDeliveryAddressTELNO.Value:=FDMemDelivAddresphone_1.Value;
                FDQDeliveryAddressTELNO2.Value:=FDMemDelivAddresphone_2.Value;
                FDQDeliveryAddressCOMPANY.Value:=FDMemDelivAddrescompany.Value;
                FDQDeliveryAddress.Post;
            end;

            FDMemDelivAddres.Next;
         end;
    end;
end;

procedure TCopyCustomers.ProcessCustomers;
begin
    ImportCustomers;
    ImportDeliveryAddresses;
end;

end.

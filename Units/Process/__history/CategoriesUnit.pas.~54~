unit CategoriesUnit;

interface

uses dmLocalUnit, ItemGroupUnit, AssetUnit, ErrorLogUnit, dmMainUnit;

type

  TCategories = record
    private
      FWebSettings: TWebSettings;
      FItemGroup: TItemGroup;
      Asset: TAssets;
      FErrorLog: TErrorLog;
      ID: integer;
      FParentID: integer;
      FAssetID: integer;
      FLevel: integer;
      FPath: string;
      Title: string;
      Alias: string;
      Description: string;
      PublishedOnWeb: Boolean;
      Extension: string;
      FParams: string;
      Access: smallint;
      Lft: integer;
      Rgt: integer;
      function GetNewID: integer;
      procedure CategoriesInit;
      procedure CreateCategory;
      procedure GetParentID;
      procedure OpenMemCatTable;
      function SaveToDb(WebSettings: TWebSettings): Integer;
      procedure UpdateAssetID(CatID: integer);
    public
      procedure CreateRootCategory(WebSettings: TWebSettings);
      function ExportCatToWeb(ItemGroup: TItemGroup; WebSettings: TWebSettings; ErrorLog: TErrorLog): integer;
      function GetCategoryIDByItemGroupID(WebSettings: TWebSettings; ItemGroupID: string): integer;
      Property AssetID: integer read FAssetID;
  end;

implementation

uses
  System.SysUtils;

{ TCategories }

procedure TCategories.CategoriesInit;
var
  AssetID: integer;
begin
    PublishedOnWeb:=True;
    Extension:='com_content';
    FParams:='{"category_layout":"","image":"","image_alt":""}';
    Access:=1;
    Lft:=0;
    Rgt:=0;
    Description:=' ';
end;

procedure TCategories.CreateCategory;
var
  ItemGrpRec: TItemGrpRec;
begin
   ItemGrpRec:=FItemGroup.GetItemGroup;
   Alias:=FWebSettings.Cat_AliasPrefix+ItemGrpRec.GroupID;
   GetParentID; //ParentID:= 10;
   FLevel:= ItemGrpRec.GroupLevel+1;
   Lft:=ItemGrpRec.MenuOrder+MENU_ORDER_CONST;
   Rgt:=ItemGrpRec.MenuSpan+MENU_ORDER_CONST;
   FPath:= FWebSettings.Menu_RootTitle.ToLower+'/'+ItemGrpRec.GroupPath.ToLower;
   Title:=ItemGrpRec.GroupName;
end;

procedure TCategories.CreateRootCategory(WebSettings: TWebSettings);
var
    CatID: integer;
begin
    FWebSettings:=WebSettings;
    CategoriesInit;
    Alias:=FWebSettings.Cat_AliasPrefix+'root'+dmLocal.FDQItemGrpHeaderHEADERID.AsString;
    FAssetID:=0;  //Asset.CreateRootAsset(Alias, dmLocal.FDQItemGrpHeaderHEADERNAME.AsString, '', FWebSettings);
    FParentID:=1;
    FLevel:=1;
    FPath:=Alias;   //dmLocal.FDQItemGrpHeaderHEADERNAME.AsString.ToLower;
    Title:=dmLocal.FDQItemGrpHeaderHEADERNAME.AsString;
    Lft:=1000;
    Rgt:=1001;
    CatID:= SaveToDb(WebSettings);

    FAssetID:=Asset.CreateRootAsset(Alias, FWebSettings.Asset_CategoryNamePrefix+intToStr(CatID), '', FWebSettings);
    //Asset.UpdateAssetName(FassetID,FWebSettings.Asset_CategoryNamePrefix+intToStr(CatID));
    UpdateAssetID(CatID);

    with dmLocal do
    begin
        FDQWebSettings.Edit;
        FDQWebSettingsCAT_ROOTID.Value:=CatID;
        FDQWebSettingsMENU_ROOTTITLE.Value:=Alias;  //FDQItemGrpHeaderHEADERNAME.AsString;
        FDQWebSettings.Post;
    end;
end;

function TCategories.ExportCatToWeb(ItemGroup: TItemGroup; WebSettings: TWebSettings; ErrorLog: TErrorLog): integer;
var
  ItemGrpRec: TItemGrpRec;
begin
    FWebSettings:=WebSettings;
    FItemGroup:=ItemGroup;
    ItemGrpRec:=FItemGroup.GetItemGroup;
    FErrorLog:= ErrorLog;
    CategoriesInit;
    CreateCategory;
    FAssetID:=-1;
    result:= SaveToDb(WebSettings);
    FAssetID:=Asset.CreateAsset(FWebSettings.Asset_CategoryNamePrefix+intToStr(result), ItemGrpRec.GroupName, FWebSettings.Asset_CategoryNamePrefix+IntToStr(FParentID), FWebSettings);
    UpdateAssetID(result);
    // Update Assetname to have CategoryID in the name
    //Asset.UpdateAssetName(FassetID,FWebSettings.Asset_CategoryNamePrefix+intToStr(result));
end;

function TCategories.GetCategoryIDByItemGroupID(WebSettings: TWebSettings; ItemGroupID: string): integer;
begin
    FWebSettings:=WebSettings;
    //FWebSettings.Cat_AliasPrefix
    OpenMemCatTable;

    with dmMain do
    begin
        if FDMemCategories.Locate('alias',FWebSettings.Cat_AliasPrefix+ItemGroupID) then
           result:=FDMemCategoriesid.Value
        else
           result:=-1;
    end;
end;

function TCategories.SaveToDb(WebSettings: TWebSettings): Integer;
begin
    with dmMain do
    begin
        if not FDMemCategories.Active then
            FDMemCategories.Open;

        if FDMemCategories.Locate('alias', Alias, []) then
        begin
            FDMemCategories.Edit;
            ID := FDMemCategoriesid.Value;
        end
        else
        begin
            FDMemCategories.Insert;
            ID := GetNewID;
            FDMemCategoriesmodified_user_id.Value:= 953;

        end;

        result := ID;
        FDMemCategoriesid.Value := ID;
        FDMemCategoriesAsset_id.Value := FAssetID;
        FDMemCategoriesParent_id.Value := FParentID;
        FDMemCategoriesLft.Value := Lft;
        FDMemCategoriesRgt.Value := Rgt;
        FDMemCategoriesLevel.Value := FLevel;
        FDMemCategoriesPath.Value := FPath;
        FDMemCategoriesExtension.Value := Extension;
        FDMemCategoriesTitle.Value := Title;
        FDMemCategoriesAlias.Value := Alias;
        //FDMemCategoriesnote.Value:=' ';
        FDMemCategoriesdescription.Value := Description;
        FDMemCategoriespublished.Value := PublishedOnWeb;
        FDMemCategoriesaccess.Value := Access;
        FDMemCategoriesparams.Value := FParams;
        FDMemCategoriesmodified_time.Value:=now;
        FDMemCategorieslanguage.Value := WebSettings.Language;
        FDMemCategories.Post;

        if (FDMemCategories.ChangeCount > 0) then
           FDMemCategories.ApplyUpdates(0);

        FDMemCategories.Close;
        FDMemCategories.Open;

        if FDConnection1.Connected then
            FDConnection1.Connected := False;
    end;
end;

procedure TCategories.UpdateAssetID(CatID: integer);
begin
    with dmMain do
    begin
        if FDMemCategories.Locate('id', CatID, []) then
        begin
            FDMemCategories.Edit;
            FDMemCategoriesAsset_id.Value := FAssetID;
            FDMemCategories.Post;

            ApplyMemTable(FDMemCategories);
        end;
    end;
end;

function TCategories.GetNewID: integer;
begin
    with dmMain do
    begin
        fdqGetMaxID.Open;

        if not FDQGetMaxID.Fields[0].isNull  then
            result:=FDQGetMaxID.Fields[0].Value+1
        else
            result:=1;

        fdqGetMaxID.Close;

        if FDConnection1.Connected then
           FDConnection1.Close;
    end;
end;



procedure TCategories.GetParentID;
var
  ItemGrpRec: TItemGrpRec;
begin
    with dmMain do
    begin
        OpenMemCatTable;
        ItemGrpRec:=FItemGroup.GetItemGroup;

        if (trim(ItemGrpRec.ParentGroup) = '') then
          FParentID:=FWebSettings.Cat_RootID
        else if FDMemCategories.Locate('alias',FWebSettings.Cat_AliasPrefix+ItemGrpRec.ParentGroup,[]) then
           FParentID:=FDMemCategoriesid.Value
        else
        begin
           FErrorLog.FAddError('Category '+ItemGrpRec.GroupID, 'ParentCatetory er ikki uppr�tta. ');
           FParentID:=-1;
        end;
    end;
end;


procedure TCategories.OpenMemCatTable;
begin
    with dmMain do
    begin
        if not FDMemCategories.Active then
           FDMemCategories.Active:=True;

        if FDConnection1.Connected then
            FDConnection1.Close;
    end;
end;

end.

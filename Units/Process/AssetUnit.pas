unit AssetUnit;

interface

uses dmLocalUnit, ItemGroupUnit, dmMainUnit;

type

  TAssets = record
    private
      FWebSettings: TWebSettings;
      //FItemGroup: TItemGrpRec;
      AssetID: integer;
      FLft: integer;
      FRgt: integer;
      Rules: string;
      FParentID: integer;
      FParentLevel: integer;
      FItemParentID: string;
      FTitle: string;
      FLevel: integer;
      //Fname: string;
      FAssetName: string;
      procedure AssetInit;
      procedure ExportToWeb;
      procedure GetParentID(AssetName: string);
    public
      function CreateAsset(AssetName, ItemName, ItemParent:string; WebSettings: TWebSettings): integer;
      procedure UpdateAssetName(AssetID: integer; NewName: string);
      function CreateRootAsset(AssetName: string; ItemName, ItemParent:string; WebSettings: TWebSettings): integer;
  end;

implementation

uses
  System.SysUtils;


{ TAssets }

procedure TAssets.AssetInit;
begin
    FLft:=0;
    FRgt:=0;
    Rules:='{}';
end;

function TAssets.CreateAsset(AssetName, ItemName, ItemParent:string; WebSettings: TWebSettings): integer;
begin
    FWebSettings:= WebSettings;
    AssetInit;
    FTitle:=ItemName;
    FItemParentID:=ItemParent;
    FAssetName:= AssetName; //FWebSettings.Asset_CategoryNamePrefix+intToStr(CategoryID);
    GetParentID(ItemParent);
    FLevel:=FParentLevel+1; //WebSettings.Asset_RootLevel;
    ExportToWeb;
    result:=AssetID;
end;

function TAssets.CreateRootAsset(AssetName, ItemName, ItemParent: string;
  WebSettings: TWebSettings): integer;
begin
    FWebSettings:= WebSettings;
    AssetInit;
    FAssetName:=AssetName;
    FTitle:=ItemName;
    FItemParentID:=ItemParent;
    GetParentID('com_content');
    FLevel:=FWebSettings.Asset_RootLevel;
    //FLevel:=FParentLevel+1;

    ExportToWeb;

    with dmLocal do
    begin
        FDQWebSettings.Edit;
        FDQWebSettingsASSET_ROOTID.Value:=AssetID;
        FDQWebSettingsASSET_FIRSTLEVEL.Value:=FLevel;  //FDQItemGrpHeaderHEADERNAME.AsString;
        FDQWebSettings.Post;
    end;

    result:=AssetID;
end;

procedure TAssets.ExportToWeb;
begin
    with dmMain do
    begin
        if FDMemAssets.Active then
           FDMemAssets.Close;

        FDMemAssets.Open;

        if FDMemAssets.Locate('name',FAssetName ,[]) then
        begin
           AssetID:=FDMemAssetsid.Value;
           FDMemAssets.Edit;
        end
        else
        begin
            FDMemAssets.Insert;
        end;

        //FDMemAssetsid.Value:=ID;
        FDMemAssetsparent_id.Value:=FWebSettings.Asset_RootID;
        FDMemAssetsparent_id.Value:=FParentID;
        FDMemAssetslft.Value:=FLft;
        FDMemAssetsrgt.Value:=FRgt;
        FDMemAssetslevel.Value:=FLevel;
        FDMemAssetsname.Value:=FAssetName; // RelatedAlias;
        FDMemAssetstitle.Value:=FTitle;
        FDMemAssetsrules.Value:=Rules;
        FDMemAssets.Post;

        if (FDMemAssets.ChangeCount > 0) then
            FDMemAssets.ApplyUpdates(0);

        AssetID:=FDMemAssetsid.Value;
        FDMemAssets.Close;
        FDMemAssets.Open;

        if FDConnection1.Connected then
           FDConnection1.Connected:=False;
    end;
end;


procedure TAssets.GetParentID(AssetName: string);
begin
    with dmMain do
    begin
        if not FDMemAssets.Active then
           FDMemAssets.Active:=True;

        if FDMemAssets.Locate('name',AssetName,[]) then
        begin
           FParentID:=FDMemAssetsid.Value;
           FParentLevel:=FDMemAssetslevel.Value;
        end
        else if trim(FItemParentID) = '' then
        begin
           FParentID:=FWebSettings.Asset_RootID;
           FParentLevel:=FWebSettings.Asset_RootLevel;  //FDMemAssetslevel.Value;
        end
        else
           FParentID:=-1;
    end;
end;

procedure TAssets.UpdateAssetName(AssetID: integer; NewName: string);
begin
    with dmMain do
    begin
        if FDMemAssets.Locate('ID',AssetID,[]) then
        begin
            FDMemAssets.Edit;
            FDMemAssetsname.Value:=NewName;
            FDMemAssets.Post;

            ApplyMemTable(FDMemAssets);
        end;
    end;
end;

end.

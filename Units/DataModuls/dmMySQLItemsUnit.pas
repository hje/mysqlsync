unit dmMySQLItemsUnit;

interface

uses
  System.SysUtils, System.Classes, dmMainUnit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TdmMySQLItems = class(TDataModule)
    FDQGetContentByAlias: TFDQuery;
    FDComContentInsert: TFDCommand;
    FDComContentUpdate: TFDCommand;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmMySQLItems: TdmMySQLItems;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.

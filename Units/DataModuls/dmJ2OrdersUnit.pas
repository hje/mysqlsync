unit dmJ2OrdersUnit;

interface

uses
  System.SysUtils, System.Classes, dmMainUnit, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, dmLocalUnit;

type
  TLocalItem = record
    ItemID: integer;
    VatTariff: Currency;
    CostPrice: Currency;
    ItemUnit: string
  end;

  TCustomer = record
    ID: integer;
    Name: string;
  end;

  TdmJ2Orders = class(TDataModule)
    FDQJ2OrderLines: TFDQuery;
    FDQJ2OrderInfos: TFDQuery;
    FDQJ2OrderHistories: TFDQuery;
    FDQSalesOrders: TFDQuery;
    FDQGetMax: TFDQuery;
    FDQWebOrders: TFDQuery;
    FDQSalesOrderLines: TFDQuery;
    FDComUsers: TFDCommand;
    FDTAdaptUsers: TFDTableAdapter;
    FDMemUsers: TFDMemTable;
    FDQCustomers: TFDQuery;
    FDQCustomersNAVN: TStringField;
    FDQCustomersBUDSTADUR: TStringField;
    FDQCustomersPOSTBOKS: TStringField;
    FDQCustomersPOSTNR: TStringField;
    FDQCustomersBYGDBYUR: TStringField;
    FDQCustomersLAND: TStringField;
    FDQCustomersTLF: TStringField;
    FDQCustomersFARTLF: TStringField;
    FDQCustomersFAX: TStringField;
    FDQCustomersEPOSTUR: TStringField;
    FDQCustomersGJALDSTREYTIR: TSmallintField;
    FDQCustomersEXTKUNDANR: TStringField;
    FDQCustomersKONTONR: TStringField;
    FDQCustomersVIDMERKING: TStringField;
    FDQCustomersNR: TIntegerField;
    FDQCustomersACCOPTION: TStringField;
    FDQCustomersMAXKREDITIKASSA: TBCDField;
    FDQCustomersSALDO: TBCDField;
    FDQCustomersACCADDR01: TStringField;
    FDQCustomersACCADDR02: TStringField;
    FDQCustomersACCADDR03: TStringField;
    FDQCustomersACCADDR04: TStringField;
    FDQCustomersSTONGD: TSmallintField;
    FDQCustomersKUNDABOLKUR: TIntegerField;
    FDQCustomersDATAAREAID: TSmallintField;
    FDQCustomersPRISBOLKUR: TIntegerField;
    FDQSyncDBClients: TFDQuery;
    FDQGetLastSync: TFDQuery;
    FDQGetLastSyncTABLEID: TIntegerField;
    FDQGetLastSyncLASTUPDATE: TSQLTimeStampField;
    FDQGetLastSyncDIRECTION: TStringField;
    FDComOrders: TFDCommand;
    FDTAdaptOrders: TFDTableAdapter;
    FDMemOrders: TFDMemTable;
    FDMemOrdersj2store_order_id: TFDAutoIncField;
    FDMemOrdersorder_id: TStringField;
    FDMemOrdersorder_type: TStringField;
    FDMemOrdersparent_id: TIntegerField;
    FDMemOrderssubscription_id: TIntegerField;
    FDMemOrderscart_id: TLongWordField;
    FDMemOrdersinvoice_prefix: TStringField;
    FDMemOrdersinvoice_number: TIntegerField;
    FDMemOrderstoken: TStringField;
    FDMemOrdersuser_id: TIntegerField;
    FDMemOrdersuser_email: TStringField;
    FDMemOrdersorder_total: TBCDField;
    FDMemOrdersorder_subtotal: TBCDField;
    FDMemOrdersorder_subtotal_ex_tax: TBCDField;
    FDMemOrdersorder_tax: TBCDField;
    FDMemOrdersorder_shipping: TBCDField;
    FDMemOrdersorder_shipping_tax: TBCDField;
    FDMemOrdersorder_discount: TBCDField;
    FDMemOrdersorder_discount_tax: TBCDField;
    FDMemOrdersorder_credit: TBCDField;
    FDMemOrdersorder_refund: TBCDField;
    FDMemOrdersorder_surcharge: TBCDField;
    FDMemOrdersorder_fees: TBCDField;
    FDMemOrdersorderpayment_type: TStringField;
    FDMemOrderstransaction_id: TStringField;
    FDMemOrderstransaction_status: TStringField;
    FDMemOrderstransaction_details: TMemoField;
    FDMemOrderscurrency_id: TIntegerField;
    FDMemOrderscurrency_code: TStringField;
    FDMemOrderscurrency_value: TFMTBCDField;
    FDMemOrdersip_address: TStringField;
    FDMemOrdersis_shippable: TIntegerField;
    FDMemOrdersis_including_tax: TIntegerField;
    FDMemOrderscustomer_note: TMemoField;
    FDMemOrderscustomer_language: TStringField;
    FDMemOrderscustomer_group: TStringField;
    FDMemOrdersorder_state_id: TIntegerField;
    FDMemOrdersorder_state: TStringField;
    FDMemOrdersorder_params: TMemoField;
    FDMemOrderscreated_on: TDateTimeField;
    FDMemOrderscreated_by: TIntegerField;
    FDMemOrdersmodified_on: TDateTimeField;
    FDMemOrdersmodified_by: TIntegerField;
    FDQSalesOrdersID: TIntegerField;
    FDQSalesOrdersCUSTOMERID: TIntegerField;
    FDQSalesOrdersCREATEDBY: TStringField;
    FDQSalesOrdersDATAAREAID: TSmallintField;
    FDQSalesOrdersDATECREATED: TSQLTimeStampField;
    FDQSalesOrdersSTATUS: TIntegerField;
    FDQSalesOrdersCUSTREF: TStringField;
    FDQSalesOrdersORDEREDBY: TStringField;
    FDQSalesOrdersDESCRIPTION: TStringField;
    FDQSalesOrdersDESIRDELIVDATE: TSQLTimeStampField;
    FDQSalesOrdersSUBSCRIPTIONID: TIntegerField;
    FDQSalesOrdersDELIVERYNAME: TStringField;
    FDQSalesOrdersDELIVERYADDRESS: TStringField;
    FDQSalesOrdersDELIVERYCITY: TStringField;
    FDQSalesOrdersPOSTCODE: TStringField;
    FDComOrderInfo: TFDCommand;
    FDMemOrderInfo: TFDMemTable;
    FDTAdaptOrderInfo: TFDTableAdapter;
    FDMemOrderInfoj2store_orderinfo_id: TFDAutoIncField;
    FDMemOrderInfoorder_id: TStringField;
    FDMemOrderInfobilling_company: TStringField;
    FDMemOrderInfobilling_last_name: TStringField;
    FDMemOrderInfobilling_first_name: TStringField;
    FDMemOrderInfobilling_middle_name: TStringField;
    FDMemOrderInfobilling_phone_1: TStringField;
    FDMemOrderInfobilling_phone_2: TStringField;
    FDMemOrderInfobilling_fax: TStringField;
    FDMemOrderInfobilling_address_1: TStringField;
    FDMemOrderInfobilling_address_2: TStringField;
    FDMemOrderInfobilling_city: TStringField;
    FDMemOrderInfobilling_zone_name: TStringField;
    FDMemOrderInfobilling_country_name: TStringField;
    FDMemOrderInfobilling_zone_id: TIntegerField;
    FDMemOrderInfobilling_country_id: TIntegerField;
    FDMemOrderInfobilling_zip: TStringField;
    FDMemOrderInfobilling_tax_number: TStringField;
    FDMemOrderInfoshipping_company: TStringField;
    FDMemOrderInfoshipping_last_name: TStringField;
    FDMemOrderInfoshipping_first_name: TStringField;
    FDMemOrderInfoshipping_middle_name: TStringField;
    FDMemOrderInfoshipping_phone_1: TStringField;
    FDMemOrderInfoshipping_phone_2: TStringField;
    FDMemOrderInfoshipping_fax: TStringField;
    FDMemOrderInfoshipping_address_1: TStringField;
    FDMemOrderInfoshipping_address_2: TStringField;
    FDMemOrderInfoshipping_city: TStringField;
    FDMemOrderInfoshipping_zip: TStringField;
    FDMemOrderInfoshipping_zone_name: TStringField;
    FDMemOrderInfoshipping_country_name: TStringField;
    FDMemOrderInfoshipping_zone_id: TIntegerField;
    FDMemOrderInfoshipping_country_id: TIntegerField;
    FDMemOrderInfoshipping_id: TStringField;
    FDMemOrderInfoshipping_tax_number: TStringField;
    FDMemOrderInfoall_billing: TMemoField;
    FDMemOrderInfoall_shipping: TMemoField;
    FDMemOrderInfoall_payment: TMemoField;
    FDComOrderLines: TFDCommand;
    FDTAdaptOrderLines: TFDTableAdapter;
    FDMemOrderLines: TFDMemTable;
    FDQSalesOrderLinesLINEID: TIntegerField;
    FDQSalesOrderLinesSALESORDERID: TIntegerField;
    FDQSalesOrderLinesQUANTITY: TBCDField;
    FDQSalesOrderLinesSTATUS: TIntegerField;
    FDQSalesOrderLinesSALESPRICE: TBCDField;
    FDQSalesOrderLinesITEMID: TIntegerField;
    FDQSalesOrderLinesITEMNAME: TStringField;
    FDQSalesOrderLinesCOSTPRICE: TCurrencyField;
    FDQSalesOrderLinesLINEDISC: TCurrencyField;
    FDQSalesOrderLinesUNIT: TStringField;
    FDQSalesOrderLinesVATTARIFF: TCurrencyField;
    FDQSalesOrderLinesLINEDISCPERCENT: TSingleField;
    FDQSalesOrderLinesBARCODE: TStringField;
    FDQSalesOrderLinesINTERNALITEMID: TStringField;
    FDQSalesOrderLinesLINEAMOUNT: TFMTBCDField;
    FDQSalesOrderLinesLINEAMOUNTVDISC: TFMTBCDField;
    FDQSalesOrderLinesVATAMOUNT: TFMTBCDField;
    FDQSalesOrderLinesLINEAMOUNTINCVAT: TFMTBCDField;
    FDQSalesOrderLinesDATAAREAID: TSmallintField;
    FDQGetLocalSalesOrderID: TFDQuery;
    FDQGetLocalSalesOrderIDID: TIntegerField;
    FDMemOrderLinesj2store_orderitem_id: TFDAutoIncField;
    FDMemOrderLinesorder_id: TStringField;
    FDMemOrderLinesorderitem_type: TStringField;
    FDMemOrderLinescart_id: TIntegerField;
    FDMemOrderLinescartitem_id: TLongWordField;
    FDMemOrderLinesproduct_id: TIntegerField;
    FDMemOrderLinesproduct_type: TStringField;
    FDMemOrderLinesvariant_id: TIntegerField;
    FDMemOrderLinesvendor_id: TIntegerField;
    FDMemOrderLinesorderitem_sku: TStringField;
    FDMemOrderLinesorderitem_name: TStringField;
    FDMemOrderLinesorderitem_attributes: TMemoField;
    FDMemOrderLinesorderitem_quantity: TStringField;
    FDMemOrderLinesorderitem_taxprofile_id: TIntegerField;
    FDMemOrderLinesorderitem_per_item_tax: TBCDField;
    FDMemOrderLinesorderitem_tax: TBCDField;
    FDMemOrderLinesorderitem_discount: TBCDField;
    FDMemOrderLinesorderitem_discount_tax: TBCDField;
    FDMemOrderLinesorderitem_price: TFMTBCDField;
    FDMemOrderLinesorderitem_option_price: TBCDField;
    FDMemOrderLinesorderitem_finalprice: TBCDField;
    FDMemOrderLinesorderitem_finalprice_with_tax: TBCDField;
    FDMemOrderLinesorderitem_finalprice_without_tax: TBCDField;
    FDMemOrderLinesorderitem_params: TMemoField;
    FDMemOrderLinescreated_on: TDateTimeField;
    FDMemOrderLinescreated_by: TIntegerField;
    FDMemOrderLinesorderitem_weight: TStringField;
    FDMemOrderLinesorderitem_weight_total: TStringField;
    FDQSalesOrderLinesDATECREATED: TSQLTimeStampField;
    FDQSalesOrderLinesCREATEDBY: TStringField;
    FDTransOrders: TFDTransaction;
    FDQGetLocalItemInfo: TFDQuery;
    FDQGetLocalItemInfoVARUNRID: TIntegerField;
    FDQGetLocalItemInfoUNITID: TStringField;
    FDQGetLocalItemInfoKOSTPRISUR1: TBCDField;
    FDQGetLocalItemInfoSATSUR: TCurrencyField;
    FDQSalesOrderLinesEXTORDERLINEID: TIntegerField;
    FDQGetLastCopiedOrderLineID: TFDQuery;
    FDQGetLastCopiedOrderLineIDLASTLINEID: TIntegerField;
    FDQSalesOrderRemarks: TFDQuery;
    FDQSalesOrderRemarksLINEID: TIntegerField;
    FDQSalesOrderRemarksSALESORDERID: TIntegerField;
    FDQSalesOrderRemarksREMARK: TStringField;
    FDQSalesOrderRemarksDATAAREAID: TSmallintField;
    FDQWebOrdersORDERID: TStringField;
    FDQWebOrdersUSERID: TIntegerField;
    FDQWebOrdersUSEREMAIL: TStringField;
    FDQWebOrdersPAYMENTTYPE: TStringField;
    FDQWebOrdersSALESORDERID: TIntegerField;
    FDQWebOrdersSUBTOTAL: TCurrencyField;
    FDQWebOrdersVAT: TCurrencyField;
    FDQWebOrdersSHIPPING: TCurrencyField;
    FDQWebOrdersSHIPPINGVAT: TCurrencyField;
    FDQDeliveryAddress: TFDQuery;
    FDComDelivAddres: TFDCommand;
    FDTAdaptDelivAddres: TFDTableAdapter;
    FDMemDelivAddres: TFDMemTable;
    FDMemDelivAddresj2store_address_id: TFDAutoIncField;
    FDMemDelivAddresuser_id: TIntegerField;
    FDMemDelivAddresfirst_name: TStringField;
    FDMemDelivAddreslast_name: TStringField;
    FDMemDelivAddresemail: TStringField;
    FDMemDelivAddresaddress_1: TStringField;
    FDMemDelivAddresaddress_2: TStringField;
    FDMemDelivAddrescity: TStringField;
    FDMemDelivAddreszip: TStringField;
    FDMemDelivAddreszone_id: TStringField;
    FDMemDelivAddrescountry_id: TStringField;
    FDMemDelivAddresphone_1: TStringField;
    FDMemDelivAddresphone_2: TStringField;
    FDMemDelivAddresfax: TStringField;
    FDMemDelivAddrestype: TStringField;
    FDMemDelivAddrescompany: TStringField;
    FDMemDelivAddrestax_number: TStringField;
    FDMemDelivAddreslast_edited: TSQLTimeStampField;
    FDQDeliveryAddressID: TIntegerField;
    FDQDeliveryAddressREFTABLE: TIntegerField;
    FDQDeliveryAddressDATAAREAID: TSmallintField;
    FDQDeliveryAddressFIRSTNAME: TStringField;
    FDQDeliveryAddressLASTNAME: TStringField;
    FDQDeliveryAddressTELNO: TStringField;
    FDQDeliveryAddressEMAIL: TStringField;
    FDQDeliveryAddressREFID: TStringField;
    FDQDeliveryAddressADDRESS1: TStringField;
    FDQDeliveryAddressADDRESS2: TStringField;
    FDQDeliveryAddressCITY: TStringField;
    FDQDeliveryAddressPOSTCODE: TStringField;
    FDQDeliveryAddressTELNO2: TStringField;
    FDQDeliveryAddressEXTADDRESSID: TIntegerField;
    FDQDeliveryAddressCOMPANY: TStringField;
    FDMemUsersid: TFDAutoIncField;
    FDMemUsersname: TStringField;
    FDMemUsersusername: TStringField;
    FDMemUsersemail: TStringField;
    FDMemUserspassword: TStringField;
    FDMemUsersblock: TShortintField;
    FDMemUserssendEmail: TShortintField;
    FDMemUsersregisterDate: TDateTimeField;
    FDMemUserslastvisitDate: TDateTimeField;
    FDMemUsersactivation: TStringField;
    FDMemUsersparams: TMemoField;
    FDMemUserslastResetTime: TDateTimeField;
    FDMemUsersresetCount: TIntegerField;
    FDMemUsersotpKey: TStringField;
    FDMemUsersotep: TStringField;
    FDMemUsersrequireReset: TShortintField;
    FDMemUserslast_edited: TSQLTimeStampField;
    FDComInvoiceAddress: TFDCommand;
    FDTAdaptInvoiceAddress: TFDTableAdapter;
    FDMemInvoiceAddress: TFDMemTable;
    FDMemInvoiceAddressfirst_name: TStringField;
    FDMemInvoiceAddresslast_name: TStringField;
    FDMemInvoiceAddressemail: TStringField;
    FDMemInvoiceAddresscity: TStringField;
    FDMemInvoiceAddresszip: TStringField;
    FDMemInvoiceAddressphone_1: TStringField;
    FDMemInvoiceAddressphone_2: TStringField;
    FDQSalesOrdersDELIVERICONTACTID: TIntegerField;
    FDQWebOrdersDATAAREAID: TSmallintField;
    procedure FDMemUsersAfterOpen(DataSet: TDataSet);
    procedure FDMemDelivAddresAfterOpen(DataSet: TDataSet);
    procedure FDMemInvoiceAddressAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    function GetLastWebSalesOrderID: string;
    function GetLastWebSalesOrderLineID: integer;
    function GetItemInfoFromSku(Sku: string): TLocalItem;
    function GetCustIDFromExtCustID(ExtCustID: integer): TCustomer;
    function GetContactIdFromExtCustID(ExtCustID: integer): integer;
  end;



implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmJ2Orders.FDMemDelivAddresAfterOpen(DataSet: TDataSet);
begin
    dmMain.ApplyMemTable(DataSet);
end;

procedure TdmJ2Orders.FDMemInvoiceAddressAfterOpen(DataSet: TDataSet);
begin
    dmMain.ApplyMemTable(DataSet);
end;

procedure TdmJ2Orders.FDMemUsersAfterOpen(DataSet: TDataSet);
begin
    dmMain.ApplyMemTable(DataSet);
end;

function TdmJ2Orders.GetContactIdFromExtCustID(ExtCustID: integer): integer;
begin
    if FDQDeliveryAddress.Active then
       FDQDeliveryAddress.Close;

    FDQDeliveryAddress.ParamByName('EXTADDRESSID').Value:=ExtCustID;
    FDQDeliveryAddress.Open;

    if FDQDeliveryAddress.RecordCount > 0 then
      result:=FDQDeliveryAddressREFID.AsInteger
    else
      result:=-1;
end;

function TdmJ2Orders.GetCustIDFromExtCustID(ExtCustID: integer): TCustomer;
begin
    if FDQCustomers.Active then
        FDQCustomers.Close;

    FDQCustomers.ParamByName('EXTCUSTNO').Value:=ExtCustID;
    FDQCustomers.Open;

    if (FDQCustomers.RecordCount > 0) then
    begin
       result.ID:=FDQCustomersNR.Value;
       result.Name:=FDQCustomersNAVN.Value;
    end
    else
    begin
       result.ID:=-1;
       result.Name:='';
    end;
end;

function TdmJ2Orders.GetItemInfoFromSku(Sku: string): TLocalItem;
begin
    FDQGetLocalItemInfo.Close;
    FDQGetLocalItemInfo.ParamByName('SKU').Value:=Sku;
    FDQGetLocalItemInfo.Open;

    if (FDQGetLocalItemInfo.RecordCount > 0) then
    begin
      result.ItemID:=FDQGetLocalItemInfoVARUNRID.Value;

      try
        result.VatTariff:=FDQGetLocalItemInfoSATSUR.Value/100;
      except
        result.VatTariff:=0;
      end;

      result.CostPrice:=FDQGetLocalItemInfoKOSTPRISUR1.Value;
      result.ItemUnit:=FDQGetLocalItemInfoUNITID.Value;
    end
    else
    begin
      result.ItemID:=-1;
      result.VatTariff:=0;
      result.CostPrice:=0;
      result.ItemUnit:='';
    end;

    FDQGetLocalItemInfo.Close;
end;

function TdmJ2Orders.GetLastWebSalesOrderID: string;
begin
    FDQGetMax.Close;
    FDQGetMax.Open;

    if FDQGetMax.FieldByName('orderid').IsNull then
        result:=''
    else
        result:=FDQGetMax.FieldByName('orderid').value;

    FDQGetMax.Close;
end;



function TdmJ2Orders.GetLastWebSalesOrderLineID: integer;
begin
    FDQGetLastCopiedOrderLineID.Open;

    if FDQGetLastCopiedOrderLineIDLASTLINEID.IsNull then
        result:=-1
    else
        result:=FDQGetLastCopiedOrderLineIDLASTLINEID.value;

    FDQGetLastCopiedOrderLineID.Close;
end;

end.

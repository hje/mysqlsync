object dmJ2Orders: TdmJ2Orders
  OldCreateOrder = False
  Height = 576
  Width = 847
  object FDQJ2OrderLines: TFDQuery
    Connection = dmMain.FDConnection1
    SQL.Strings = (
      'select * from j3f6_j2store_orderitems'
      'where order_id = :ORDERID')
    Left = 72
    Top = 64
    ParamData = <
      item
        Name = 'ORDERID'
        DataType = ftWideString
        ParamType = ptInput
        Value = '14841728758'
      end>
  end
  object FDQJ2OrderInfos: TFDQuery
    Connection = dmMain.FDConnection1
    SQL.Strings = (
      'select * from j3f6_j2store_orderinfos'
      'where order_id = :ORDERID')
    Left = 69
    Top = 120
    ParamData = <
      item
        Name = 'ORDERID'
        DataType = ftWideString
        ParamType = ptInput
        Value = '151697800022'
      end>
  end
  object FDQJ2OrderHistories: TFDQuery
    Connection = dmMain.FDConnection1
    SQL.Strings = (
      'select H.*, S.orderstatus_name, S.orderstatus_cssclass '
      'FROM j3f6_j2store_orderhistories H'
      'left join j3f6_j2store_orderstatuses S'
      'on H.order_state_id = S.j2store_orderstatus_id '
      'where order_id = :ORDERID')
    Left = 72
    Top = 176
    ParamData = <
      item
        Name = 'ORDERID'
        DataType = ftWideString
        ParamType = ptInput
        Value = '151680296320'
      end>
  end
  object FDQSalesOrders: TFDQuery
    Connection = dmLocal.FDConLocalDb
    Transaction = FDTransOrders
    SQL.Strings = (
      'select * from salesorder'
      'where id = :ORDERID')
    Left = 704
    Top = 56
    ParamData = <
      item
        Name = 'ORDERID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQSalesOrdersID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQSalesOrdersCUSTOMERID: TIntegerField
      FieldName = 'CUSTOMERID'
      Origin = 'CUSTOMERID'
      Required = True
    end
    object FDQSalesOrdersCREATEDBY: TStringField
      FieldName = 'CREATEDBY'
      Origin = 'CREATEDBY'
      Size = 10
    end
    object FDQSalesOrdersDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQSalesOrdersDATECREATED: TSQLTimeStampField
      FieldName = 'DATECREATED'
      Origin = 'DATECREATED'
    end
    object FDQSalesOrdersSTATUS: TIntegerField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
    object FDQSalesOrdersCUSTREF: TStringField
      FieldName = 'CUSTREF'
      Origin = 'CUSTREF'
      Size = 100
    end
    object FDQSalesOrdersORDEREDBY: TStringField
      FieldName = 'ORDEREDBY'
      Origin = 'ORDEREDBY'
      Size = 50
    end
    object FDQSalesOrdersDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = 'DESCRIPTION'
      Size = 100
    end
    object FDQSalesOrdersDESIRDELIVDATE: TSQLTimeStampField
      FieldName = 'DESIRDELIVDATE'
      Origin = 'DESIRDELIVDATE'
    end
    object FDQSalesOrdersSUBSCRIPTIONID: TIntegerField
      FieldName = 'SUBSCRIPTIONID'
      Origin = 'SUBSCRIPTIONID'
    end
    object FDQSalesOrdersDELIVERYNAME: TStringField
      FieldName = 'DELIVERYNAME'
      Origin = 'DELIVERYNAME'
      Size = 50
    end
    object FDQSalesOrdersDELIVERYADDRESS: TStringField
      FieldName = 'DELIVERYADDRESS'
      Origin = 'DELIVERYADDRESS'
      Size = 50
    end
    object FDQSalesOrdersDELIVERYCITY: TStringField
      FieldName = 'DELIVERYCITY'
      Origin = 'DELIVERYCITY'
      Size = 50
    end
    object FDQSalesOrdersPOSTCODE: TStringField
      FieldName = 'POSTCODE'
      Origin = 'POSTCODE'
    end
    object FDQSalesOrdersDELIVERICONTACTID: TIntegerField
      FieldName = 'DELIVERICONTACTID'
      Origin = 'DELIVERICONTACTID'
    end
  end
  object FDQGetMax: TFDQuery
    Connection = dmLocal.FDConLocalDb
    Transaction = FDTransOrders
    SQL.Strings = (
      'select max(orderid) as orderid from websalesorders')
    Left = 576
    Top = 24
  end
  object FDQWebOrders: TFDQuery
    Connection = dmLocal.FDConLocalDb
    Transaction = FDTransOrders
    SQL.Strings = (
      'select * from websalesorders'
      'where orderid = :websalesorderid')
    Left = 704
    Top = 104
    ParamData = <
      item
        Name = 'WEBSALESORDERID'
        DataType = ftString
        ParamType = ptInput
        Size = 32
        Value = Null
      end>
    object FDQWebOrdersORDERID: TStringField
      FieldName = 'ORDERID'
      Origin = 'ORDERID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 32
    end
    object FDQWebOrdersUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'USERID'
      Required = True
    end
    object FDQWebOrdersUSEREMAIL: TStringField
      FieldName = 'USEREMAIL'
      Origin = 'USEREMAIL'
      Required = True
      Size = 255
    end
    object FDQWebOrdersPAYMENTTYPE: TStringField
      FieldName = 'PAYMENTTYPE'
      Origin = 'PAYMENTTYPE'
      Required = True
      Size = 255
    end
    object FDQWebOrdersSALESORDERID: TIntegerField
      FieldName = 'SALESORDERID'
      Origin = 'SALESORDERID'
      Required = True
    end
    object FDQWebOrdersSUBTOTAL: TCurrencyField
      FieldName = 'SUBTOTAL'
      Origin = 'SUBTOTAL'
      Required = True
    end
    object FDQWebOrdersVAT: TCurrencyField
      FieldName = 'VAT'
      Origin = 'VAT'
      Required = True
    end
    object FDQWebOrdersSHIPPING: TCurrencyField
      FieldName = 'SHIPPING'
      Origin = 'SHIPPING'
      Required = True
    end
    object FDQWebOrdersSHIPPINGVAT: TCurrencyField
      FieldName = 'SHIPPINGVAT'
      Origin = 'SHIPPINGVAT'
      Required = True
    end
    object FDQWebOrdersDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      Required = True
    end
  end
  object FDQSalesOrderLines: TFDQuery
    Connection = dmLocal.FDConLocalDb
    Transaction = FDTransOrders
    SQL.Strings = (
      'select * from salesorderline'
      'where salesorderid = :salesorderid')
    Left = 708
    Top = 152
    ParamData = <
      item
        Name = 'SALESORDERID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQSalesOrderLinesLINEID: TIntegerField
      FieldName = 'LINEID'
      Origin = 'LINEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQSalesOrderLinesSALESORDERID: TIntegerField
      FieldName = 'SALESORDERID'
      Origin = 'SALESORDERID'
      Required = True
    end
    object FDQSalesOrderLinesQUANTITY: TBCDField
      FieldName = 'QUANTITY'
      Origin = 'QUANTITY'
      Precision = 18
      Size = 3
    end
    object FDQSalesOrderLinesSTATUS: TIntegerField
      FieldName = 'STATUS'
      Origin = 'STATUS'
    end
    object FDQSalesOrderLinesSALESPRICE: TBCDField
      FieldName = 'SALESPRICE'
      Origin = 'SALESPRICE'
      Precision = 18
      Size = 3
    end
    object FDQSalesOrderLinesITEMID: TIntegerField
      FieldName = 'ITEMID'
      Origin = 'ITEMID'
      Required = True
    end
    object FDQSalesOrderLinesITEMNAME: TStringField
      FieldName = 'ITEMNAME'
      Origin = 'ITEMNAME'
      Size = 1000
    end
    object FDQSalesOrderLinesCOSTPRICE: TCurrencyField
      FieldName = 'COSTPRICE'
      Origin = 'COSTPRICE'
    end
    object FDQSalesOrderLinesLINEDISC: TCurrencyField
      FieldName = 'LINEDISC'
      Origin = 'LINEDISC'
    end
    object FDQSalesOrderLinesUNIT: TStringField
      FieldName = 'UNIT'
      Origin = 'UNIT'
      Size = 10
    end
    object FDQSalesOrderLinesVATTARIFF: TCurrencyField
      FieldName = 'VATTARIFF'
      Origin = 'VATTARIFF'
    end
    object FDQSalesOrderLinesLINEDISCPERCENT: TSingleField
      FieldName = 'LINEDISCPERCENT'
      Origin = 'LINEDISCPERCENT'
    end
    object FDQSalesOrderLinesBARCODE: TStringField
      FieldName = 'BARCODE'
      Origin = 'BARCODE'
    end
    object FDQSalesOrderLinesINTERNALITEMID: TStringField
      FieldName = 'INTERNALITEMID'
      Origin = 'INTERNALITEMID'
    end
    object FDQSalesOrderLinesLINEAMOUNT: TFMTBCDField
      FieldName = 'LINEAMOUNT'
      Origin = 'LINEAMOUNT'
      Precision = 18
      Size = 6
    end
    object FDQSalesOrderLinesLINEAMOUNTVDISC: TFMTBCDField
      FieldName = 'LINEAMOUNTVDISC'
      Origin = 'LINEAMOUNTVDISC'
      Precision = 18
      Size = 6
    end
    object FDQSalesOrderLinesVATAMOUNT: TFMTBCDField
      FieldName = 'VATAMOUNT'
      Origin = 'VATAMOUNT'
      Precision = 18
    end
    object FDQSalesOrderLinesLINEAMOUNTINCVAT: TFMTBCDField
      FieldName = 'LINEAMOUNTINCVAT'
      Origin = 'LINEAMOUNTINCVAT'
      Precision = 18
    end
    object FDQSalesOrderLinesDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      Required = True
    end
    object FDQSalesOrderLinesDATECREATED: TSQLTimeStampField
      FieldName = 'DATECREATED'
      Origin = 'DATECREATED'
      Required = True
    end
    object FDQSalesOrderLinesCREATEDBY: TStringField
      FieldName = 'CREATEDBY'
      Origin = 'CREATEDBY'
      Required = True
      Size = 10
    end
    object FDQSalesOrderLinesEXTORDERLINEID: TIntegerField
      FieldName = 'EXTORDERLINEID'
      Origin = 'EXTORDERLINEID'
    end
  end
  object FDComUsers: TFDCommand
    Connection = dmMain.FDConnection1
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    CommandText.Strings = (
      'select * from j3f6_users'
      'where last_edited > :LASTUPDATE')
    ParamData = <
      item
        Name = 'LASTUPDATE'
        DataType = ftDateTime
        ParamType = ptInput
        Value = 0d
      end>
    Left = 72
    Top = 232
  end
  object FDTAdaptUsers: TFDTableAdapter
    DatSTableName = 'FDMemUsers'
    SelectCommand = FDComUsers
    Left = 160
    Top = 232
  end
  object FDMemUsers: TFDMemTable
    AfterOpen = FDMemUsersAfterOpen
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Adapter = FDTAdaptUsers
    Left = 256
    Top = 232
    object FDMemUsersid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object FDMemUsersname: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'name'
      Origin = '`name`'
      Size = 400
    end
    object FDMemUsersusername: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'username'
      Origin = 'username'
      Size = 150
    end
    object FDMemUsersemail: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'email'
      Origin = 'email'
      Size = 100
    end
    object FDMemUserspassword: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'password'
      Origin = '`password`'
      Size = 100
    end
    object FDMemUsersblock: TShortintField
      AutoGenerateValue = arDefault
      FieldName = 'block'
      Origin = 'block'
    end
    object FDMemUserssendEmail: TShortintField
      AutoGenerateValue = arDefault
      FieldName = 'sendEmail'
      Origin = 'sendEmail'
    end
    object FDMemUsersregisterDate: TDateTimeField
      AutoGenerateValue = arDefault
      FieldName = 'registerDate'
      Origin = 'registerDate'
    end
    object FDMemUserslastvisitDate: TDateTimeField
      AutoGenerateValue = arDefault
      FieldName = 'lastvisitDate'
      Origin = 'lastvisitDate'
    end
    object FDMemUsersactivation: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'activation'
      Origin = 'activation'
      Size = 100
    end
    object FDMemUsersparams: TMemoField
      FieldName = 'params'
      Origin = 'params'
      Required = True
      BlobType = ftMemo
    end
    object FDMemUserslastResetTime: TDateTimeField
      AutoGenerateValue = arDefault
      FieldName = 'lastResetTime'
      Origin = 'lastResetTime'
    end
    object FDMemUsersresetCount: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'resetCount'
      Origin = 'resetCount'
    end
    object FDMemUsersotpKey: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'otpKey'
      Origin = 'otpKey'
      Size = 1000
    end
    object FDMemUsersotep: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'otep'
      Origin = 'otep'
      Size = 1000
    end
    object FDMemUsersrequireReset: TShortintField
      AutoGenerateValue = arDefault
      FieldName = 'requireReset'
      Origin = 'requireReset'
    end
    object FDMemUserslast_edited: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'last_edited'
      Origin = 'last_edited'
    end
  end
  object FDQCustomers: TFDQuery
    Connection = dmLocal.FDConLocalDb
    Transaction = FDTransOrders
    SQL.Strings = (
      'select * from kundar'
      'where extkundanr = :EXTCUSTNO')
    Left = 704
    Top = 304
    ParamData = <
      item
        Name = 'EXTCUSTNO'
        DataType = ftString
        ParamType = ptInput
        Size = 20
        Value = Null
      end>
    object FDQCustomersNAVN: TStringField
      FieldName = 'NAVN'
      Origin = 'NAVN'
      Size = 50
    end
    object FDQCustomersBUDSTADUR: TStringField
      FieldName = 'BUDSTADUR'
      Origin = 'BUDSTADUR'
      Size = 50
    end
    object FDQCustomersPOSTBOKS: TStringField
      FieldName = 'POSTBOKS'
      Origin = 'POSTBOKS'
      Size = 10
    end
    object FDQCustomersPOSTNR: TStringField
      FieldName = 'POSTNR'
      Origin = 'POSTNR'
    end
    object FDQCustomersBYGDBYUR: TStringField
      FieldName = 'BYGDBYUR'
      Origin = 'BYGDBYUR'
      Size = 50
    end
    object FDQCustomersLAND: TStringField
      FieldName = 'LAND'
      Origin = 'LAND'
      Size = 50
    end
    object FDQCustomersTLF: TStringField
      FieldName = 'TLF'
      Origin = 'TLF'
    end
    object FDQCustomersFARTLF: TStringField
      FieldName = 'FARTLF'
      Origin = 'FARTLF'
    end
    object FDQCustomersFAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
    end
    object FDQCustomersEPOSTUR: TStringField
      FieldName = 'EPOSTUR'
      Origin = 'EPOSTUR'
      Size = 50
    end
    object FDQCustomersGJALDSTREYTIR: TSmallintField
      FieldName = 'GJALDSTREYTIR'
      Origin = 'GJALDSTREYTIR'
    end
    object FDQCustomersEXTKUNDANR: TStringField
      FieldName = 'EXTKUNDANR'
      Origin = 'EXTKUNDANR'
    end
    object FDQCustomersKONTONR: TStringField
      FieldName = 'KONTONR'
      Origin = 'KONTONR'
    end
    object FDQCustomersVIDMERKING: TStringField
      FieldName = 'VIDMERKING'
      Origin = 'VIDMERKING'
      Size = 100
    end
    object FDQCustomersNR: TIntegerField
      FieldName = 'NR'
      Origin = 'NR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQCustomersACCOPTION: TStringField
      FieldName = 'ACCOPTION'
      Origin = 'ACCOPTION'
    end
    object FDQCustomersMAXKREDITIKASSA: TBCDField
      FieldName = 'MAXKREDITIKASSA'
      Origin = 'MAXKREDITIKASSA'
      Precision = 18
      Size = 2
    end
    object FDQCustomersSALDO: TBCDField
      FieldName = 'SALDO'
      Origin = 'SALDO'
      Precision = 18
      Size = 2
    end
    object FDQCustomersACCADDR01: TStringField
      FieldName = 'ACCADDR01'
      Origin = 'ACCADDR01'
      Size = 200
    end
    object FDQCustomersACCADDR02: TStringField
      FieldName = 'ACCADDR02'
      Origin = 'ACCADDR02'
      Size = 200
    end
    object FDQCustomersACCADDR03: TStringField
      FieldName = 'ACCADDR03'
      Origin = 'ACCADDR03'
      Size = 200
    end
    object FDQCustomersACCADDR04: TStringField
      FieldName = 'ACCADDR04'
      Origin = 'ACCADDR04'
      Size = 200
    end
    object FDQCustomersSTONGD: TSmallintField
      FieldName = 'STONGD'
      Origin = 'STONGD'
    end
    object FDQCustomersKUNDABOLKUR: TIntegerField
      FieldName = 'KUNDABOLKUR'
      Origin = 'KUNDABOLKUR'
    end
    object FDQCustomersDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQCustomersPRISBOLKUR: TIntegerField
      FieldName = 'PRISBOLKUR'
      Origin = 'PRISBOLKUR'
    end
  end
  object FDQSyncDBClients: TFDQuery
    Connection = dmLocal.FDConLocalDb
    SQL.Strings = (
      'select * from syncextdbclients'
      'where clientid = :clientid')
    Left = 256
    Top = 16
    ParamData = <
      item
        Name = 'CLIENTID'
        DataType = ftWideString
        ParamType = ptInput
        Value = '1'
      end>
  end
  object FDQGetLastSync: TFDQuery
    Connection = dmLocal.FDConLocalDb
    Transaction = FDTransOrders
    SQL.Strings = (
      'select tableid, lastupdate, direction '
      'from syncextdbclients'
      'where clientid = :clientid')
    Left = 608
    Top = 304
    ParamData = <
      item
        Name = 'CLIENTID'
        DataType = ftWideString
        ParamType = ptInput
        Value = '1'
      end>
    object FDQGetLastSyncTABLEID: TIntegerField
      FieldName = 'TABLEID'
      Origin = 'TABLEID'
      Required = True
    end
    object FDQGetLastSyncLASTUPDATE: TSQLTimeStampField
      FieldName = 'LASTUPDATE'
      Origin = 'LASTUPDATE'
    end
    object FDQGetLastSyncDIRECTION: TStringField
      FieldName = 'DIRECTION'
      Origin = 'DIRECTION'
      Required = True
      FixedChar = True
      Size = 6
    end
  end
  object FDComOrders: TFDCommand
    Connection = dmMain.FDConnection1
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    CommandText.Strings = (
      'select * from j3f6_j2store_orders'
      'where order_id > :LASTORDERID ')
    ParamData = <
      item
        Name = 'LASTORDERID'
        DataType = ftWideString
        ParamType = ptInput
        Value = '0'
      end>
    Left = 72
    Top = 288
  end
  object FDTAdaptOrders: TFDTableAdapter
    DatSTableName = 'FDMemOrders'
    SelectCommand = FDComOrders
    Left = 160
    Top = 288
  end
  object FDMemOrders: TFDMemTable
    AfterOpen = FDMemUsersAfterOpen
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Adapter = FDTAdaptOrders
    Left = 256
    Top = 288
    object FDMemOrdersj2store_order_id: TFDAutoIncField
      FieldName = 'j2store_order_id'
      Origin = 'j2store_order_id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object FDMemOrdersorder_id: TStringField
      FieldName = 'order_id'
      Origin = 'order_id'
      Required = True
      Size = 255
    end
    object FDMemOrdersorder_type: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'order_type'
      Origin = 'order_type'
      Size = 255
    end
    object FDMemOrdersparent_id: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'parent_id'
      Origin = 'parent_id'
    end
    object FDMemOrderssubscription_id: TIntegerField
      FieldName = 'subscription_id'
      Origin = 'subscription_id'
      Required = True
    end
    object FDMemOrderscart_id: TLongWordField
      FieldName = 'cart_id'
      Origin = 'cart_id'
      Required = True
    end
    object FDMemOrdersinvoice_prefix: TStringField
      FieldName = 'invoice_prefix'
      Origin = 'invoice_prefix'
      Required = True
      Size = 255
    end
    object FDMemOrdersinvoice_number: TIntegerField
      FieldName = 'invoice_number'
      Origin = 'invoice_number'
      Required = True
    end
    object FDMemOrderstoken: TStringField
      FieldName = 'token'
      Origin = 'token'
      Required = True
      Size = 255
    end
    object FDMemOrdersuser_id: TIntegerField
      FieldName = 'user_id'
      Origin = 'user_id'
      Required = True
    end
    object FDMemOrdersuser_email: TStringField
      FieldName = 'user_email'
      Origin = 'user_email'
      Required = True
      Size = 255
    end
    object FDMemOrdersorder_total: TBCDField
      FieldName = 'order_total'
      Origin = 'order_total'
      Required = True
      Precision = 12
    end
    object FDMemOrdersorder_subtotal: TBCDField
      FieldName = 'order_subtotal'
      Origin = 'order_subtotal'
      Required = True
      Precision = 12
    end
    object FDMemOrdersorder_subtotal_ex_tax: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'order_subtotal_ex_tax'
      Origin = 'order_subtotal_ex_tax'
      Precision = 12
    end
    object FDMemOrdersorder_tax: TBCDField
      FieldName = 'order_tax'
      Origin = 'order_tax'
      Required = True
      Precision = 12
    end
    object FDMemOrdersorder_shipping: TBCDField
      FieldName = 'order_shipping'
      Origin = 'order_shipping'
      Required = True
      Precision = 12
    end
    object FDMemOrdersorder_shipping_tax: TBCDField
      FieldName = 'order_shipping_tax'
      Origin = 'order_shipping_tax'
      Required = True
      Precision = 12
    end
    object FDMemOrdersorder_discount: TBCDField
      FieldName = 'order_discount'
      Origin = 'order_discount'
      Required = True
      Precision = 12
    end
    object FDMemOrdersorder_discount_tax: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'order_discount_tax'
      Origin = 'order_discount_tax'
      Precision = 12
    end
    object FDMemOrdersorder_credit: TBCDField
      FieldName = 'order_credit'
      Origin = 'order_credit'
      Required = True
      Precision = 12
    end
    object FDMemOrdersorder_refund: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'order_refund'
      Origin = 'order_refund'
      Precision = 12
    end
    object FDMemOrdersorder_surcharge: TBCDField
      FieldName = 'order_surcharge'
      Origin = 'order_surcharge'
      Required = True
      Precision = 12
    end
    object FDMemOrdersorder_fees: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'order_fees'
      Origin = 'order_fees'
      Precision = 12
    end
    object FDMemOrdersorderpayment_type: TStringField
      FieldName = 'orderpayment_type'
      Origin = 'orderpayment_type'
      Required = True
      Size = 255
    end
    object FDMemOrderstransaction_id: TStringField
      FieldName = 'transaction_id'
      Origin = 'transaction_id'
      Required = True
      Size = 255
    end
    object FDMemOrderstransaction_status: TStringField
      FieldName = 'transaction_status'
      Origin = 'transaction_status'
      Required = True
      Size = 255
    end
    object FDMemOrderstransaction_details: TMemoField
      FieldName = 'transaction_details'
      Origin = 'transaction_details'
      Required = True
      BlobType = ftMemo
    end
    object FDMemOrderscurrency_id: TIntegerField
      FieldName = 'currency_id'
      Origin = 'currency_id'
      Required = True
    end
    object FDMemOrderscurrency_code: TStringField
      FieldName = 'currency_code'
      Origin = 'currency_code'
      Required = True
      Size = 255
    end
    object FDMemOrderscurrency_value: TFMTBCDField
      AutoGenerateValue = arDefault
      FieldName = 'currency_value'
      Origin = 'currency_value'
      Precision = 15
    end
    object FDMemOrdersip_address: TStringField
      FieldName = 'ip_address'
      Origin = 'ip_address'
      Required = True
      Size = 255
    end
    object FDMemOrdersis_shippable: TIntegerField
      FieldName = 'is_shippable'
      Origin = 'is_shippable'
      Required = True
    end
    object FDMemOrdersis_including_tax: TIntegerField
      FieldName = 'is_including_tax'
      Origin = 'is_including_tax'
      Required = True
    end
    object FDMemOrderscustomer_note: TMemoField
      FieldName = 'customer_note'
      Origin = 'customer_note'
      Required = True
      BlobType = ftMemo
    end
    object FDMemOrderscustomer_language: TStringField
      FieldName = 'customer_language'
      Origin = 'customer_language'
      Required = True
      Size = 255
    end
    object FDMemOrderscustomer_group: TStringField
      FieldName = 'customer_group'
      Origin = 'customer_group'
      Required = True
      Size = 255
    end
    object FDMemOrdersorder_state_id: TIntegerField
      FieldName = 'order_state_id'
      Origin = 'order_state_id'
      Required = True
    end
    object FDMemOrdersorder_state: TStringField
      FieldName = 'order_state'
      Origin = 'order_state'
      Required = True
      Size = 255
    end
    object FDMemOrdersorder_params: TMemoField
      AutoGenerateValue = arDefault
      FieldName = 'order_params'
      Origin = 'order_params'
      BlobType = ftMemo
    end
    object FDMemOrderscreated_on: TDateTimeField
      FieldName = 'created_on'
      Origin = 'created_on'
      Required = True
    end
    object FDMemOrderscreated_by: TIntegerField
      FieldName = 'created_by'
      Origin = 'created_by'
      Required = True
    end
    object FDMemOrdersmodified_on: TDateTimeField
      FieldName = 'modified_on'
      Origin = 'modified_on'
      Required = True
    end
    object FDMemOrdersmodified_by: TIntegerField
      FieldName = 'modified_by'
      Origin = 'modified_by'
      Required = True
    end
  end
  object FDComOrderInfo: TFDCommand
    Connection = dmMain.FDConnection1
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    CommandText.Strings = (
      'select * from j3f6_j2store_orderinfos'
      'where order_id = :ORDERID')
    ParamData = <
      item
        Name = 'ORDERID'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
    Left = 72
    Top = 336
  end
  object FDMemOrderInfo: TFDMemTable
    AfterOpen = FDMemUsersAfterOpen
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Adapter = FDTAdaptOrderInfo
    Left = 256
    Top = 338
    object FDMemOrderInfoj2store_orderinfo_id: TFDAutoIncField
      FieldName = 'j2store_orderinfo_id'
      Origin = 'j2store_orderinfo_id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object FDMemOrderInfoorder_id: TStringField
      FieldName = 'order_id'
      Origin = 'order_id'
      Required = True
      Size = 255
    end
    object FDMemOrderInfobilling_company: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_company'
      Origin = 'billing_company'
      Size = 255
    end
    object FDMemOrderInfobilling_last_name: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_last_name'
      Origin = 'billing_last_name'
      Size = 255
    end
    object FDMemOrderInfobilling_first_name: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_first_name'
      Origin = 'billing_first_name'
      Size = 255
    end
    object FDMemOrderInfobilling_middle_name: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_middle_name'
      Origin = 'billing_middle_name'
      Size = 255
    end
    object FDMemOrderInfobilling_phone_1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_phone_1'
      Origin = 'billing_phone_1'
      Size = 255
    end
    object FDMemOrderInfobilling_phone_2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_phone_2'
      Origin = 'billing_phone_2'
      Size = 255
    end
    object FDMemOrderInfobilling_fax: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_fax'
      Origin = 'billing_fax'
      Size = 255
    end
    object FDMemOrderInfobilling_address_1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_address_1'
      Origin = 'billing_address_1'
      Size = 255
    end
    object FDMemOrderInfobilling_address_2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_address_2'
      Origin = 'billing_address_2'
      Size = 255
    end
    object FDMemOrderInfobilling_city: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_city'
      Origin = 'billing_city'
      Size = 255
    end
    object FDMemOrderInfobilling_zone_name: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_zone_name'
      Origin = 'billing_zone_name'
      Size = 255
    end
    object FDMemOrderInfobilling_country_name: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_country_name'
      Origin = 'billing_country_name'
      Size = 255
    end
    object FDMemOrderInfobilling_zone_id: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'billing_zone_id'
      Origin = 'billing_zone_id'
    end
    object FDMemOrderInfobilling_country_id: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'billing_country_id'
      Origin = 'billing_country_id'
    end
    object FDMemOrderInfobilling_zip: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_zip'
      Origin = 'billing_zip'
      Size = 255
    end
    object FDMemOrderInfobilling_tax_number: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'billing_tax_number'
      Origin = 'billing_tax_number'
      Size = 255
    end
    object FDMemOrderInfoshipping_company: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_company'
      Origin = 'shipping_company'
      Size = 255
    end
    object FDMemOrderInfoshipping_last_name: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_last_name'
      Origin = 'shipping_last_name'
      Size = 255
    end
    object FDMemOrderInfoshipping_first_name: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_first_name'
      Origin = 'shipping_first_name'
      Size = 255
    end
    object FDMemOrderInfoshipping_middle_name: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_middle_name'
      Origin = 'shipping_middle_name'
      Size = 255
    end
    object FDMemOrderInfoshipping_phone_1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_phone_1'
      Origin = 'shipping_phone_1'
      Size = 255
    end
    object FDMemOrderInfoshipping_phone_2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_phone_2'
      Origin = 'shipping_phone_2'
      Size = 255
    end
    object FDMemOrderInfoshipping_fax: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_fax'
      Origin = 'shipping_fax'
      Size = 255
    end
    object FDMemOrderInfoshipping_address_1: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_address_1'
      Origin = 'shipping_address_1'
      Size = 255
    end
    object FDMemOrderInfoshipping_address_2: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_address_2'
      Origin = 'shipping_address_2'
      Size = 255
    end
    object FDMemOrderInfoshipping_city: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_city'
      Origin = 'shipping_city'
      Size = 255
    end
    object FDMemOrderInfoshipping_zip: TStringField
      FieldName = 'shipping_zip'
      Origin = 'shipping_zip'
      Required = True
      Size = 255
    end
    object FDMemOrderInfoshipping_zone_name: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_zone_name'
      Origin = 'shipping_zone_name'
      Size = 255
    end
    object FDMemOrderInfoshipping_country_name: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_country_name'
      Origin = 'shipping_country_name'
      Size = 255
    end
    object FDMemOrderInfoshipping_zone_id: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_zone_id'
      Origin = 'shipping_zone_id'
    end
    object FDMemOrderInfoshipping_country_id: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_country_id'
      Origin = 'shipping_country_id'
    end
    object FDMemOrderInfoshipping_id: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_id'
      Origin = 'shipping_id'
      Size = 255
    end
    object FDMemOrderInfoshipping_tax_number: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'shipping_tax_number'
      Origin = 'shipping_tax_number'
      Size = 255
    end
    object FDMemOrderInfoall_billing: TMemoField
      FieldName = 'all_billing'
      Origin = 'all_billing'
      Required = True
      BlobType = ftMemo
    end
    object FDMemOrderInfoall_shipping: TMemoField
      FieldName = 'all_shipping'
      Origin = 'all_shipping'
      Required = True
      BlobType = ftMemo
    end
    object FDMemOrderInfoall_payment: TMemoField
      FieldName = 'all_payment'
      Origin = 'all_payment'
      Required = True
      BlobType = ftMemo
    end
  end
  object FDTAdaptOrderInfo: TFDTableAdapter
    UpdateTableName = 'j3f6_j2store_orderinfos'
    DatSTableName = 'FDMemOrderInfo'
    SelectCommand = FDComOrderInfo
    Left = 166
    Top = 336
  end
  object FDComOrderLines: TFDCommand
    Connection = dmMain.FDConnection1
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    CommandText.Strings = (
      'select * from j3f6_j2store_orderitems'
      'where j2store_orderitem_id > :LASTORDERLINEID')
    ParamData = <
      item
        Name = 'LASTORDERLINEID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    Left = 72
    Top = 384
  end
  object FDTAdaptOrderLines: TFDTableAdapter
    UpdateTableName = 'j3f6_j2store_orderitems'
    DatSTableName = 'FDMemOrderLines'
    SelectCommand = FDComOrderLines
    Left = 168
    Top = 384
  end
  object FDMemOrderLines: TFDMemTable
    AfterOpen = FDMemUsersAfterOpen
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Adapter = FDTAdaptOrderLines
    Left = 256
    Top = 384
    object FDMemOrderLinesj2store_orderitem_id: TFDAutoIncField
      FieldName = 'j2store_orderitem_id'
      Origin = 'j2store_orderitem_id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object FDMemOrderLinesorder_id: TStringField
      FieldName = 'order_id'
      Origin = 'order_id'
      Required = True
      Size = 255
    end
    object FDMemOrderLinesorderitem_type: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'orderitem_type'
      Origin = 'orderitem_type'
      Size = 255
    end
    object FDMemOrderLinescart_id: TIntegerField
      FieldName = 'cart_id'
      Origin = 'cart_id'
      Required = True
    end
    object FDMemOrderLinescartitem_id: TLongWordField
      FieldName = 'cartitem_id'
      Origin = 'cartitem_id'
      Required = True
    end
    object FDMemOrderLinesproduct_id: TIntegerField
      FieldName = 'product_id'
      Origin = 'product_id'
      Required = True
    end
    object FDMemOrderLinesproduct_type: TStringField
      FieldName = 'product_type'
      Origin = 'product_type'
      Required = True
      Size = 255
    end
    object FDMemOrderLinesvariant_id: TIntegerField
      FieldName = 'variant_id'
      Origin = 'variant_id'
      Required = True
    end
    object FDMemOrderLinesvendor_id: TIntegerField
      FieldName = 'vendor_id'
      Origin = 'vendor_id'
      Required = True
    end
    object FDMemOrderLinesorderitem_sku: TStringField
      FieldName = 'orderitem_sku'
      Origin = 'orderitem_sku'
      Required = True
      Size = 255
    end
    object FDMemOrderLinesorderitem_name: TStringField
      FieldName = 'orderitem_name'
      Origin = 'orderitem_name'
      Required = True
      Size = 255
    end
    object FDMemOrderLinesorderitem_attributes: TMemoField
      FieldName = 'orderitem_attributes'
      Origin = 'orderitem_attributes'
      Required = True
      BlobType = ftMemo
    end
    object FDMemOrderLinesorderitem_quantity: TStringField
      FieldName = 'orderitem_quantity'
      Origin = 'orderitem_quantity'
      Required = True
      Size = 255
    end
    object FDMemOrderLinesorderitem_taxprofile_id: TIntegerField
      FieldName = 'orderitem_taxprofile_id'
      Origin = 'orderitem_taxprofile_id'
      Required = True
    end
    object FDMemOrderLinesorderitem_per_item_tax: TBCDField
      FieldName = 'orderitem_per_item_tax'
      Origin = 'orderitem_per_item_tax'
      Required = True
      Precision = 12
    end
    object FDMemOrderLinesorderitem_tax: TBCDField
      FieldName = 'orderitem_tax'
      Origin = 'orderitem_tax'
      Required = True
      Precision = 12
    end
    object FDMemOrderLinesorderitem_discount: TBCDField
      FieldName = 'orderitem_discount'
      Origin = 'orderitem_discount'
      Required = True
      Precision = 12
    end
    object FDMemOrderLinesorderitem_discount_tax: TBCDField
      FieldName = 'orderitem_discount_tax'
      Origin = 'orderitem_discount_tax'
      Required = True
      Precision = 12
    end
    object FDMemOrderLinesorderitem_price: TFMTBCDField
      FieldName = 'orderitem_price'
      Origin = 'orderitem_price'
      Required = True
      Precision = 12
      Size = 5
    end
    object FDMemOrderLinesorderitem_option_price: TBCDField
      FieldName = 'orderitem_option_price'
      Origin = 'orderitem_option_price'
      Required = True
      Precision = 12
    end
    object FDMemOrderLinesorderitem_finalprice: TBCDField
      FieldName = 'orderitem_finalprice'
      Origin = 'orderitem_finalprice'
      Required = True
      Precision = 12
    end
    object FDMemOrderLinesorderitem_finalprice_with_tax: TBCDField
      FieldName = 'orderitem_finalprice_with_tax'
      Origin = 'orderitem_finalprice_with_tax'
      Required = True
      Precision = 12
    end
    object FDMemOrderLinesorderitem_finalprice_without_tax: TBCDField
      FieldName = 'orderitem_finalprice_without_tax'
      Origin = 'orderitem_finalprice_without_tax'
      Required = True
      Precision = 12
    end
    object FDMemOrderLinesorderitem_params: TMemoField
      FieldName = 'orderitem_params'
      Origin = 'orderitem_params'
      Required = True
      BlobType = ftMemo
    end
    object FDMemOrderLinescreated_on: TDateTimeField
      FieldName = 'created_on'
      Origin = 'created_on'
      Required = True
    end
    object FDMemOrderLinescreated_by: TIntegerField
      FieldName = 'created_by'
      Origin = 'created_by'
      Required = True
    end
    object FDMemOrderLinesorderitem_weight: TStringField
      FieldName = 'orderitem_weight'
      Origin = 'orderitem_weight'
      Required = True
      Size = 255
    end
    object FDMemOrderLinesorderitem_weight_total: TStringField
      FieldName = 'orderitem_weight_total'
      Origin = 'orderitem_weight_total'
      Required = True
      Size = 255
    end
  end
  object FDQGetLocalSalesOrderID: TFDQuery
    Connection = dmLocal.FDConLocalDb
    SQL.Strings = (
      'select s.ID from salesorder s'
      'left join websalesorders w'
      'on s.id = w.salesorderid'
      'where w.orderid = :EXTSALESORDERID')
    Left = 576
    Top = 112
    ParamData = <
      item
        Name = 'EXTSALESORDERID'
        DataType = ftString
        ParamType = ptInput
        Value = '151791095024'
      end>
    object FDQGetLocalSalesOrderIDID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object FDTransOrders: TFDTransaction
    Connection = dmLocal.FDConLocalDb
    Left = 704
    Top = 16
  end
  object FDQGetLocalItemInfo: TFDQuery
    Connection = dmLocal.FDConLocalDb
    SQL.Strings = (
      'select v.varunrid, v.unitid, kostprisur1, m.satsur'
      'from varunr v'
      'left join mvgkotur m'
      'on v.mvg = m.mvgid'
      'where v.egivarunr = :sku')
    Left = 580
    Top = 161
    ParamData = <
      item
        Name = 'SKU'
        DataType = ftString
        ParamType = ptInput
        Size = 20
        Value = Null
      end>
    object FDQGetLocalItemInfoVARUNRID: TIntegerField
      FieldName = 'VARUNRID'
      Origin = 'VARUNRID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQGetLocalItemInfoUNITID: TStringField
      FieldName = 'UNITID'
      Origin = 'UNITID'
      Required = True
      Size = 10
    end
    object FDQGetLocalItemInfoKOSTPRISUR1: TBCDField
      FieldName = 'KOSTPRISUR1'
      Origin = 'KOSTPRISUR1'
      Precision = 18
      Size = 3
    end
    object FDQGetLocalItemInfoSATSUR: TCurrencyField
      AutoGenerateValue = arDefault
      FieldName = 'SATSUR'
      Origin = 'SATSUR'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object FDQGetLastCopiedOrderLineID: TFDQuery
    Connection = dmLocal.FDConLocalDb
    SQL.Strings = (
      'select max(extorderlineid) as lastlineid'
      'from salesorderline')
    Left = 576
    Top = 72
    object FDQGetLastCopiedOrderLineIDLASTLINEID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'LASTLINEID'
      Origin = 'LASTLINEID'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object FDQSalesOrderRemarks: TFDQuery
    Connection = dmLocal.FDConLocalDb
    Transaction = FDTransOrders
    SQL.Strings = (
      'select * from salesorderremarks'
      'where salesorderid = :salesorderid and dataareaid = :dataareaid')
    Left = 704
    Top = 208
    ParamData = <
      item
        Name = 'SALESORDERID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
      end>
    object FDQSalesOrderRemarksLINEID: TIntegerField
      FieldName = 'LINEID'
      Origin = 'LINEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQSalesOrderRemarksSALESORDERID: TIntegerField
      FieldName = 'SALESORDERID'
      Origin = 'SALESORDERID'
      Required = True
    end
    object FDQSalesOrderRemarksREMARK: TStringField
      FieldName = 'REMARK'
      Origin = 'REMARK'
      Size = 100
    end
    object FDQSalesOrderRemarksDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      Required = True
    end
  end
  object FDQDeliveryAddress: TFDQuery
    Connection = dmLocal.FDConLocalDb
    Transaction = FDTransOrders
    SQL.Strings = (
      'select * from contacts'
      'where EXTADDRESSID = :EXTADDRESSID')
    Left = 704
    Top = 360
    ParamData = <
      item
        Name = 'EXTADDRESSID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 9
      end>
    object FDQDeliveryAddressID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQDeliveryAddressREFTABLE: TIntegerField
      FieldName = 'REFTABLE'
      Origin = 'REFTABLE'
      Required = True
    end
    object FDQDeliveryAddressDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQDeliveryAddressFIRSTNAME: TStringField
      FieldName = 'FIRSTNAME'
      Origin = 'FIRSTNAME'
      Size = 50
    end
    object FDQDeliveryAddressLASTNAME: TStringField
      FieldName = 'LASTNAME'
      Origin = 'LASTNAME'
      Size = 50
    end
    object FDQDeliveryAddressTELNO: TStringField
      FieldName = 'TELNO'
      Origin = 'TELNO'
      Size = 22
    end
    object FDQDeliveryAddressEMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 255
    end
    object FDQDeliveryAddressREFID: TStringField
      FieldName = 'REFID'
      Origin = 'REFID'
    end
    object FDQDeliveryAddressADDRESS1: TStringField
      FieldName = 'ADDRESS1'
      Origin = 'ADDRESS1'
      Size = 50
    end
    object FDQDeliveryAddressADDRESS2: TStringField
      FieldName = 'ADDRESS2'
      Origin = 'ADDRESS2'
      Size = 50
    end
    object FDQDeliveryAddressCITY: TStringField
      FieldName = 'CITY'
      Origin = 'CITY'
      Size = 50
    end
    object FDQDeliveryAddressPOSTCODE: TStringField
      FieldName = 'POSTCODE'
      Origin = 'POSTCODE'
    end
    object FDQDeliveryAddressTELNO2: TStringField
      FieldName = 'TELNO2'
      Origin = 'TELNO2'
      Size = 22
    end
    object FDQDeliveryAddressEXTADDRESSID: TIntegerField
      FieldName = 'EXTADDRESSID'
      Origin = 'EXTADDRESSID'
    end
    object FDQDeliveryAddressCOMPANY: TStringField
      FieldName = 'COMPANY'
      Origin = 'COMPANY'
      Size = 50
    end
  end
  object FDComDelivAddres: TFDCommand
    Connection = dmMain.FDConnection1
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    CommandText.Strings = (
      'select * from j3f6_j2store_addresses'
      'where last_edited > :LASTUPDATE')
    ParamData = <
      item
        Name = 'LASTUPDATE'
        DataType = ftDateTime
        ParamType = ptInput
        Value = Null
      end>
    Left = 64
    Top = 435
  end
  object FDTAdaptDelivAddres: TFDTableAdapter
    UpdateTableName = 'j3f6_j2store_addresses'
    DatSTableName = 'FDComDelivAddres'
    SelectCommand = FDComDelivAddres
    Left = 168
    Top = 436
  end
  object FDMemDelivAddres: TFDMemTable
    AfterOpen = FDMemDelivAddresAfterOpen
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Adapter = FDTAdaptDelivAddres
    Left = 256
    Top = 440
    object FDMemDelivAddresj2store_address_id: TFDAutoIncField
      FieldName = 'j2store_address_id'
      Origin = 'j2store_address_id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object FDMemDelivAddresuser_id: TIntegerField
      FieldName = 'user_id'
      Origin = 'user_id'
      Required = True
    end
    object FDMemDelivAddresfirst_name: TStringField
      FieldName = 'first_name'
      Origin = 'first_name'
      Required = True
      Size = 255
    end
    object FDMemDelivAddreslast_name: TStringField
      FieldName = 'last_name'
      Origin = 'last_name'
      Required = True
      Size = 255
    end
    object FDMemDelivAddresemail: TStringField
      FieldName = 'email'
      Origin = 'email'
      Required = True
      Size = 255
    end
    object FDMemDelivAddresaddress_1: TStringField
      FieldName = 'address_1'
      Origin = 'address_1'
      Required = True
      Size = 255
    end
    object FDMemDelivAddresaddress_2: TStringField
      FieldName = 'address_2'
      Origin = 'address_2'
      Required = True
      Size = 255
    end
    object FDMemDelivAddrescity: TStringField
      FieldName = 'city'
      Origin = 'city'
      Required = True
      Size = 255
    end
    object FDMemDelivAddreszip: TStringField
      FieldName = 'zip'
      Origin = 'zip'
      Required = True
      Size = 255
    end
    object FDMemDelivAddreszone_id: TStringField
      FieldName = 'zone_id'
      Origin = 'zone_id'
      Required = True
      Size = 255
    end
    object FDMemDelivAddrescountry_id: TStringField
      FieldName = 'country_id'
      Origin = 'country_id'
      Required = True
      Size = 255
    end
    object FDMemDelivAddresphone_1: TStringField
      FieldName = 'phone_1'
      Origin = 'phone_1'
      Required = True
      Size = 255
    end
    object FDMemDelivAddresphone_2: TStringField
      FieldName = 'phone_2'
      Origin = 'phone_2'
      Required = True
      Size = 255
    end
    object FDMemDelivAddresfax: TStringField
      FieldName = 'fax'
      Origin = 'fax'
      Required = True
      Size = 255
    end
    object FDMemDelivAddrestype: TStringField
      FieldName = 'type'
      Origin = '`type`'
      Required = True
      Size = 255
    end
    object FDMemDelivAddrescompany: TStringField
      FieldName = 'company'
      Origin = 'company'
      Required = True
      Size = 255
    end
    object FDMemDelivAddrestax_number: TStringField
      FieldName = 'tax_number'
      Origin = 'tax_number'
      Required = True
      Size = 255
    end
    object FDMemDelivAddreslast_edited: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'last_edited'
      Origin = 'last_edited'
    end
  end
  object FDComInvoiceAddress: TFDCommand
    Connection = dmMain.FDConnection1
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    CommandText.Strings = (
      
        'select first_name, last_name, email, city, zip, phone_1, phone_2' +
        ' '
      'from j3f6_j2store_addresses'
      'where user_id = :CUSTOMERID'
      'order by j2store_address_id')
    ParamData = <
      item
        Name = 'CUSTOMERID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 954
      end>
    Left = 64
    Top = 491
  end
  object FDTAdaptInvoiceAddress: TFDTableAdapter
    DatSTableName = 'FDComDelivAddres'
    SelectCommand = FDComInvoiceAddress
    Left = 168
    Top = 492
  end
  object FDMemInvoiceAddress: TFDMemTable
    AfterOpen = FDMemInvoiceAddressAfterOpen
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Adapter = FDTAdaptInvoiceAddress
    Left = 264
    Top = 491
    object FDMemInvoiceAddressfirst_name: TStringField
      FieldName = 'first_name'
      Origin = 'first_name'
      Required = True
      Size = 255
    end
    object FDMemInvoiceAddresslast_name: TStringField
      FieldName = 'last_name'
      Origin = 'last_name'
      Required = True
      Size = 255
    end
    object FDMemInvoiceAddressemail: TStringField
      FieldName = 'email'
      Origin = 'email'
      Required = True
      Size = 255
    end
    object FDMemInvoiceAddresscity: TStringField
      FieldName = 'city'
      Origin = 'city'
      Required = True
      Size = 255
    end
    object FDMemInvoiceAddresszip: TStringField
      FieldName = 'zip'
      Origin = 'zip'
      Required = True
      Size = 255
    end
    object FDMemInvoiceAddressphone_1: TStringField
      FieldName = 'phone_1'
      Origin = 'phone_1'
      Required = True
      Size = 255
    end
    object FDMemInvoiceAddressphone_2: TStringField
      FieldName = 'phone_2'
      Origin = 'phone_2'
      Required = True
      Size = 255
    end
  end
end

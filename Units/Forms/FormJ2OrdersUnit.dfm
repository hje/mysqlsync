object FormJ2Orders: TFormJ2Orders
  Left = 0
  Top = 0
  Caption = 'j2Orders'
  ClientHeight = 531
  ClientWidth = 799
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 799
    Height = 531
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object tbsOrders: TTabSheet
      Caption = 'Orders'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 791
        Height = 41
        Align = alTop
        Caption = 'Panel1'
        TabOrder = 0
        object Button1: TButton
          Left = 11
          Top = 5
          Width = 75
          Height = 25
          Caption = 'Open'
          TabOrder = 0
          OnClick = Button1Click
        end
        object Button5: TButton
          Left = 672
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Process'
          TabOrder = 1
          OnClick = Button5Click
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 41
        Width = 791
        Height = 343
        Align = alClient
        DataSource = DSoOrders
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'j2store_order_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_id'
            Width = 103
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'user_id'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_subtotal_ex_tax'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_tax'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_subtotal'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_total'
            Width = 66
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'invoice_prefix'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'invoice_number'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_shipping'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'user_email'
            Width = 156
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_shipping_tax'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderpayment_type'
            Width = 138
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_discount'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_discount_tax'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_credit'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_refund'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_surcharge'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_fees'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'customer_group'
            Width = 97
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cart_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'transaction_id'
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'transaction_status'
            Width = 107
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'currency_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'currency_code'
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'parent_id'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'token'
            Width = 205
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'subscription_id'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_type'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'currency_value'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ip_address'
            Width = 103
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'is_shippable'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'is_including_tax'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'customer_language'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_state_id'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_state'
            Width = 67
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'created_on'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'created_by'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'modified_on'
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'modified_by'
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 384
        Width = 791
        Height = 119
        Align = alBottom
        TabOrder = 2
        object DBMemo1: TDBMemo
          Left = 1
          Top = 16
          Width = 264
          Height = 102
          Align = alLeft
          DataField = 'customer_note'
          DataSource = DSoOrders
          TabOrder = 0
        end
        object DBMemo2: TDBMemo
          Left = 265
          Top = 16
          Width = 271
          Height = 102
          Align = alClient
          DataField = 'order_params'
          DataSource = DSoOrders
          TabOrder = 1
        end
        object DBMemo3: TDBMemo
          Left = 536
          Top = 16
          Width = 254
          Height = 102
          Align = alRight
          DataField = 'transaction_details'
          DataSource = DSoOrders
          TabOrder = 2
        end
        object Panel3: TPanel
          Left = 1
          Top = 1
          Width = 789
          Height = 15
          Align = alTop
          TabOrder = 3
          object Label2: TLabel
            Left = 274
            Top = 0
            Width = 67
            Height = 13
            Caption = 'order_params'
          end
          object Label3: TLabel
            Left = 544
            Top = -1
            Width = 91
            Height = 13
            Caption = 'transaction_details'
          end
          object Label1: TLabel
            Left = 8
            Top = -1
            Width = 72
            Height = 13
            Caption = 'customer_note'
          end
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'OrderLines'
      ImageIndex = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 791
        Height = 32
        Align = alTop
        Caption = 'Panel4'
        TabOrder = 0
        object Button2: TButton
          Left = 8
          Top = 3
          Width = 75
          Height = 25
          Caption = 'Open'
          TabOrder = 0
          OnClick = Button2Click
        end
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 32
        Width = 791
        Height = 359
        Align = alClient
        DataSource = DSoOrderLines
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'j2store_orderitem_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'product_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_sku'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_name'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_quantity'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_finalprice_without_tax'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_finalprice_with_tax'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_tax'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_finalprice'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_price'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_option_price'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'product_type'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_id'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_type'
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cart_id'
            Width = 46
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cartitem_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'variant_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'vendor_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_taxprofile_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_per_item_tax'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_discount'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_discount_tax'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'created_on'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'created_by'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_weight'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderitem_weight_total'
            Visible = True
          end>
      end
      object Panel5: TPanel
        Left = 0
        Top = 391
        Width = 791
        Height = 112
        Align = alBottom
        Caption = 'Panel5'
        TabOrder = 2
        object DBMemo4: TDBMemo
          Left = 1
          Top = 18
          Width = 224
          Height = 93
          Align = alLeft
          DataField = 'orderitem_attributes'
          DataSource = DSoOrderLines
          TabOrder = 0
        end
        object DBMemo5: TDBMemo
          Left = 225
          Top = 18
          Width = 216
          Height = 93
          Align = alLeft
          DataField = 'orderitem_params'
          DataSource = DSoOrderLines
          TabOrder = 1
        end
        object Panel6: TPanel
          Left = 1
          Top = 1
          Width = 789
          Height = 17
          Align = alTop
          TabOrder = 2
          object Label4: TLabel
            Left = 3
            Top = -1
            Width = 99
            Height = 13
            Caption = 'orderitem_attributes'
          end
          object Label5: TLabel
            Left = 230
            Top = -1
            Width = 87
            Height = 13
            Caption = 'orderitem_params'
          end
        end
      end
    end
    object tbsOrderInfos: TTabSheet
      Caption = 'OrderInfos'
      ImageIndex = 2
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 791
        Height = 32
        Align = alTop
        TabOrder = 0
        object Button3: TButton
          Left = 3
          Top = 3
          Width = 75
          Height = 25
          Caption = 'Open'
          TabOrder = 0
          OnClick = Button3Click
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 392
        Width = 791
        Height = 111
        Align = alBottom
        Caption = 'Panel8'
        TabOrder = 1
        object DBMemo6: TDBMemo
          Left = 1
          Top = 16
          Width = 244
          Height = 94
          Align = alLeft
          DataField = 'all_billing'
          DataSource = DSoOrderInfos
          TabOrder = 0
        end
        object DBMemo7: TDBMemo
          Left = 245
          Top = 16
          Width = 291
          Height = 94
          Align = alClient
          DataField = 'all_payment'
          DataSource = DSoOrderInfos
          TabOrder = 1
        end
        object DBMemo8: TDBMemo
          Left = 536
          Top = 16
          Width = 254
          Height = 94
          Align = alRight
          DataField = 'all_shipping'
          DataSource = DSoOrderInfos
          TabOrder = 2
        end
        object Panel9: TPanel
          Left = 1
          Top = 1
          Width = 789
          Height = 15
          Align = alTop
          TabOrder = 3
          object Label6: TLabel
            Left = 7
            Top = 0
            Width = 42
            Height = 13
            Caption = 'all_billing'
          end
          object Label7: TLabel
            Left = 250
            Top = 0
            Width = 55
            Height = 13
            Caption = 'all_shipping'
          end
          object Label8: TLabel
            Left = 546
            Top = -1
            Width = 58
            Height = 13
            Caption = 'all_payment'
          end
        end
      end
      object DBGrid3: TDBGrid
        Left = 0
        Top = 32
        Width = 791
        Height = 360
        Align = alClient
        DataSource = DSoOrderInfos
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'j2store_orderinfo_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_id'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_company'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_last_name'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_first_name'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_middle_name'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_phone_1'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_phone_2'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_fax'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_address_1'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_address_2'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_city'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_zip'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'billing_tax_number'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_company'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_last_name'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_first_name'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_middle_name'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_phone_1'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_phone_2'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_fax'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_address_1'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_address_2'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_city'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_zip'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_zone_name'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_country_name'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_zone_id'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_country_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_id'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'shipping_tax_number'
            Width = 100
            Visible = True
          end>
      end
    end
    object tbsOrderHistories: TTabSheet
      Caption = 'tbsOrderHistories'
      ImageIndex = 3
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 791
        Height = 33
        Align = alTop
        Caption = 'Panel10'
        TabOrder = 0
        object Button4: TButton
          Left = 7
          Top = 3
          Width = 75
          Height = 25
          Caption = 'open'
          TabOrder = 0
          OnClick = Button4Click
        end
      end
      object Panel11: TPanel
        Left = 0
        Top = 376
        Width = 791
        Height = 127
        Align = alBottom
        Caption = 'Panel11'
        TabOrder = 1
        object Panel12: TPanel
          Left = 1
          Top = 1
          Width = 789
          Height = 24
          Align = alTop
          TabOrder = 0
          object Label9: TLabel
            Left = 10
            Top = 5
            Width = 43
            Height = 13
            Caption = 'comment'
          end
          object Label10: TLabel
            Left = 226
            Top = 5
            Width = 35
            Height = 13
            Caption = 'params'
          end
          object Label11: TLabel
            Left = 438
            Top = 5
            Width = 100
            Height = 13
            Caption = 'orderstatus_cssclass'
          end
        end
        object DBMemo9: TDBMemo
          Left = 433
          Top = 25
          Width = 216
          Height = 101
          Align = alLeft
          DataField = 'orderstatus_cssclass'
          DataSource = DSoOrderHistories
          TabOrder = 1
        end
        object DBMemo10: TDBMemo
          Left = 1
          Top = 25
          Width = 216
          Height = 101
          Align = alLeft
          DataField = 'comment'
          DataSource = DSoOrderHistories
          TabOrder = 2
        end
        object DBMemo11: TDBMemo
          Left = 217
          Top = 25
          Width = 216
          Height = 101
          Align = alLeft
          DataField = 'params'
          DataSource = DSoOrderHistories
          TabOrder = 3
        end
      end
      object DBGrid4: TDBGrid
        Left = 0
        Top = 33
        Width = 791
        Height = 343
        Align = alClient
        DataSource = DSoOrderHistories
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'j2store_orderhistory_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_id'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'order_state_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'notify_customer'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'created_on'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'created_by'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'orderstatus_name'
            Width = 100
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 4
      object Panel13: TPanel
        Left = 0
        Top = 0
        Width = 791
        Height = 30
        Align = alTop
        Caption = 'Panel13'
        TabOrder = 0
        object Button6: TButton
          Left = 3
          Top = 1
          Width = 75
          Height = 25
          Caption = 'Open'
          TabOrder = 0
          OnClick = Button6Click
        end
      end
      object Panel14: TPanel
        Left = 0
        Top = 368
        Width = 791
        Height = 135
        Align = alBottom
        Caption = 'Panel14'
        TabOrder = 1
        object DBMemo12: TDBMemo
          Left = 465
          Top = 17
          Width = 232
          Height = 117
          Align = alLeft
          DataSource = DSoUsers
          TabOrder = 0
        end
        object Panel15: TPanel
          Left = 1
          Top = 1
          Width = 789
          Height = 16
          Align = alTop
          TabOrder = 1
          object Label12: TLabel
            Left = 2
            Top = 0
            Width = 53
            Height = 13
            Caption = 'Description'
          end
          object Label13: TLabel
            Left = 240
            Top = 1
            Width = 28
            Height = 13
            Caption = 'Notes'
          end
          object Label14: TLabel
            Left = 473
            Top = 1
            Width = 28
            Height = 13
            Caption = 'Plugin'
          end
        end
        object DBMemo13: TDBMemo
          Left = 1
          Top = 17
          Width = 232
          Height = 117
          Align = alLeft
          DataSource = DSoUsers
          TabOrder = 2
        end
        object DBMemo14: TDBMemo
          Left = 233
          Top = 17
          Width = 232
          Height = 117
          Align = alLeft
          DataSource = DSoUsers
          TabOrder = 3
        end
      end
      object DBGrid5: TDBGrid
        Left = 0
        Top = 30
        Width = 791
        Height = 338
        Align = alClient
        DataSource = DSoUsers
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'j2store_address_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'user_id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'first_name'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'last_name'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'email'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'address_1'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'address_2'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'city'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'zip'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'phone_1'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'phone_2'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'type'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'company'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tax_number'
            Width = 100
            Visible = True
          end>
      end
    end
  end
  object DSoOrders: TDataSource
    DataSet = dmJ2Orders.FDMemOrders
    Left = 76
    Top = 152
  end
  object DSoOrderLines: TDataSource
    DataSet = dmJ2Orders.FDQJ2OrderLines
    Left = 76
    Top = 216
  end
  object DSoOrderInfos: TDataSource
    DataSet = dmJ2Orders.FDQJ2OrderInfos
    Left = 76
    Top = 280
  end
  object DSoOrderHistories: TDataSource
    DataSet = dmJ2Orders.FDQJ2OrderHistories
    Left = 76
    Top = 336
  end
  object DSoUsers: TDataSource
    DataSet = dmJ2Orders.FDMemUsers
    Left = 76
    Top = 392
  end
end

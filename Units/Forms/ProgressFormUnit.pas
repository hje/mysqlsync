unit ProgressFormUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls;

type
  TFormProgress = class(TForm)
    ProgressBar1: TProgressBar;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;




implementation

{$R *.dfm}

procedure TFormProgress.FormCreate(Sender: TObject);
begin
    ProgressBar1.Min:=0;
    ProgressBar1.Step:=1;
end;

end.

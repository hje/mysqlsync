unit DBGridFilterUnit;

// Uses GridFilterUnit wich is a TStringGrid descendant.
// DBGridfilterUnit should be placed last in the uses clause under the interface section
// then all DBGrid components will be replaced by the DBGridFilter component.

interface

uses  Windows, Messages, DBGrids, Grids, System.Types, Classes , Graphics, GridFilterUnit,
      System.SysUtils,  DBClient, vcl.Controls, Data.DB, System.UITypes,
      vcl.ComCtrls, Vcl.Dialogs, Vcl.Forms, Vcl.Menus,
      System.Variants, FireDAC.Comp.DataSet, FireDAC.Comp.Client;
      //, DBGridPopUpUnit;



type TDBGrid=class(DBGrids.TDBGrid)
      procedure WM_ACTIVATE(var Msg: messages.TWMActivate); message WM_ACTIVATE;
      procedure WMHScroll(var Msg: TWMHScroll); message WM_HSCROLL;
  private
      FFiltered: boolean;
      FFilteredOnField: boolean;
      FSenderDBGrid: TDBGrid;
      FFilterBtn: TToolButton;
      FFilterRefreshBtn: TToolButton;
      FGridResize: TNotifyEvent;
      FDBGridWidth: integer;
      FStatusFieldName: string;
      FStatusImageList: TImageList;
      FRowColorByFieldName: string;
      FEnableRowColorCodes: boolean;
      //FPopup : TDbGridPopUpMenu;
      FUseAutoPopUp: Boolean;
      procedure ColEnter; override;
      procedure DrawColumnCell(const Rect: TRect; DataCol: Integer;
                  Column: TColumn; State: TGridDrawState); override;
      procedure SetFiltered(FilteredState: Boolean);
      procedure Resize(Sender : TObject);
      procedure Click; override;
      procedure ExitDBGrid(Sender: TObject);
      procedure EnterDBGrid(Sender: TObject);
      procedure Invalidate; override;
      procedure TitleClick(Column: TColumn); override;
      procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
      procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
      procedure Paint; override;
      procedure UseInternalPopUpMenu(UsePopUpMenu:Boolean);
  protected
  public
      GridFilter: TGridFilter;
      procedure KeyDown(var Key: Word; Shift: TShiftState); override;
      procedure ShowFilter(SenderDBGrid: TDBGrid; FilterBtn, FilterRefreshBtn: TToolbutton);
      procedure HideFilter;
      procedure FilterOnField(FieldName, FilterValue, FilterOperator: string);
      procedure CloseFilterOnField;
      procedure FilterRefresh;
      procedure FilterBtnClick(Sender: TObject);
      procedure ExportDataToFile;
      function GetColumnIndex(FieldName: string):Integer;
      procedure SyncFilterButtons(FilterBtn, FilterRefreshBtn: TToolbutton);
      procedure FilterDateChange(SenderDBGrid: TDBGrid; DateFrom, DateTo: TDate; DateFromFieldName, DateToFieldName: string);

      property Filtered: Boolean read FFiltered write SetFiltered;
      property OnResize: TNotifyEvent read FGridResize write FGridResize;
      property StatusFieldName: string read FStatusFieldName write FStatusFieldName;
      property StatusImageList: TImageList read FStatusImageList write FStatusImagelist;
      property RowColorByFieldName: string read FRowColorByFieldName write FRowColorByFieldName;
      property EnableRowColorCodes: boolean read FEnableRowColorCodes write FEnableRowColorCodes;
      property UseAutoPopUp: boolean read FUseAutoPopUp write UseInternalPopUpMenu;
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
end;

implementation

uses TeraFunkUnit;


procedure TDBGrid.ShowFilter(SenderDBGrid: TDBGrid; FilterBtn, FilterRefreshBtn: TToolbutton);
begin
   FFiltered:=True;
   FSenderDBGrid:=SenderDBGRid;
   FSenderDBGrid.RowHeights[0]:=40;

   if (FilterBtn <> nil) then
   begin
      FFilterBtn:=FilterBtn;
      FFilterBtn.Enabled:=True;
      FFilterBtn.ImageIndex:=44;
      FFilterBtn.Down:=True;
   end;

   if (FilterRefreshBtn <> nil) then
   begin
       FFilterRefreshBtn:=FilterRefreshBtn;
       FFilterRefreshBtn.Enabled:=True;
   end;

   if not Assigned(GridFilter) then
      GridFilter:=TGridFilter.Create(SenderDBGrid,  TClientDataset(self.DataSource.DataSet));

   self.GridFilter.Visible:=True;
   SenderDBGrid.Refresh;
end;

procedure TDBGrid.SyncFilterButtons(FilterBtn, FilterRefreshBtn: TToolbutton);
begin
   if assigned(GridFilter) then
   begin
       if (self.GridFilter.Visible = true) and (Filtered = true) then
       begin
           if (FilterBtn <> nil) then
           begin
              FFilterBtn:=FilterBtn;
              FFilterBtn.Enabled:=True;
              FFilterBtn.ImageIndex:=44;
              FFilterBtn.Down:=True;
           end;

           if (FilterRefreshBtn <> nil) then
           begin
               FFilterRefreshBtn:=FilterRefreshBtn;
               FilterRefreshBtn.Enabled:=True;
           end;
       end
       else
       begin
           if (FilterBtn <> nil) then
           begin
              FFilterBtn.Enabled:=True;
              FFilterBtn:=FilterBtn;
              FFilterBtn.ImageIndex:=46;
              FFilterBtn.Down:=False;
           end;

           if (FilterRefreshBtn <> nil) then
           begin
              FFilterRefreshBtn:=FilterRefreshBtn;
              FFilterRefreshBtn.Enabled:=False;
           end;
       end;
   end
   else
   begin
       if (FilterBtn <> nil) then
       begin
          FFilterBtn.Enabled:=True;
          FFilterBtn:=FilterBtn;
          FFilterBtn.ImageIndex:=46;
          FFilterBtn.Down:=False;
       end;

       if (FilterRefreshBtn <> nil) then
       begin
          FFilterRefreshBtn:=FilterRefreshBtn;
          FFilterRefreshBtn.Enabled:=False;
       end;
   end;
end;

procedure TDBGrid.TitleClick(Column: TColumn);
var
    AggrActiv: Boolean;
begin
  inherited;
  if (self.DataSource.DataSet is TClientDataset) then
  begin
      AggrActiv:=TClientDataset(self.DataSource.DataSet).AggregatesActive;

      if AggrActiv = true then
         TClientDataset(self.DataSource.DataSet).AggregatesActive:= False;

      TClientDataset(self.DataSource.DataSet).IndexFieldNames:=Column.FieldName;
      TClientDataset(self.DataSource.DataSet).AggregatesActive:=AggrActiv;
  end
  else if (self.DataSource.DataSet is TFDQuery) then
  begin
      AggrActiv:=TFDQuery(self.DataSource.DataSet).AggregatesActive;

      if AggrActiv = true then
          TFDQuery(self.DataSource.DataSet).AggregatesActive:=False;

      TFDQuery(self.DataSource.DataSet).IndexFieldNames:=Column.FieldName;
      TFDQuery(self.DataSource.DataSet).AggregatesActive:=AggrActiv;
  end
  else if (self.DataSource.DataSet is TFDMemTable) then
  begin
      AggrActiv:=TFDMemTable(self.DataSource.DataSet).AggregatesActive;

      if AggrActiv = true then
          TFDMemTable(self.DataSource.DataSet).AggregatesActive:=False;

      TFDMemTable(self.DataSource.DataSet).IndexFieldNames:=Column.FieldName;
      TFDMemTable(self.DataSource.DataSet).AggregatesActive:=AggrActiv;
  end


end;

procedure TDBGrid.Click;
begin
  inherited;

end;

procedure TDBGrid.CloseFilterOnField;
var
  i: integer;
begin
    // If gridfilter is active then keep user typed informations
    if (self.Filtered = true) and (self.GridFilter.Visible = true) then
    begin
        for i := 0 to self.ColCount do
        begin
            if self.GridFilter.Cells[i+1,4] > '' then
                self.GridFilter.Cells[i+1,1]:='';

            self.GridFilter.Cells[i+1,1]:='';
        end;

        self.FilterRefresh;
    end
    else
    begin           // else close both filters
        if Assigned(self.GridFilter) then
        begin
            self.GridFilter.Free;
            self.GridFilter:=nil;
        end;

        self.Filtered:=False;
        Self.DataSource.DataSet.Filtered:=False;
    end;

    FFilteredOnField:=False;
end;

procedure TDBGrid.ColEnter;
begin
  inherited;

  if Self.Filtered = True then
     GridFilter.Col:=self.SelectedIndex+1;

end;


constructor TDBGrid.Create(AOwner: TComponent);
begin
    inherited;
    FFilterBtn:= TToolButton(owner.FindComponent('tBtnBaseFilter'));
    FFilterRefreshBtn:=TToolButton(owner.FindComponent('tBtnBaseFilterRefresh'));
    self.Options:=self.Options + [dgMultiSelect];

    FDBGridWidth:=self.Width;
    self.OnResize:=Resize;
    self.OnEnter:=EnterDBGrid;
    self.OnExit:=ExitDBGrid;

    UseAutoPopUp:=False;
end;



procedure TDBGrid.UseInternalPopUpMenu(UsePopUpMenu:Boolean);
begin
   {FUseAutoPopUp:=UsePopUpMenu;
   if UsePopUpMenu = true then
   begin
       self.FPopup := TDBGridPopUpMenu.Create(self.Owner);
       Self.PopupMenu := TPopupMenu(FPopup);
   end;}
end;

procedure TDBGrid.SetFiltered(FilteredState: Boolean);
begin
    if Assigned(GridFilter) then
       GridFilter.SetFiltered(FilteredState);

    if assigned(self) then
        FFiltered:=FilteredState;
end;



procedure TDBGrid.HideFilter;
var
  i: integer;
begin
    self.DataSource.DataSet.Filtered:=False;
    FFiltered:=False;

    if FFilteredOnField = true then
    begin
        GridFilter.Visible:=False;

        for I := 0 to self.ColCount-1 do
        begin
             self.GridFilter.Cells[i+1,1]:=self.GridFilter.Cells[i+1,4]
        end;

        self.Filtered:=True;
        self.FFilteredOnField:=True;
    end
    else
    begin
        GridFilter.Free;
        GridFilter:=nil;
    end;

    self.RowHeights[0]:=20;

    if FSenderDBGrid <> nil then
       FSenderDBGrid.Repaint;

    if FFilterBtn <> nil then
    begin
       FFilterBtn.ImageIndex:=46;
       FFilterBtn.Down:=False;
    end;
    if FFilterRefreshBtn <> nil then
       FFilterRefreshBtn.Enabled:=False;
end;

procedure TDBGrid.Invalidate;
begin
  inherited;

  if self.Filtered = true then
  begin
      if self.Width > FDBGridWidth then
      begin
         if assigned(FGridResize) then
            OnResize(self);

         FDBGridWidth:=self.Width;
      end;
  end;

end;

procedure TDBGrid.KeyDown(var Key: Word; Shift: TShiftState);
var
    FilterBtn, FilterRefreshBtn: TToolButton;
begin
    inherited;

    if Key = VK_F8  then
    begin
       if (Filtered = True) and (self.GridFilter.Visible = true)  then
       begin
           self.HideFilter;
       end
       else
       begin
           FilterBtn:= TToolButton(Owner.FindComponent('tBtnBaseFilter'));
           FilterRefreshBtn:=TToolButton(Owner.FindComponent('tBtnBaseFilterRefresh'));
           self.ShowFilter(self, FilterBtn, FilterRefreshBtn);
       end;
    end;

end;

procedure TDBGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
//    if Button = mbRight then
//       PopupMenu.Popup(Left + X, Top + Y);

end;

procedure TDBGrid.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  pnt: TPoint;
  RefTableFieldIndex: integer;
begin
  inherited;
 { GetCursorPos(pnt);

  if UseAutoPopUp = true then
  begin
      if Button = mbRight then
      begin
           PopupMenu.Items[0].Enabled:=False;

           if (self.SelectedField.Origin > '') then
           begin
               self.FPopup.FGridTableName:=HeintaTeknSepareraFelt(self.SelectedField.Origin,'.','"',1);
               self.FPopup.FGridFieldName:=HeintaTeknSepareraFelt(self.SelectedField.Origin,'.','"',2);
               self.FPopup.FLookUpValue:=self.SelectedField.Value;

               with DataModule1 do
               begin
                   if CdsTableFieldMasterTable.Locate('TableName;FieldName',VarArrayOf([FPopup.FGridTableName,FPopup.FGridFieldName]),[]) then
                   begin
                       if not (DataModule1.CdsTableFieldMasterTable.FieldByName('MainTableRefID').isNull) then
                          RefTableFieldIndex:=Self.GetColumnIndex(DataModule1.CdsTableFieldMasterTable.FieldByName('MainTableRefID').Value)
                       else
                          RefTableFieldIndex:=-1;

                       if (RefTableFieldIndex >= 0)  then
                          self.FPopup.FMasterTableRefID:=StrToInt(self.GetFieldValue(RefTableFieldIndex))
                       else
                          self.FPopup.FMasterTableRefID:=0;

                       PopupMenu.Items[0].Enabled:=True;
                   end
                   else
                      PopupMenu.Items[0].Enabled:=False;
                      //ShowMessage(CdsTableFieldMasterTableMainTable.Value+', '+CdsTableFieldMasterTableMainTableFieldName.Value);
                   //if CdsTableFieldMasterTable.Locate('TableName;FieldName',varArrayOf([self.SelectedField.Origin,  self.SelectedField.FieldName then
               end;
           end;

           PopupMenu.Popup(pnt.X, pnt.Y);
      end;
  end; }
end;

procedure TDBGrid.Paint;
begin
  inherited;

  if assigned(FsenderDBGrid) then
  begin
      if (FFiltered = True) and (self.GridFilter.Visible = true) then
          FSenderDBGrid.RowHeights[0]:=40
      else
      begin
          if not assigned(GridFilter) then
             FSenderDBGrid.RowHeights[0]:=20;
      end;
  end;
end;

procedure TDBGrid.WMHScroll(var Msg: TWMHScroll);
begin
    inherited;
end;

procedure TDBGrid.WM_ACTIVATE(var Msg: messages.TWMActivate);
begin
    inherited;
end;

procedure TDBGrid.Resize(Sender: TObject);
begin
    if Assigned(GridFilter) then
       GridFilter.GridRefresh;
end;

destructor TDBGrid.Destroy;
begin
  if (self <> nil) then
  begin
      //self.FPopup.Free;

      if self.Filtered = true then
         self.Filtered:= False;

      inherited;
  end;
end;

procedure TDBGrid.DrawColumnCell(const Rect: TRect; DataCol: Integer;
  Column: TColumn; State: TGridDrawState);
var
  row : integer;
  _BMP: TBitmap;
  _Rect: TRect;
begin
   inherited;

   row := self.DataSource.DataSet.RecNo;

   if EnableRowColorCodes = false then
   begin
       if Odd(row) then
         self.Canvas.Brush.Color := clWhite
       else
         self.Canvas.Brush.Color := clScrollBar;
   end
   else
   begin
       {if (DataSource.DataSet.FieldByName(FRowColorByFieldName).Value = 0) or (DataSource.DataSet.FieldByName(FRowColorByFieldName).IsNull) then
       begin
          if Odd(row) then
             self.Canvas.Brush.Color := clWhite
           else
             self.Canvas.Brush.Color := clScrollBar;
       end
       else if DataSource.DataSet.FieldByName(FRowColorByFieldName).Value < 0 then
       begin
          if Odd(row) then
             self.Canvas.Brush.Color := DataModule1.LightYellow
           else
             self.Canvas.Brush.Color := DataModule1.Gult;
       end
       else if DataSource.DataSet.FieldByName(FRowColorByFieldName).Value > 0 then
       begin
          if Odd(row) then
             self.Canvas.Brush.Color := DataModule1.LightOrange
           else
             self.Canvas.Brush.Color := DataModule1.AppelsinGult;
       end; }
   end;

   if (gdSelected in State) then
   begin
      with self.Canvas do
      begin
        Brush.Color := clYellow;
        Font.Style := Font.Style + [fsBold];
        Font.Color := clHighlightText;
      end;
   end;

   self.Canvas.Font.Color:=clBlack;
   DefaultDrawColumnCell(Rect, DataCol, Column, State) ;


   // Statusfield
   // Is activated by defining properties StatusFieldName and StatusImageList
   if self.StatusFieldName > '' then
   begin
       if Assigned(DataSource) and Assigned(DataSource.DataSet) and
               DataSource.DataSet.Active and (not DataSource.DataSet.IsEmpty()) then
       begin
          if (UpperCase(Column.FieldName) = UpperCase(self.StatusFieldName)) then
          begin
             // Cell clear (to erase the default text) :
             Canvas.FillRect(Rect);

             // Minimum size for the column (assuming that my bitmaps are 16x16 sized):
             if (Column.Width < 18) then
             begin
                Column.Width := 18;
             end;
                 if not Column.Field.IsNull  then
                 begin
                     // _BMP gets the ImageList's associated LED
                     _Bmp := TBitmap.Create();
                     StatusImageList.GetBitmap(Column.Field.asInteger, _BMP);
                     //if Column.Field.Value > '' then
                     //    ImageList1.GetBitmap(0, _BMP);

                     // Calculates the image position & size (here it'll be centered into your State column) :
                     _Rect.Left := Rect.Left + (Rect.Right - Rect.Left - _BMP.Width) div 2;
                     _Rect.Right := _Rect.Left + _BMP.Width;
                     _Rect.Top := Rect.Top + (Rect.Bottom - Rect.Top - _BMP.Height) div 2;
                     _Rect.Bottom := _Rect.Top + _BMP.Height;
                     // Draws the image :
                     Canvas.CopyRect(_Rect, _BMP.Canvas, _BMP.Canvas.ClipRect);
                     FreeAndNil(_BMP);
                 end;
           end;
       end;
   end;
end;


{procedure TDBGrid.OnEnter;
begin
  inherited;
  SyncFilterButtons(FFilterBtn,FFilterRefreshBtn);
end;
}

procedure TDBGrid.ExitDBGrid(Sender: TObject);
begin
   if (Sender as TDBGrid).DataSource <> nil  then
   begin
       if (self.DataSource.DataSet.state in [dsEdit,dsInsert]) then
           self.DataSource.DataSet.Post;

       if (FFilterBtn <> nil) then
       begin
          FFilterBtn.ImageIndex:=46;
          FFilterBtn.Enabled:=False;
          FFilterBtn.Down:=False;
       end;

       if (FFilterRefreshBtn <> nil) then
       begin
          FFilterRefreshBtn.Enabled:=False;
       end;
   end;
end;

procedure TDBGrid.EnterDBGrid(Sender: TObject);
begin

    FFilterBtn:= TToolButton(owner.FindComponent('tBtnBaseFilter'));
    FFilterRefreshBtn:=TToolButton(owner.FindComponent('tBtnBaseFilterRefresh'));

    if (FFilterBtn <> nil) and (FFilterRefreshBtn <> nil) then
       SyncFilterButtons(FFilterBtn,FFilterRefreshBtn);
end;

procedure TDBGrid.ExportDataToFile;
var
    ExpStrList: TStringList;
    i: integer;
    SaveDialog: TSaveDialog;
    FileName: string;
    FieldValue: string;
    TempString: string;
begin
   ExpStrList:=TStringList.Create;
   try
       self.DataSource.DataSet.First;

       // Add fieldnames to the first row
       for i := 0 to self.DataSource.DataSet.FieldCount-1 do
       begin
            FieldValue:=self.DataSource.DataSet.Fields[i].FieldName;

            if i < self.DataSource.DataSet.FieldCount-1 then
               FieldValue:=FieldValue+';';

            TempString:=TempString+FieldValue;
       end;

       ExpStrList.Append(TempString);
       TempString:='';

       // All records fieldvalues
       while not self.DataSource.DataSet.eof do
       begin
           for i := 0 to self.DataSource.DataSet.FieldCount-1 do
           begin
                // Make sure that no passwordfields are exported
                if (UpperCase(self.DataSource.DataSet.Fields[i].FieldName) = 'LOYNIORD') or (UpperCase(self.DataSource.DataSet.Fields[i].FieldName) = 'PASSWORD') then
                   FieldValue:='*********'
                else
                   FieldValue:=self.DataSource.DataSet.Fields[i].AsString;

                if i < self.DataSource.DataSet.FieldCount-1 then
                   FieldValue:=FieldValue+';';

                TempString:=TempString+FieldValue;
           end;

           ExpStrList.Append(TempString);
           TempString:='';

           self.DataSource.DataSet.Next;
       end;

       SaveDialog:=TSaveDialog.Create(self);

       if SaveDialog.Execute then
       begin
          SaveDialog.DefaultExt:='.csv';
          FileName:=SaveDialog.FileName;
          ExpStrList.SaveToFile(FileName);
       end;

   finally
       ExpStrList.Free;
   end;
end;


procedure TDBGrid.FilterBtnClick(Sender: TObject);
begin
  if (Sender is TToolButton) then
  begin
      if (Sender as TToolButton).Down then
      begin
         if ((Owner as TForm).ActiveControl is TDBGrid) then
            ((Owner as TForm).ActiveControl as TDBGrid).ShowFilter(((Owner as TForm).ActiveControl as TDBGrid), TToolButton((Owner as TForm).FindComponent('tBtnBaseFilter')),TToolButton((Owner as TForm).FindComponent('tBtnBaseFilterRefresh')))
         else
            TToolButton((Owner as TForm).FindComponent('tBtnBaseFilter')).Down:=false;
      end
      else
      begin
         if ((Owner as TForm).ActiveControl is TDBGrid) then
            ((Owner as TForm).ActiveControl as TDBGrid).HideFilter;
      end;
  end;
end;

procedure TDBGrid.FilterDateChange(SenderDBGrid: TDBGrid; DateFrom, DateTo: TDate; DateFromFieldName, DateToFieldName: string);
var
    idx: integer;
begin
   //
   if FFiltered = True then
   begin
       if (DateFrom <> 0) then
       begin
           idx:=GetColumnIndex(DateFromFieldName);
           FSenderDBGrid.GridFilter.Cells[idx+1,1]:='>= '+DateToStr(DateFrom);
       end;

       if (DateTo <> 0) then
       begin
           idx:=GetColumnIndex(DateToFieldName);
           FSenderDBGrid.GridFilter.Cells[idx+1,1]:='<= '+DateToStr(DateTo);
       end;
   end;

   SenderDBGrid.DataSource.DataSet.Close;
   SenderDBGrid.DataSource.DataSet.Open;

   if FFiltered = True then
      FilterRefresh;
end;

procedure TDBGrid.FilterOnField(FieldName, FilterValue, FilterOperator: string);
var
    idx: integer;
begin
    if not assigned(GridFilter) then
       GridFilter:=TGridFilter.Create(self,  TClientDataset(self.DataSource.DataSet));

    self.Filtered:=False;
    idx:=GetColumnIndex(FieldName);
    self.GridFilter.Cells[idx+1,1]:=FilterOperator+' '+FilterValue;
    self.GridFilter.Cells[idx+1,4]:=FilterOperator+' '+FilterValue;
    self.GridFilter.Visible:=False;
    self.Filtered:=True;
    self.FFilteredOnField:=True;
end;

procedure TDBGrid.FilterRefresh;
begin
    if FFiltered = true then
    begin
        if self.GridFilter.Visible = True then
           self.RowHeights[0]:=40;

        GridFilter.SetFiltered(True);
    end;
end;

function TDBGrid.GetColumnIndex(FieldName: string):Integer;
var
  i: Integer;
begin
  result:=-1;

  for i := 0 to self.Columns.Count - 1 do
  begin
    if self.Columns[i].FieldName = FieldName then
      result := i;
  end;
end;

end.


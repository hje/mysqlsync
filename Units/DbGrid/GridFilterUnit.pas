unit GridFilterUnit;

interface

uses
    Windows, vcl.Forms, vcl.Grids, vcl.DBGrids,Classes, Graphics, Controls,
    System.UITypes, Data.DB, System.Sysutils, DBClient, Vcl.Dialogs;



type
  TGridFilter = class(TStringGrid)

  private
     FFiltered: boolean;
     FSenderDBGrid: TDBGrid;
     FSenderDataSet: TClientDataset;
     StringGridFilter: TGridFilter;
     procedure CreateFields(SenderDBGrid: TDBGrid);
     procedure TopLeftChanged; override;
     procedure Click; override;
     function CheckIfNumberField(FieldType: TFieldType): boolean;
     procedure ApplyFilter;
     procedure DBLClick; override;
     procedure KeyDown(var Key: Word; Shift: TShiftState); override;
     procedure KeyUp(var Key: Word; Shift: TShiftState); override;
     procedure FixedCellClick(ACol, ARow: Longint); override;
  public
     procedure GridRefresh;
     procedure SetFiltered(FilteredState: Boolean);
     constructor create(SenderDBGrid: TComponent; SenderDataSet: TClientDataset);  reintroduce;
     destructor Destroy; override;
  end;



implementation



{ TGridFilter }

uses TeraFunkUnit;

procedure TGridFilter.ApplyFilter;
var
  FilterString, tempFilterString, FieldName, SearchText: string;
  i: integer;
  Condition: string;
  NumberField: Boolean;
begin
    FieldName:='';
    FilterString:='';
    Condition:=' = ';

    for i := 0 to self.ColCount - 1 do
    begin
        tempFilterString:='';

        if trim(self.Cells[i+1,1]) > '' then
        begin
            NumberField:=CheckIfNumberField(FSenderDBGrid.Columns[i].Field.DataType);
            FieldName:=FSenderDBGrid.Columns[i].FieldName;
            SearchText:=self.Cells[i+1,1];

            if (SearchText[1] = '>') or (SearchText[1] = '<') or (SearchText[1] = '=') then
            begin
               if (SearchText[2] = '>') or (SearchText[2] = '<') or (SearchText[2] = '=') then
               begin
                   Condition:=' '+SearchText[1]+SearchText[2]+' ';
                   SearchText:=trim(Copy(SearchText,3,length(SearchText)));
               end
               else
               begin
                   Condition:=' '+SearchText[1]+' ';
                   SearchText:=trim(Copy(SearchText,2,length(SearchText)));
               end;
            end
            else if (trim(SearchText) = 'IS NULL') then
               Condition:=' '
            else
               Condition:=' = ';

            if  NumberField = True then
                tempFilterString:='('+FieldName+Condition+SearchText
            else
                tempFilterString:='('+FieldName+Condition+QuotedStr(SearchText);

            tempFilterString:=tempFilterString+')';
        end;

        if FilterString = '' then
           FilterString:=tempFilterString
        else if tempFilterString > '' then
           FilterString:=FilterString+' and '+tempFilterString;

    end;

    FSenderDataSet.Filter:=FilterString;

    if FieldName = '' then
        self.FSenderDataSet.Filtered:=False
    else
        self.FSenderDataSet.Filtered:=True;

end;


function TGridFilter.CheckIfNumberField(FieldType: TFieldType): boolean;
begin
    if (FieldType = ftCurrency) then
       result:=True
    else if FieldType = ftExtended then
       result:=True
    else if FieldType = ftFloat then
       result:=True
    else if FieldType = ftInteger then
       result:=True
    else if FieldType = ftSmallInt then
       result:=True
    else if FieldType = ftShortInt then
       result:=True
    else if FieldType = ftBCD then
       result:=True
    else
       result:=False;
end;


procedure TGridFilter.Click;
begin
  inherited;
  FSenderDBGrid.SelectedIndex:=self.Col-1;
end;


constructor TGridFilter.create(SenderDBGrid: TComponent; SenderDataSet: TClientDataset);
begin
  inherited create(SenderDBGrid);

  self.Parent:=SenderDBGrid as TDBGrid;
  self.Width:=Parent.Width-20;
  self.BevelOuter:=bvNone;
  self.BevelKind:=bkNone;
  self.BorderStyle:=bsNone;
  self.Height:=40;
  self.RowHeights[0]:=18;    // Row[0] = Headers
  self.FixedRows:=1;         // Fixedrows = headers
  self.FixedCols:=1;
  self.ColWidths[0]:=11;
  self.RowHeights[1]:=18;    // Row[1] = Filtervalues
  self.RowHeights[2]:=0;     // Row[2] = Filtervalues
  self.RowHeights[3]:=0;     // Row[3] = FieldNames
  self.RowHeights[4]:=0;     // Row[4] = Used for FilterOnFields function
  self.Color:=clYellow;
  self.ScrollBars:= TScrollStyle(0);
  self.Options:=self.Options + [goEditing, goTabs, goFixedRowClick, goAlwaysShowEditor];
  self.TabStop:=True;
  FSenderDBGrid:=SenderDBGrid as TDBGrid;

  FSenderDataSet:=SenderDataset;
  CreateFields(SenderDBGRid as TDBGrid);
end;

procedure TGridFilter.CreateFields(SenderDBGrid: TDBGrid);
var
    i: integer;
begin
    self.ColCount:=SenderDBGrid.Columns.count+1;

    for i:=0 to SenderDBGrid.Columns.count-1 do
    begin
        self.ColWidths[i+1]:=SenderDBGrid.Columns[i].Width;
        self.Cells[i+1,0]:=SenderDBGrid.Columns[i].Title.Caption;
        //self.Cells[i+1,1]:='';
        self.Cells[i+1,3]:=SenderDBGrid.Columns[i].FieldName;
    end;
    self.RowHeights[2]:=0;
    self.RowHeights[3]:=0;
end;

procedure TGridFilter.DBLClick;
begin
  inherited;
  ApplyFilter;
end;

destructor TGridFilter.Destroy;
begin
  inherited;

end;

procedure TGridFilter.FixedCellClick(ACol, ARow: Integer);
begin
  inherited;
  try
      if FSenderDataSet.AggregatesActive = False then
      begin
          if ACol > 0 then
          begin
            //showmessage(self.Cols[ACol].Strings[ARow+3]+' : '+FSenderDataset.Name );
            FSenderDataset.IndexFieldNames:=self.Cols[ACol].Strings[ARow+3];
          end;
      end
      else
          Showmessage('Can not sort on this grid');
  except
      Showmessage('Can not sort on field "'+self.Cols[ACol].Strings[ARow+3]+'"');
  end;

end;

procedure TGridFilter.GridRefresh;
begin
    if assigned(self) and (Parent <> nil) then
    begin
        Self.Width:=Parent.Width;
        Self.repaint;
    end;
end;

procedure TGridFilter.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_TAB) and shiftdown then
  begin
      if (self.Col = 1)  then
      begin
         Key:=0;
         Exit;
      end;
          //self.Col:= 1; //self.col-1;
  end
  else if Key = VK_TAB then
  begin
      if (self.Col = self.ColCount-1)  then
      begin
         Key:=0;
         Exit;
      end;
          //self.Col:= self.ColCount-1;
  end;


  inherited;
  //showmessage(owner.ClassName);
  //if Key = VK_return then
  //    DblClick;
end;

procedure TGridFilter.KeyUp(var Key: Word; Shift: TShiftState);
begin
  if Key = VK_return then
  begin
      DBLClick;
  end;

  inherited;
end;

procedure TGridFilter.SetFiltered(FilteredState: Boolean);
begin
    if FSenderDataSet <> nil then
    begin
        if FilteredState = True then
           ApplyFilter
        else
           FSenderDataSet.Filtered:=False;
    end;
end;

procedure TGridFilter.TopLeftChanged;
begin
  inherited;
  //showmessage('topleft');
  self.Repaint;
  //self.Refresh;
end;

end.
